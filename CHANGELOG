✔️ New feature
⚡ Improvement
🐛 Bugfix
❌ Deprecated or removed feature

**05-06-2024**
🐛 Fixed a performance issue in the multi-select block

**19-03-2024**
✔️ Added a signature block (see https://tripetto.com/blog/product-update-new-signature-block/)
⚡ Made domain validation (URL validation) less strict so that local domains, IP addresses and relative paths are now allowed
⚡ Added the alias of a block to the tracking functionality
⚡ Added a live preview of uploaded images in the result panel
🐛 Fixed a bug in the email address field of the notification panel

**15-01-2024**
🐛 Fixed a bug in the calculator block when calculating scores for individual rows in a matrix block

**06-11-2023**
⚡ Improved the zoom functionality of the builder when working with very large forms
🐛 Fixed an issue where it was not possible to complete a form in some specific cases

**18-10-2023**
✔️ Added a setting to hide sections after a (required) block that fails validation (this was the default behavior prior to version 8.1.0)
🐛 Fixed a backward compatibility issue in the hidden-field block

**12-10-2023**
✔️ New version of the Autoscroll runner that is capable of showing blocks (questions) past a required block that is left unanswered (this also greatly improves the progress bar behavior)
✔️ Added a display value that will be used when recalling values in a form (this ensures that not the optional alias is shown, but the display-friendly value or name)
✔️ Added hyperlink support to the ranking block
🐛 Fixed a bug in the Autoscroll runner where the welcome or closing screen of a form could become unresponsive
🐛 Fixed Firefox drag-and-drop issue in ranking block

**25-08-2023**
🐛 Fix bug in Studio package typings

**05-07-2023**
✔️ Added a ranking block (see https://tripetto.com/blog/product-update-the-often-requested-ranking-block/)
✔️ Added password support and improved the login procedure
✔️ Added version conflict detection for workspaces and forms
✔️ Added a signature property to the Studio package that allows securing form embeds so they only can use forms from a certain account
⚡ Make file uploads available to webhooks without a session token (with a maximum of 24 hours)
⚡ Add file name as querystring parameter to file upload URLs (allows to extract the original file name of an upload)
⚡ Improved workspace performance
⚡ Improved protection for data values that are automatically generated (these values can no longer be changed with, for example, a setter block)

**23-05-2023**
✔️ Forms-in-forms support (subforms) that allows for improved organization and structuring of large forms
✔️ Allow the use of `mailto:` and `tel:` hyperlinks in buttons (works in the multiple choice block, picture choice block, and in closing messages)
✔️ Added technical documentation for using the Tripetto Studio API (https://tripetto.com/sdk/docs/applications/studio/api/)
⚡ Improved embed codes
⚡ Improved share screen
⚡ Update help URLs to the new Tripetto website

**03-03-2023**
🐛 Fixed a bug in the text condition evaluation in the following blocks: calculator, evaluate, hidden field, text (single line), text (multiple lines), and variable

**21-12-2022**
🐛 Fixed a bug in the Classic runner where the height of the form was incorrect

**22-11-2022**
🐛 Fixed a timezone-related bug in the date/time selection control

**17-11-2022**
🐛 Fixed a bug in the pause function of the Autoscroll runner

**07-11-2022**
🐛 Fixed a bug in the multi-select dropdown block where condition labels were missing
🐛 Fixed a bug where an empty option in a dropdown block could be selected as the default option (when no placeholder was defined)
🐛 Fixed missing translations for the multi-select dropdown block

**23-09-2022**
🐛 Fixed a bug where scores for the matrix block were not calculated properly

**21-09-2022**
✔️ Added color setting for multiple-choice, picture-choice, and yes/no buttons

**20-09-2022**
✔️ Added color picker to the form styles editor

**28-07-2022**
✔️ Added multi-select dropdown block
✔️ Added the ability to add branches without sections (right-click or hold the `+` button in the builder and select `Insert branch` to directly add a new branch)
⚡ Added right-click mouse support in the builder (you can now right-click on any item to reveal the context menu with additional options)
🐛 Fixed a bug where tracking events would not emit when using a global Google Analytics tracker code

**27-05-2022**
⚡ Added more alignment options to the multiple-choice block

**16-05-2022**
✔️ Added denylist to block accounts

**25-02-2022**
⚡ Migrate Integromat to Make

**17-02-2022**
✔️ Added Google Tag Manager support for form tracking
✔️ Added an option to configure trackers to use global tracker codes when embedding forms
🐛 Fixed a bug where icons in buttons would turn the wrong color when hovering over the button
🐛 Fixed a bug in the required feature of the matrix block

**15-02-2022**
✔️ Added an option to store a concatenated text with all the selected options in the dataset (available for the checkboxes, multiple-choice, and picture-choice blocks)
⚡ Improved the exportability feature
🐛 Fixed submission issues when using Safari with response data that contains combining diacritical marks

**22-11-2021**
✔️ Added limits feature (minimum/maximum text length) to multi-line text

**21-10-2021**
⚡ Improved chat button visibility for small devices

**14-10-2021**
🐛 Fixed a bug where the builder stopped working when an evaluate block was added after a cluster without any nodes

**14-09-2021**
⚡ Added a style setting to the runners to customize the font size for small screens (mobile devices)
🐛 Fixed a bug where pausing the form with an invalid email address would freeze the form

**02-09-2021**
🐛 Fixed a bug in the address validation of the email block
🐛 Fixed a bug in the calculator where an outcome was not always available correctly
🐛 Fixed a bug where duplicate fields where shown

**29-07-2021**
✔️ Added tracking support for Google Analytics, Facebook Pixel or a custom tracker code (see https://tripetto.com/blog/new-feature-form-activity-tracking-with-google-analytics-and-facebook-pixel/)
✔️ Added Zapier feature to the connections panel
✔️ Added Integromat feature to the connections panel
✔️ Added Pably Connect feature to the connections panel
✔️ Added feature to share panel to set the CDN for the embed code
⚡ Improved the automate menu (there are now separate panels for managing notifications, connections and tracking)
⚡ Hide Tripetto branding in emails for forms that have branding disabled
⚡ Improved trackpad support for the form builder (now supports panning in all directions)
⚡ Improved test function for Slack notifications and webhooks (fields of the form are now submitted as test data)

**20-07-2021**
✔️ Added limiting feature to limit the number of selected options (available for the checkboxes, multiple choice, and picture choice blocks)
✔️ Added randomization feature (available for the dropdown, checkboxes, multiple choice, picture choice, and radio buttons blocks)
✔️ Added counter slot that counts the number of selected options (available for the checkboxes, multiple choice, and picture choice blocks)
🐛 Fixed a bug where recalling values of other blocks didn't work properly

**29-06-2021**
✔️ Added new branch mode for checking if none of the conditions match (logical NOT)
✔️ Added automatic video playback and pausing
✔️ Added a new constant to the calculator to find the current branch index number
⚡ Improved block type selection when adding a new block
⚡ Added a style setting to remove the asterisk for required blocks (only applies to the autoscroll and classic runner)
⚡ Improved variables support in URLs so you can use a variable to specify the protocol and domain part of an URL
⚡ Improved the multiple choice and picture choice blocks so you can select a hyperlink target for URL choices
🐛 Fixed a typo in the mailer block

**31-05-2021**
⚡ Improved the URL block
🐛 Fixed a data regression bug causing some forms to not work properly

**27-05-2021**
🐛 Fixed a bug in the closing message of the classic runner
🐛 Fixed a bug in the capitalize function (see https://gitlab.com/tripetto/studio/-/issues/40)

**20-05-2021**
✔️ Added count occurrences function to the calculator block
✔️ Added concatenate option to the set value block
✔️ Added additional alias options to the yes/no block
✔️ Added an option to disable scrolling in the autoscroll runner
✔️ Implemented Trusted Types support (see https://web.dev/trusted-types/)
✔️ Added German translation for the runners (form faces)
✔️ Added French translation for the runners (form faces)
✔️ Added Spanish translation for the runners (form faces)
✔️ Added Portuguese translation for the runners (form faces)
✔️ Added Indonesian translation (🙏 https://gitlab.com/hisamafahri)
⚡ Improved performance for very large forms
⚡ Improved the live preview of the classic runner
⚡ Improved usability of the set value block
🐛 Fixed a bug in the number field where the formatting was not applied
🐛 Fixed a bug in the calculator feature of the number block
🐛 Fixed a bug in the classic runner where sections with no blocks (or only invisible blocks) caused paginated forms to stop working
🐛 Fixed an accessibility issue with the dropdown lists in the runners (form faces)

**17-02-2021**
✔️ Added an import/export function to allow easy adding of multiple items at once (for example, dropdown options, multiple choice buttons, text suggestions, etc.)
✔️ Added a custom variable block that allows the use of custom variables
✔️ Added a set value block that allows to set field values or other variables
✔️ Added suggestions support in the single line text block so you can specify a list of pre-defined options for the text input
✔️ Added a score feature to blocks so you can calculate scores directly without the need of a separate calculator block (this works for the blocks checkbox, checkboxes, dropdown, matrix, multiple choice, picture choice, radiobuttons, scale, single line text and yes/no)
✔️ Added a calculator option to the number block, so you can make successive calculations directly without the need of a separate calculator block
✔️ Added a prefill setting to the number, single line text and multiple lines text blocks so you can set an initial value for those fields (if you want to set an initial value for other blocks, use the new set value block)
✔️ Added a minimum required text length option to the single line text block
⚡ Enabled a scrollbar for menus with lots of options (you can grab the scrollbar to quickly scroll through all items in the menu)
⚡ Changed the initial behavior of a subcalculation within a calculator block: The initial value is now set to the last answer (ANS) of the parent calculation instead of an empty value (please check your form if you are using subcalculations as this change might break things for you)
⚡ Improved the calculator behavior when using variables that have no value yet as input (this will only lead to an invalid calculation when that variable is used as initial input, for multiplication, or for division, otherwise the variable input will be considered 0)
🐛 Fixed a keyboard bug in MacOS Safari where the text cursor sometimes jumped to end of a line (https://gitlab.com/tripetto/studio/-/issues/28)
🐛 Fixed a bug where the label of a node was not displayed properly (https://gitlab.com/tripetto/studio/-/issues/31)
🐛 Fixed a bug in the allowed file types setting of the file upload block
🐛 Fixed a bug in the closing message where sometimes the wrong message was shown
🐛 Fixed a bug in the radio button block in the chat runner
🐛 Fixed a query string bug in Internet Explorer

**13-01-2021**
🐛 Fixed the mobile keyboard for numeric fields with decimal precision

**12-01-2021**
✔️ Added a calculator block that allows (advanced) calculations inside forms (see https://tripetto.com/no-code-calculations-with-the-calculator-block/)
✔️ Added a stop block that can be used to prevent completion of a form
⚡ Improved condition logic (you can now use values of other blocks)
⚡ Improved switching between block types (more block settings are now retained)
⚡ Improved markdown support for checkboxes and radio buttons
⚡ Implemented variables support in dropdown options, multiple choice items and picture choice items
⚡ Implemented an option to set readable labels for boolean (true/false) values
🐛 Fixed an alignment bug in the mobile view of the Autoscroll runner
🐛 Fixed the usage of variables inside image/video URLs
🐛 Fixed a bug where the identifier of a form response was not available in the closing message of nested branches
🐛 Fixed a bug where an illegal custom font name would lead to a runner fault
🐛 Fixed a bug in the file upload condition block

**13-11-2020**
🐛 Fixed a bug in branch iteration logic
🐛 Fixed a bug in scale block

**04-11-2020**
🐛 Fixed a bug in the translation editor

**21-10-2020**
⚡ Improved error messages
🐛 Fixed a bug related to font ligatures

**13-10-2020**
🐛 Fixed a bug where the Hindi keyboard on iOS could not be used properly

**10-10-2020**
✔️ Added picture choice block
✔️ Added scale block
⚡ Added more shapes to the rating block
⚡ Improved rating block conditions
⚡ Improved URL block (if the protocol prefix is missing, it is added automatically on blur)
⚡ Switched to RFC5322 for email validation (which is more strict)
⚡ Auto-focus is now set to the selected button/option whenever possible
🐛 Fixed a bug in the web font loader

**02-10-2020**
🐛 Fixed a bug where the font `Fira Sans` could not be used in Firefox
🐛 Added missing label in translation settings for the autoscroll runner

**30-09-2020**
✔️ Added a style option to set the opacity of the background image in the runners
⚡ Improved the multiple choice buttons (they now wrap to the next line when necessary)
⚡ Automatically remove empty checkboxes, multiple choice buttons and radio buttons when in live or test mode (preview mode still shows placeholders for empty options)
🐛 Fixed the z-index of the chat runner in inline mode

**23-07-2020**
✔️ Added chat runner
✔️ Added classic runner
✔️ Added custom welcome message support
✔️ Added custom closing message support
✔️ Added translation support for static labels and texts in forms
✔️ Added date/time block
✔️ Added telephone block
✔️ Added error action block
✔️ Added evaluate condition block
✔️ Added regex condition block
✔️ Added identification number of each result to the results list
⚡ Migrated rolling collector to autoscroll runner
⚡ Improved mailer block
⚡ Improved styling options to allow more customizations
⚡ Upgraded builder to latest and greatest version (lot's of imrovements under the hood there)
⚡ Improved SPAM-protection
⚡ Improved automation tests
🐛 Fixed a bug in the validation of empty number fields
🐛 Fixed a bug while filling out number fields in Firefox browser
🐛 Fixed embed codes to allow multiple forms on a single page
🐛 Fixed the unwanted behavior of the first radio button getting selected for required radio button questions
🐛 Fixed a bug on Android devices having difficulty with showing the soft-keyboard while filling out the form
🐛 Fixed the unwanted ability to create 'infinite loops' inside your form, resulting in a freezing form

**05-06-2020**
⚡ Improve services package to support multiple forms on a single page

**17-07-2019**
✔️ Added hidden field block (this allows the use of hidden fields in forms and also logic based on the value of those hidden fields)
🐛 Fixed a bug to let the app work on IE11

**16-07-2019**
✔️ Added the "Send an email"-block to send emails from within forms
🐛 Fixed a bug in keyboard navigation in the collector

**10-07-2019**
✔️ Added single checkbox block
✔️ Added multiple checkboxes block
✔️ Added radio buttons block
✔️ Added the possibility to hide the title of a block
✔️ Added ability to change the labels of the `Next` and `Complete` buttons in the form style panel
⚡ Renamed `Next` button to `Ok` button
⚡ Automatically make blocks required when the required feature is enabled (this saves an additional click by the user)
🐛 Fixed a bug in the positioning of the empty message of the collector

**06-05-2019**
✔️ Short URL's for shared surveys/forms
✔️ Share as template (allows sharing form designs with others)
⚡ Display email address when user signs in (to give feedback to the user where the magic link is going)
