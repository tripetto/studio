const pkg = require("../../package.json");
const services = require("../../src/services/package.json");

module.exports = `${services.title} ${pkg.version} - Copyright (C) ${new Date().getFullYear()} Tripetto B.V. - All Rights Reserved`;
