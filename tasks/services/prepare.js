const pkg = require("../../package.json");
const services = require("../../services/package.json");
const fs = require("fs");
const prettier = require("prettier");

services.version = pkg.version;
services.repository = pkg.repository;
services.author = pkg.author;
services.license = pkg.license;
services.homepage = pkg.homepage;
services.private = pkg.version;
services.private = false;
services.scripts = {};

console.log(`Services package version: ${services.version}`);

fs.writeFileSync(
    "./services/package.json",
    prettier.format(JSON.stringify(services), {
        parser: "json",
    }),
    "utf8"
);
