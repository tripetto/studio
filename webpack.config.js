const webpack = require("webpack");
const webpackLiveReload = require("webpack-livereload-plugin");
const webpackTerser = require("terser-webpack-plugin");
const webpackCopy = require("copy-webpack-plugin");
const webpackReplaceHash = require("replace-hash-in-file-webpack-plugin");
const path = require("path");
const banner = require("./tasks/banner/banner.js");
const pkg = require("./package.json");
const production = process.argv.includes("production");
const analyzer = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

const app = {
    target: ["web", "es5"],
    entry: "./src/app/index.ts",
    output: {
        filename: production ? "app-[fullhash].js" : "app-dev.js",
        path: path.resolve(__dirname, "static", "js"),
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                options: {
                    configFile: "tsconfig-app.json",
                    compilerOptions: {
                        noEmit: false,
                    },
                },
            },
            {
                test: /\.(graphql|gql)$/,
                exclude: /node_modules/,
                loader: "graphql-tag/loader",
            },
            {
                test: /\.svg$/,
                use: ["url-loader", "image-webpack-loader"],
            },
        ],
    },
    resolve: {
        extensions: [".ts", ".js", ".graphql"],
        alias: {
            "@app": path.resolve(__dirname, "./src/app"),
            "@server": path.resolve(__dirname, "./src/server"),
            "@services": path.resolve(__dirname, "./src/services"),
        },
    },
    performance: {
        hints: false,
    },
    optimization: {
        minimizer: [
            new webpackTerser({
                terserOptions: {
                    format: {
                        preamble: `/*! ${banner} */`,
                        comments: false,
                    },
                },
                extractComments: false,
            }),
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            PACKAGE_NAME: JSON.stringify(pkg.name),
            PACKAGE_TITLE: JSON.stringify(pkg.title),
            PACKAGE_VERSION: JSON.stringify(pkg.version),
        }),
        new webpackCopy({
            patterns: [
                {
                    from: "src/server/views/",
                    to: "../../views/",
                    globOptions: {
                        ignore: ["**/pages/runner/autoscroll.ejs", "**/pages/runner/chat.ejs", "**/pages/runner/classic.ejs"],
                    },
                },
                { from: "node_modules/@tripetto/builder/fonts/", to: "../fonts/" },
                { from: "node_modules/@tripetto/builder/locales/", to: "../../locales/" },
            ],
        }),
        new webpackReplaceHash([
            {
                dir: "views/pages",
                files: ["app.ejs"],
                rules: [
                    {
                        search: /\[hash\]/,
                        replace: production ? "[hash]" : "dev",
                    },
                ],
            },
        ]),
        ...(!production
            ? [
                  new webpackLiveReload({
                      appendScriptTag: true,
                  }),
              ]
            : [
                  new analyzer({
                      analyzerMode: "static",
                      reportFilename: "../../reports/bundle-app.html",
                      openAnalyzer: false,
                  }),
              ]),
    ],
    devtool: (!production && "cheap-module-source-map") || undefined,
};

const runners = ["autoscroll", "chat", "classic"].map((runner) => ({
    target: ["web", "es5"],
    entry: `./src/runners/${runner}.ts`,
    output: {
        filename: production ? "bundle-[fullhash].js" : "bundle-dev.js",
        path: path.resolve(__dirname, "static", "js", runner),
        library: {
            name: "Tripetto",
            type: "umd",
        },
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                loader: "ts-loader",
                options: {
                    configFile: "tsconfig-runners.json",
                    compilerOptions: {
                        noEmit: false,
                    },
                },
            },
        ],
    },
    resolve: {
        extensions: [".ts", ".js"],
        alias: {
            "@services": path.resolve(__dirname, "./src/services"),
            "@server": path.resolve(__dirname, "./src/server"),
        },
    },
    performance: {
        hints: false,
    },
    optimization: {
        minimizer: [
            new webpackTerser({
                terserOptions: {
                    format: {
                        preamble: `/*! ${banner} */`,
                        comments: false,
                    },
                },
                extractComments: false,
            }),
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            PACKAGE_NAME: JSON.stringify(pkg.name),
            PACKAGE_TITLE: JSON.stringify(pkg.title),
            PACKAGE_VERSION: JSON.stringify(pkg.version),
        }),
        new webpackCopy({
            patterns: [
                { from: `src/server/views/pages/runner/${runner}.ejs`, to: `../../../views/pages/runner/${runner}.ejs` },
                { from: `node_modules/@tripetto/runner-${runner}/runner/index.js`, to: "runner.js" },
                { from: `node_modules/@tripetto/runner-${runner}/builder/index.js`, to: "builder.js" },
            ],
        }),
        new webpackReplaceHash([
            {
                dir: "views/pages/runner",
                files: [`${runner}.ejs`],
                rules: [
                    {
                        search: /\[hash\]/,
                        replace: production ? "[hash]" : "dev",
                    },
                ],
            },
        ]),
        ...(production
            ? [
                  new analyzer({
                      analyzerMode: "static",
                      reportFilename: `../../../reports/bundle-runner-${runner}.html`,
                      openAnalyzer: false,
                  }),
              ]
            : []),
    ],
    devtool: (!production && "cheap-module-source-map") || undefined,
}));

const services = (target) => {
    return {
        target: ["web", "es5"],
        entry: "./src/services/index.ts",
        output: {
            filename: "index.js",
            path: path.resolve(__dirname, "services", "dist", target),
            library:
                target === "umd"
                    ? {
                          name: "TripettoStudio",
                          type: "umd",
                          export: "default",
                      }
                    : {
                          type: "commonjs2",
                      },
            globalObject: (target === "umd" && "this") || undefined,
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    loader: "ts-loader",
                    options: {
                        configFile: "tsconfig-services.json",
                        compilerOptions: {
                            noEmit: false,
                        },
                    },
                },
            ],
        },
        resolve: {
            extensions: [".ts", ".js"],
            alias: {
                "@server": path.resolve(__dirname, "./src/server"),
            },
        },
        externals: {
            "@tripetto/builder": target === "umd" ? "Tripetto" : "commonjs @tripetto/builder",
            "@tripetto/runner": target === "umd" ? "TripettoRunner" : "commonjs @tripetto/runner",
        },
        performance: {
            hints: false,
        },
        optimization: {
            minimizer: [
                new webpackTerser({
                    terserOptions: {
                        format: {
                            preamble: `/*! ${banner} */`,
                            comments: false,
                        },
                    },
                    extractComments: false,
                }),
            ],
        },
        plugins: [
            new webpack.DefinePlugin({
                PACKAGE_NAME: JSON.stringify(require("./src/services/package.json").name),
                PACKAGE_TITLE: JSON.stringify(require("./src/services/package.json").title),
                PACKAGE_VERSION: JSON.stringify(pkg.version),
            }),
            new webpackCopy({
                patterns: [
                    { from: "src/services/package.json", to: "../../" },
                    { from: "src/services/.npmignore", to: "../../" },
                    { from: "src/services/README.md", to: "../../" },
                ],
            }),
            ...(production
                ? [
                      new analyzer({
                          analyzerMode: "static",
                          reportFilename: `../../../reports/bundle-services-${target}.html`,
                          openAnalyzer: false,
                      }),
                  ]
                : []),
        ],
    };
};

module.exports = (env, argv) => {
    return [app, ...runners, ...(production ? [services("umd"), services("es5")] : [])];
};
