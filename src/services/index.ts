import Promise from "promise-polyfill";
import { Export, IDefinition, ISnapshot, Instance, L10n, TAny, TL10n, TStyles, isPromise, isString } from "@tripetto/runner";
import { FILESTORE_URL, URL } from "./settings";
import { getDefinition, getL10n, getLicense, getStyles } from "./definitions";
import { getSnapshot, saveSnapshot } from "./snapshots";
import { getLocale } from "./locales";
import { getTranslation } from "./translations";
import { submitResponse } from "./responses";
import { deleteFile, downloadFile, uploadFile } from "./files";

interface IAttachments {
    readonly get: (file: string) => Promise<Blob>;
    readonly put: (file: File, onProgress: (percentage: number) => void) => Promise<string>;
    readonly delete: (file: string) => Promise<void>;
}

export default class Services<Snapshot = unknown> {
    /** Static services instance (for backwards compatibility). */
    private static services?: Services;

    /** Specifies the token to send along with a server request.  */
    private token: string;

    /** Specifies the token signature. */
    private signature?: string;

    /** Specifies the url of the server.  */
    private url: string;

    /** Specifices the url of the filestore. */
    private filestoreUrl: string;

    private static assert(): Services {
        if (!this.services) {
            throw new Error("Services are not initialized.");
        }

        return this.services;
    }

    /** Initialize a services instance. */
    static init<Snapshot = unknown>(props: {
        /** Specifies the token. */
        readonly token: string;

        /** Specifies an optional signature of the owner of the form (this allows to only load forms that are attached to a certain account or domain). */
        readonly signature?: string;

        /** Specifies the url of the server. */
        readonly url?: string;

        /** Specifies the url of the filestore service. */
        readonly filestoreUrl?: string;
    }): Services<Snapshot> {
        return (Services.services = new Services<Snapshot>(props));
    }

    /** Creates an internal services instance. */
    static internal<Snapshot = unknown>(props?: { readonly token?: string; readonly filestoreUrl?: string }): Services<Snapshot> {
        return new Services<Snapshot>({ ...props, url: "" });
    }

    /** Retrieves a Studio form instance. */
    static get<Snapshot = unknown>(
        props: {
            [key: string]: TAny;
        } & {
            /** Specifies the token. */
            readonly token: string;

            /** Specifies an optional signature of the owner of the form (this allows to only load forms that are attached to a certain account or domain). */
            readonly signature?: string;

            /** Specifies the element for the form. */
            readonly element?: string | HTMLElement | null;

            /** Specifies the url of the server. */
            readonly url?: string;

            /** Specifies the url of the filestore service. */
            readonly filestoreUrl?: string;

            /** Specifies to store sessions in the local store to preserve persistency when navigating between multiple pages that host the runner. */
            readonly persistent?: boolean;

            /** Specifies if the form can be paused and resumed. */
            readonly pausable?: boolean;

            /** Specifies a tracking function that is invoked when the user performs an action. */
            readonly trackers?: (
                type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
                definition: {
                    readonly fingerprint: string;
                    readonly name: string;
                },
                block?: {
                    readonly id: string;
                    readonly name: string;
                }
            ) => void;

            /** Specifies a function that is invoked when the form is submitted. */
            readonly onSubmit?: (
                instance: Instance,
                id: string,
                language: string,
                locale: string,
                namespace?: string
            ) => Promise<void> | void;
        }
    ): {
        readonly element?: HTMLElement | null;
        readonly definition?: Promise<IDefinition | undefined>;
        readonly snapshot?: Promise<ISnapshot<Snapshot> | undefined>;
        readonly styles?: Promise<TStyles | undefined>;
        readonly l10n?: Promise<TL10n | undefined>;
        readonly license?: Promise<string | undefined>;
        readonly locale?: (locale: "auto" | string) => Promise<L10n.ILocale | undefined>;
        readonly translations?: (
            language: "auto" | string,
            context: string
        ) => Promise<L10n.TTranslation | L10n.TTranslation[] | undefined>;
        readonly attachments?: IAttachments;
        readonly persistent?: boolean;
        readonly onSubmit?: (
            instance: Instance,
            language: string,
            locale: string,
            runner?: string
        ) => Promise<string | undefined> | boolean | void;
        readonly onPause?: {
            recipe: "email";
            onPause: (
                emailAddress: string,
                snapshot: ISnapshot<Snapshot>,
                language: string,
                locale: string,
                runner: string
            ) => Promise<void>;
        };
        readonly onReload?: () => Promise<IDefinition>;
        readonly onAction?: (
            type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
            definition: {
                readonly fingerprint: string;
                readonly name: string;
            },
            block?: {
                readonly id: string;
                readonly name: string;
            }
        ) => void;
    } {
        const services = Services.init<Snapshot>(props);

        return {
            onAction: props.trackers || undefined,
            ...props,
            element: isString(props.element) ? document.getElementById(props.element) : props.element || null,
            definition: services.definition,
            styles: services.styles,
            l10n: services.l10n,
            license: services.license,
            locale: services.locale,
            translations: services.translations,
            attachments: services.attachments,
            persistent: props.persistent,
            snapshot: (props.pausable && services.snapshot) || undefined,
            onPause: (props.pausable && services.onPause) || undefined,
            onSubmit:
                (props.onSubmit &&
                    ((instance: Instance, language: string, locale: string, runner?: string) =>
                        new Promise<string | undefined>((resolve: (id: string | undefined) => void, reject: (reason?: string) => void) => {
                            services
                                .onSubmit(instance, language, locale, runner)
                                .then((id) => {
                                    if (id) {
                                        const result = props.onSubmit!(instance, id, language, locale, runner);

                                        if (isPromise(result)) {
                                            result.then(() => resolve(id)).catch(() => resolve(id));
                                        } else {
                                            resolve(id);
                                        }
                                    } else {
                                        resolve(id);
                                    }
                                })
                                .catch((reason) => reject(reason));
                        }))) ||
                services.onSubmit,
            onReload: services.onReload,
        };
    }

    /** Initializes a form runner. */
    static form<
        Runner extends { readonly run: (props: Props) => Promise<Controller> },
        Props extends {},
        Controller extends {},
        Snapshot = unknown
    >(
        props: Props & {
            /** Specifies the runner to use. */
            readonly runner: Runner;

            /** Specifies the token. */
            readonly token: string;

            /** Specifies an optional signature of the owner of the form (this allows to only load forms that are attached to a certain account or domain). */
            readonly signature?: string;

            /** Specifies the element for the form. */
            readonly element?: string;

            /** Specifies the url of the server. */
            readonly url?: string;

            /** Specifies the url of the filestore service. */
            readonly filestoreUrl?: string;

            /** Specifies to store sessions in the local store to preserve persistency when navigating between multiple pages that host the runner. */
            readonly persistent?: boolean;

            /** Specifies if the form can be paused and resumed. */
            readonly pausable?: boolean;

            /** Specifies a tracking function that is invoked when the user performs an action. */
            readonly trackers?: (
                type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
                definition: {
                    readonly fingerprint: string;
                    readonly name: string;
                },
                block?: {
                    readonly id: string;
                    readonly name: string;
                }
            ) => void;

            /** Specifies a function that is invoked when the form is submitted. */
            readonly onSubmit?: (
                instance: Instance,
                id: string,
                language: string,
                locale: string,
                namespace?: string
            ) => Promise<void> | void;
        }
    ): Promise<Controller> {
        return props.runner.run(this.get<Snapshot>(props) as Props);
    }

    static get definition(): Promise<IDefinition | undefined> {
        return Services.assert().definition;
    }

    static get style(): Promise<TStyles | undefined> {
        return Services.assert().styles;
    }

    static get onAttachment(): IAttachments {
        return Services.assert().attachments;
    }

    static onFinish(instance: Instance, language?: string, locale?: string): void {
        Services.assert().onSubmit(instance, language || "", locale || "");
    }

    /** Creates a new services instance. */
    constructor(props: { readonly token?: string; readonly signature?: string; readonly url?: string; readonly filestoreUrl?: string }) {
        this.token = props.token || "";
        this.signature = props.signature;
        this.url = typeof props.url === "string" ? props.url : URL;
        this.filestoreUrl = props.filestoreUrl || FILESTORE_URL;
    }

    /** Verifies whether the token is set. */
    private requireToken(): string {
        if (!this.token) {
            throw new Error("Token is required, but not available.");
        }

        return this.token;
    }

    /** Handles retrieving an attachment from the filestore.
     * @param file The file to be retrieved.
     */
    private onAttachmentGet(file: string): Promise<Blob> {
        return downloadFile(this.filestoreUrl, this.requireToken(), file);
    }

    /** Handles uploading an attachment to the filestore.
     * @param file The file to be uploaded.
     * @param onProgress Function for handling the progress of the file upload.
     */
    private onAttachmentPut(file: File, onProgress: (percentage: number) => void): Promise<string> {
        return uploadFile(this.filestoreUrl, this.requireToken(), file, onProgress);
    }

    /** Handles deleting an attachment from the filestore.
     * @param file The file to be deleted.
     */
    private onAttachmentDelete(file: string): Promise<void> {
        return deleteFile(this.filestoreUrl, this.requireToken(), file);
    }

    /** Fetches the definition from the server. */
    get definition(): Promise<IDefinition | undefined> {
        return getDefinition(this.url, this.requireToken(), this.signature);
    }

    /** Fetches the snapshot from the server. */
    get snapshot(): Promise<ISnapshot<Snapshot> | undefined> {
        return getSnapshot(this.url, this.requireToken());
    }

    /** Fetches the styles from the server. */
    get styles(): Promise<TStyles | undefined> {
        return getStyles(this.url, this.requireToken(), this.signature);
    }

    /** Fetches the l10n data from the server. */
    get l10n(): Promise<TL10n | undefined> {
        return getL10n(this.url, this.requireToken(), this.signature);
    }

    /** Fetches the license from the server. */
    get license(): Promise<string | undefined> {
        return getLicense(this.url, this.requireToken(), this.signature);
    }

    /** Retrieves the attachments handler. */
    get attachments(): IAttachments {
        return {
            get: (file: string) => this.onAttachmentGet(file),
            put: (file: File, onProgress: (percentage: number) => void) => this.onAttachmentPut(file, onProgress),
            delete: (file: string) => this.onAttachmentDelete(file),
        };
    }

    /** Retrieves locale data from the server. */
    get locale(): (locale: "auto" | string) => Promise<L10n.ILocale | undefined> {
        return (locale: "auto" | string) => getLocale(this.url, locale);
    }

    /** Retrieves translations from the server. */
    get translations(): (language: "auto" | string, context: string) => Promise<L10n.TTranslation | L10n.TTranslation[] | undefined> {
        return (language: "auto" | string, context: string) => getTranslation(this.url, language, context);
    }

    /** Saves submitted responses to the server. */
    get onSubmit(): (instance: Instance, language: string, locale: string, runner?: string) => Promise<string | undefined> {
        return (instance: Instance, language: string, locale: string, runner?: string) =>
            submitResponse(
                this.url,
                this.requireToken(),
                Export.exportables(instance),
                Export.actionables(instance),
                language,
                locale,
                runner || ""
            );
    }

    /** Saves the snapshot to the server. */
    get onPause(): {
        recipe: "email";
        onPause: (emailAddress: string, snapshot: ISnapshot<Snapshot>, language: string, locale: string, runner: string) => Promise<void>;
    } {
        return {
            recipe: "email",
            onPause: (emailAddress: string, snapshot: ISnapshot<Snapshot>, language: string, locale: string, runner: string) =>
                saveSnapshot(this.url, this.requireToken(), snapshot, emailAddress, language, locale, runner),
        };
    }

    /** Reloads the definition from the server. */
    get onReload(): () => Promise<IDefinition> {
        return () =>
            new Promise((resolve: (definition: IDefinition) => void, reject: () => void) => {
                getDefinition(this.url, this.requireToken())
                    .then((definition) => {
                        if (definition) {
                            resolve(definition);
                        } else {
                            reject();
                        }
                    })
                    .catch(() => reject());
            });
    }
}
