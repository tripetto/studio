import Promise from "promise-polyfill";
import * as Superagent from "superagent";
import { IDefinition, SHA2, TL10n, TStyles, calculateFingerprintAndStencil, setAny } from "@tripetto/runner";
import { noCacheUrl } from "../no-cache";
import { HEADER_RUNNER_SIGNATURE, HEADER_RUNNER_TOKEN } from "@server/headers";
import { DEFINITION, L10N, LICENSE, STYLES } from "@server/endpoints";

/**
 * Gets a definition from the server.
 * @param url Specifies the base url.
 * @param token The token that represents the definition.
 * @param signature Specifies the optional signature for the form.
 */
export const getDefinition = (url: string, token: string, signature?: string) =>
    new Promise<IDefinition | undefined>((resolve: (definition: IDefinition | undefined) => void) => {
        Superagent.get(noCacheUrl(url + DEFINITION, token))
            .set({
                [HEADER_RUNNER_TOKEN]: token,
                ...(signature
                    ? {
                          [HEADER_RUNNER_SIGNATURE]: signature,
                      }
                    : {}),
                "Cache-Control": "no-store",
            })
            .then((response: Superagent.Response) => {
                if (typeof window !== "undefined" && response.body?.definition) {
                    const fingerprint = calculateFingerprintAndStencil(response.body?.definition).fingerprint;

                    setAny(window, `tripetto-${SHA2.CSHA2_256(token + fingerprint)}`, fingerprint);
                }

                resolve(response.body?.definition);
            })
            .catch(() => resolve(undefined));
    });

/**
 * Gets a style from the server.
 * @param url Specifies the base url.
 * @param token The token that represents the definition the styles belongs to.
 * @param signature Specifies the optional signature for the form.
 */
export const getStyles = (url: string, token: string, signature?: string) =>
    new Promise<TStyles | undefined>((resolve: (styles: TStyles | undefined) => void) => {
        Superagent.get(noCacheUrl(url + STYLES, token))
            .set({
                [HEADER_RUNNER_TOKEN]: token,
                ...(signature
                    ? {
                          [HEADER_RUNNER_SIGNATURE]: signature,
                      }
                    : {}),
                "Cache-Control": "no-store",
            })
            .then((response: Superagent.Response) => resolve(response.body?.styles))
            .catch(() => resolve(undefined));
    });

/**
 * Gets a l10n data from the server.
 * @param url Specifies the base url.
 * @param token The token that represents the definition the l10n data belongs to.
 * @param signature Specifies the optional signature for the form.
 */
export const getL10n = (url: string, token: string, signature?: string) =>
    new Promise<TL10n | undefined>((resolve: (l10n: TL10n | undefined) => void) => {
        Superagent.get(noCacheUrl(url + L10N, token))
            .set({
                [HEADER_RUNNER_TOKEN]: token,
                ...(signature
                    ? {
                          [HEADER_RUNNER_SIGNATURE]: signature,
                      }
                    : {}),
                "Cache-Control": "no-store",
            })
            .then((response: Superagent.Response) => resolve(response.body?.l10n))
            .catch(() => resolve(undefined));
    });

/**
 * Gets the license from the server.
 * @param url Specifies the base url.
 * @param token The token that represents the definition the license belongs to.
 * @param signature Specifies the optional signature for the form.
 */
export const getLicense = (url: string, token: string, signature?: string) =>
    new Promise<string | undefined>((resolve: (license: string | undefined) => void) => {
        Superagent.get(noCacheUrl(url + LICENSE, token))
            .set({
                [HEADER_RUNNER_TOKEN]: token,
                ...(signature
                    ? {
                          [HEADER_RUNNER_SIGNATURE]: signature,
                      }
                    : {}),
                "Cache-Control": "no-store",
            })
            .then((response: Superagent.Response) => resolve(response.body?.license))
            .catch(() => resolve(undefined));
    });
