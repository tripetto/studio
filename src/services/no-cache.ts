import { SHA2 } from "@tripetto/runner";

export function noCacheUrl(url: string, token: string): string {
    return url + (url.indexOf("?") === -1 ? "?" : "&") + SHA2.SHA2_256(token).substr(0, 20);
}
