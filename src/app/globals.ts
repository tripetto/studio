/** Globals. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;
declare const PACKAGE_TITLE: string;
declare const APP: {
    ENV: "production" | "staging" | "development";
    LICENSE?: string;
    URL: string;
    FILESTORE: string;
    DSN?: string;
    TEMPLATE?: string;
};

/** Hash of bundle. */
declare const __webpack_hash__: string;

/** Contains the application name. */
export const NAME = PACKAGE_NAME;

/** Contains the application title. */
export const TITLE = PACKAGE_TITLE;

/** Contains the version number. */
export const VERSION = PACKAGE_VERSION;

/** Contains the hash of the bundle. */
export const HASH = __webpack_hash__ || "";

/** Contains the current environment of the application. */
export const ENV = APP.ENV;

/** Contains the URL of the application. */
export const URL = APP.URL;

/** Contains the license for the application. */
export const LICENSE = APP.LICENSE;

/** Contains the URL of the filestore. */
export const FILESTORE_URL = APP.FILESTORE;

/** Contains the Sentry DSN. */
export const SENTRY_DSN = APP.DSN;

/** Contains the id of the template definition. */
export const DEFINITION_TEMPLATE = APP.TEMPLATE;
