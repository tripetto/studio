import { StudioComponent } from "@app/components/studio";
import { Components, L10n, Layers, Num, each, map, npgettext, pgettext } from "@tripetto/builder";
import { IResponse } from "@server/entities/responses";
import { Loader } from "@app/components/loader";
import { DialogComponent } from "@app/components/dialog";
import { query } from "@app/helpers/api";
import { Result } from "./result";
import { ResultsList } from "./list";
import { URL } from "@app/globals";
import { EXPORT } from "@server/endpoints";
import { HELP_EXPORT, HELP_RESULTS } from "@app/urls";
import ReadResponsesQuery from "../../queries/responses/read-all.graphql";

type ResultViews = "normal" | "delete";

export class ResultsComponent extends Components.Controller<
    {
        definitionId: string | undefined;
        style: Components.IListStyle;
    },
    ResultViews
> {
    private readonly refreshButton: Components.ToolbarButton<ResultViews> | undefined;
    private readonly toggleButton: Components.ToolbarButton<ResultViews> | undefined;
    private readonly exportButton: Components.ToolbarMenu<ResultViews> | undefined;
    private readonly deleteButton: Components.ToolbarButton<ResultViews> | undefined;
    private list!: ResultsList;
    readonly id?: string;
    readonly studio: StudioComponent;
    whenReady?: () => void;
    whenUpdated?: (resultsCount: number) => void;
    whenClosed?: () => void;

    static open(studio: StudioComponent, definitionId?: string): ResultsComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new ResultsComponent(panel, studio, definitionId),
            Layers.Layer.configuration.width(600).animation(Layers.LayerAnimations.Zoom).autoCloseChildPanels("tap")
        );
    }

    private constructor(layer: Layers.Layer, studio: StudioComponent, definitionId?: string) {
        const refreshButton = new Components.ToolbarButton<ResultViews>(studio.style.results.buttons.refresh, undefined, undefined, () =>
            this.fetch()
        )
            .disable()
            .excludeInViews("delete");
        const toggleButton = new Components.ToolbarButton<ResultViews>(
            studio.style.results.buttons.delete.withoutLabel,
            undefined,
            undefined,
            () => this.toggle()
        )
            .disable()
            .excludeInViews("delete");
        const deleteButton = new Components.ToolbarButton<ResultViews>(
            studio.style.results.buttons.delete.withLabel,
            pgettext("studio", "Delete"),
            undefined,
            () => this.delete()
        )
            .disable()
            .includeInViews("delete");
        const doneButton = new Components.ToolbarButton<ResultViews>(
            studio.style.forms.header.buttons.close,
            pgettext("studio", "Done"),
            undefined,
            () => this.toggle()
        ).includeInViews("delete");
        const exportButton = new Components.ToolbarMenu<ResultViews>(
            studio.style.results.buttons.export,
            pgettext("studio", "Download"),
            () => this.parseExports()
        )
            .disable()
            .excludeInViews("delete");

        super(
            layer,
            {
                definitionId,
                style: studio.style.results.list,
            },
            pgettext("studio", "Results"),
            "compact",
            studio.style,
            "right",
            "always-on",
            pgettext("studio", "Close"),
            [
                refreshButton,
                new Components.ToolbarLink<ResultViews>(studio.style.results.buttons.help, HELP_RESULTS).excludeInViews("delete"),
                exportButton,
                toggleButton,
                deleteButton,
                doneButton,
            ]
        );

        this.refreshButton = refreshButton;
        this.toggleButton = toggleButton;
        this.exportButton = exportButton;
        this.deleteButton = deleteButton;
        this.studio = studio;

        if (this.closeButton) {
            this.closeButton.excludeInViews("delete");
        }

        layer.hook("OnReady", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    private fetch(): void {
        if (this.refreshButton) {
            this.refreshButton.disable();
        }

        if (this.toggleButton) {
            this.toggleButton.disable();
        }

        if (this.ref.definitionId) {
            Loader.show();

            if (this.title) {
                this.title.label = pgettext("studio", "Loading...");
            }

            query({ query: ReadResponsesQuery, variables: { id: this.ref.definitionId }, onError: StudioComponent.showApiError }).then(
                (responses: IResponse[]) => {
                    responses.sort((a: IResponse, b: IResponse) => b.created - a.created);

                    this.list.clear();
                    this.list.addN(
                        ...responses.map(
                            (response: IResponse) =>
                                new Result(
                                    this,
                                    response.id,
                                    response.created,
                                    response.fingerprint,
                                    response.stencil,
                                    this.ref.definitionId
                                )
                        )
                    );

                    this.update(this.list.count);

                    if (this.refreshButton) {
                        this.refreshButton.enable();
                    }

                    Loader.hide();

                    this.layer.done();
                }
            );
        } else {
            this.layer.done();
        }
    }

    private update(count: number): void {
        if (this.exportButton) {
            this.exportButton.disabled(count === 0);
        }

        if (this.toggleButton) {
            if (this.toggleButton.isSelected && count === 0) {
                this.toggle();
            }

            this.toggleButton.disabled(count === 0);
        }

        if (this.title) {
            this.title.label = npgettext("studio", "%1 result", "%1 results", count);
        }

        if (this.whenUpdated) {
            this.whenUpdated(count);
        }
    }

    private updateDeleteButton(): void {
        if (this.deleteButton) {
            const count = (this.list.isMultipleSelect && this.list.selectionCount) || 0;

            this.deleteButton.label =
                count === 0 ? pgettext("studio", "Delete") : pgettext("studio", "Delete (%1)", L10n.locale.number(count));
            this.deleteButton.disabled(count === 0);
        }
    }

    private toggle(): void {
        if (this.toggleButton) {
            this.toggleButton.selected(!this.toggleButton.isSelected);

            if (this.toggleButton.toolbar) {
                this.toggleButton.toolbar.view = this.toggleButton.isSelected ? "delete" : "normal";
            }

            this.list.isMultipleSelect = this.toggleButton.isSelected;

            this.updateDeleteButton();

            const menu = this.list.column("menu");

            if (menu) {
                menu.isVisible = !this.toggleButton.isSelected;
            }

            if (!this.toggleButton.isSelected) {
                this.list.deselectAll();
            }
        }
    }

    private parseExports(): Components.MenuOption[] {
        interface IStencil {
            readonly stencil: string;
            count: number;
            from: number;
            to: number;
        }

        const menu: Components.MenuOption[] = [];
        const stencils: {
            [stencil: string]: IStencil;
        } = {};

        this.list.each((result: Result) => {
            const stencil = result.stencil || result.fingerprint;

            stencils[stencil] = stencils[stencil] || {
                stencil,
                count: 0,
                offset: 0,
            };

            stencils[stencil].count++;
            stencils[stencil].from = Num.min(stencils[stencil].from, result.dateSubmitted);
            stencils[stencil].to = Num.max(stencils[stencil].to, result.dateSubmitted);
        });

        const versions = map(stencils, (stencil: IStencil) => stencil).sort((a: IStencil, b: IStencil) => b.from - a.from);

        each(versions, (version: IStencil) => {
            menu.push(
                new Components.MenuLabel(
                    pgettext("studio", "%1 to %2", L10n.locale.dateShort(version.from), L10n.locale.dateShort(version.to))
                ),
                new Components.MenuLinkWithIcon(
                    0xe914,
                    npgettext("studio", "%1 result", "%1 results", version.count),
                    `${URL}${EXPORT}/${this.ref.definitionId}/${version.stencil}`,
                    "self"
                )
            );
        });

        if (menu.length > 2) {
            menu.push(
                new Components.MenuSeparator(),
                new Components.MenuLinkWithIcon(0xe933, pgettext("studio", "Why multiple downloads"), HELP_EXPORT)
            );
        }

        return menu;
    }

    private delete(): void {
        if (this.list.selectionCount > 0) {
            const label = npgettext("studio", "Delete %1 result", "Delete %1 results", this.list.selectionCount);

            DialogComponent.confirm(
                label,
                npgettext(
                    "studio",
                    "Are you sure you want to permanently delete %1 result?",
                    "Are you sure you want to permanently delete %1 results?",
                    this.list.selectionCount
                ),
                label,
                pgettext("studio", "Cancel"),
                true,
                () => {
                    each(this.list.selection, (result: Result) => result.deleteFromServer());
                }
            );
        }
    }

    onLayer(): void {
        this.layer.wait();
        this.layer.createChild((listLayer: Layers.Layer) => {
            this.list = new ResultsList(
                listLayer,
                this.ref.style,
                (resultCount: number) => this.update(resultCount),
                this.ref.definitionId
            );

            this.list.hook("OnSelectionChange", "synchronous", () => this.updateDeleteButton());

            this.fetch();
        }, Layers.Layer.configuration.left(1));

        this.layer.onClose = () => this.whenClosed && this.whenClosed();
    }

    onCards(): void {
        // This component does not use cards, instead it renders a custom layer.
    }

    createPanel<T>(fnPanel: (panel: Layers.Layer) => T, config: Layers.LayerConfiguration): T {
        let ref!: T;

        this.layer.createPanel((panel: Layers.Layer) => {
            ref = fnPanel(panel);
        }, config);

        return ref;
    }
}
