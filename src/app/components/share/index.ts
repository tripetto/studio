import { Components, Forms, IDefinition, Layers, Str, pgettext } from "@tripetto/builder";
import { StudioComponent } from "@app/components/studio";
import { BuilderComponent } from "../builder";
import { ENV, FILESTORE_URL, URL } from "@app/globals";
import { RUN } from "@server/endpoints";
import { TRunners } from "@server/entities/definitions/runners";
import { ITrackers } from "@server/entities/definitions/interface";
import { HELP_SHARE, HELP_SHARE_EMBED, HELP_SHARE_INTEGRATE, HELP_SHARE_LINK, SDK_RUNNER_QUICKSTART } from "@app/urls";
import { addMetric } from "@app/helpers/metric";
import { MetricEventType } from "@server/providers/metric";
import { TL10n, TStyles } from "@tripetto/runner";
import { getTrackerCode } from "@server/helpers/trackers";

/** Share modes. */
type ShareModes = "url" | "embed" | "integrate";

/** Types to embed to sharing code. */
type ShareEmbedTypes = "inline" | "page" | "html" | "js";

export class ShareComponent extends Components.Controller<{
    readonly runner: TRunners;
    readonly definition?: IDefinition | BuilderComponent;
    readonly styles?: TStyles;
    readonly l10n?: TL10n;
    readonly token?: string;
    readonly alias?: string;
    readonly trackers?: ITrackers;
}> {
    private readonly studio: StudioComponent;
    private codePen?: HTMLInputElement;
    private definitionId?: string;
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(
        studio: StudioComponent,
        runner?: TRunners,
        definition?: IDefinition | BuilderComponent,
        styles?: TStyles,
        l10n?: TL10n,
        token?: string,
        alias?: string,
        trackers?: ITrackers
    ): ShareComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new ShareComponent(panel, studio, runner, definition, styles, l10n, token, alias, trackers),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(
        layer: Layers.Layer,
        studio: StudioComponent,
        runner?: TRunners,
        definition?: IDefinition | BuilderComponent,
        styles?: TStyles,
        l10n?: TL10n,
        token?: string,
        alias?: string,
        trackers?: ITrackers
    ) {
        super(
            layer,
            {
                runner: runner || "autoscroll",
                definition,
                styles,
                l10n,
                token,
                alias,
                trackers,
            },
            pgettext("studio", "Share"),
            "compact",
            studio.style,
            "right",
            "always-on",
            pgettext("studio", "Close"),
            [new Components.ToolbarLink(studio.style.results.buttons.help, HELP_SHARE)]
        );

        layer.hook("OnReady", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });

        this.studio = studio;
        this.definitionId = this.ref.definition instanceof BuilderComponent ? this.ref.definition.id : undefined;
    }

    private calculateLines(src: string): number {
        return src.split("\n").length || 1;
    }

    private update(dest: Forms.Text, value: string): void {
        dest.fixedLines(this.calculateLines((dest.value = value)));
        dest.visible(value ? true : false);
    }

    private addSharingMetric(event: MetricEventType): void {
        addMetric(event, "definition", this.definitionId);
    }

    private copyToClipboard(source: Forms.Text, eventType: MetricEventType): Forms.Button {
        const label = pgettext("studio", "Copy to clipboard");

        return new Forms.Button(label).width(200).onClick((button: Forms.Button) => {
            button.disable();
            button.type("accept");
            button.label(pgettext("studio", "✔ Copied!"));

            source.copyToClipboard();

            this.addSharingMetric(eventType);

            setTimeout(() => {
                button.enable();
                button.type("normal");
                button.label(label);
            }, 1000);
        });
    }

    private tryOnCodePen(source: Forms.Text): void {
        if (!this.codePen) {
            const form = document.createElement("form");

            this.codePen = document.createElement("input");
            this.codePen.type = "hidden";
            this.codePen.name = "data";

            form.action = "https://codepen.io/pen/define";
            form.target = "_blank";
            form.method = "POST";
            form.style.visibility = "hidden";

            form.appendChild(this.codePen);

            document.body.appendChild(form);

            this.layer.hook("OnClose", "framed", () => form.remove());
        }

        this.codePen.value = JSON.stringify({
            title: "Tripetto Runner",
            html: source.value,
            editors: "100",
            css: "",
            js: "",
        });

        (this.codePen.parentElement as HTMLFormElement).submit();

        this.addSharingMetric("share_codepen");
    }

    private code(
        type: ShareEmbedTypes,
        options: {
            supportPauseAndResume: boolean;
            persistent: boolean;
        },
        cdn: string | undefined
    ): string {
        let code = "";
        const elementId = Str.djb2Hash(new Date().toISOString(), "tripetto-");

        cdn = Str.replace(cdn || "", "/", "\u002f");

        if (type === "html") {
            code += `<!DOCTYPE html>\n`;
            code += `<html>\n`;
            code += `<head>\n`;
            code += `<meta charset="UTF-8" \u002f>\n`;
            code += `<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" \u002f>\n`;
            code += `<\u002fhead>\n`;
            code += `<body>\n`;
        }

        if (type !== "js") {
            if (type !== "page" && type !== "html") {
                code += `<div id="${elementId}"></div>\n`;
            }

            code +=
                `<script src="${cdn ? cdn + "@tripetto/" : "tripetto-"}runner${cdn === "" ? ".js" : ""}"><\u002fscript>` +
                `\n<script src="${cdn ? cdn + "@tripetto/" : "tripetto-"}runner-${this.ref.runner}${
                    cdn === "" ? ".js" : ""
                }"><\u002fscript>`;

            code += `\n<script src="${cdn ? cdn + "@tripetto/" : "tripetto-"}studio${cdn === "" ? ".js" : ""}"><\u002fscript>`;

            code += "\n<script>";
            code += `\nTripettoStudio.form({`;
            code += `\n    runner: Tripetto${Str.capitalize(this.ref.runner)},`;
        } else {
            code += `import { run } from "@tripetto/runner-${this.ref.runner}";\n`;
            code += `import { Services } from "@tripetto/studio";\n`;

            code += `\n${type === "js" ? "" : `Tripetto${Str.capitalize(this.ref.runner)}.`}run(${
                type !== "js" ? "TripettoStudio" : "Services"
            }.get({`;
        }

        code += `\n    token: "${this.ref.token || ""}"`;

        if (ENV !== "production") {
            code += `,\n    url: "${URL}",\n    filestoreUrl: "${FILESTORE_URL}"`;
        }

        if (type !== "page" && type !== "html" && type !== "js") {
            code += `,\n    element: "${elementId}"`;
        } else if (type === "page" || type === "html") {
            code += `,\n    element: document.body`;
        }

        if (options.supportPauseAndResume) {
            code += `,\n    pausable: true`;
        }

        if (options.persistent) {
            code += `,\n    persistent: true`;
        }

        if (this.ref.trackers) {
            const trackerCode = getTrackerCode(this.ref.trackers, true, false);

            if (trackerCode) {
                code += `,\n    trackers: (function(trackers){return function(a,b,c){trackers.forEach(function(tracker){tracker(a,b,c)})}})(${trackerCode})`;
            }
        }

        if (type !== "js") {
            code += `\n});`;
            code += "\n<\u002fscript>";
        } else {
            code += `\n}));`;
        }

        if (type === "html") {
            code += "\n<\u002fbody>";
            code += "\n<\u002fhtml>";
        }

        return code;
    }

    private bash(type: ShareEmbedTypes): string {
        if (type !== "js") {
            return "";
        }

        return `npm i @tripetto/runner @tripetto/runner-${this.ref.runner} @tripetto/studio --save`;
    }

    onCards(cards: Components.Cards): void {
        const runnerURL = URL + RUN + "/" + (this.ref.alias || "");
        const urlText = new Forms.Text("singleline", runnerURL).readonly();
        const embedCode = new Forms.Text("multiline", "")
            .fixedLines(10)
            .sanitize(false)
            .trim(false)
            .readonly()
            .label(
                pgettext(
                    "studio",
                    "Paste the following code into your website or application (scroll down to select the embed type and for more embed options)."
                )
            );
        const embedBash = new Forms.Text("multiline", "")
            .fixedLines(1)
            .sanitize(false)
            .trim(false)
            .readonly()
            .label(pgettext("studio", "Make sure to install the required npm packages:"));
        const embedType = new Forms.Radiobutton<ShareEmbedTypes>(
            [
                {
                    value: "inline",
                    label: pgettext("studio", "Inline with other content (HTML snippet)"),
                },
                {
                    value: "page",
                    label: pgettext("studio", "Full page (HTML snippet)"),
                },
                {
                    value: "html",
                    label: pgettext("studio", "Full page (HTML page)"),
                },
                {
                    value: "js",
                    label: pgettext("studio", "Using JavaScript or TypeScript"),
                },
            ],
            "inline"
        ).on(() => fnUpdate());

        const fnUpdate = () => {
            const typeOfEmbed = embedType.value || "inline";

            this.update(
                embedCode,
                this.code(
                    typeOfEmbed,
                    {
                        supportPauseAndResume: optionPausing.isChecked,
                        persistent: optionPersistent.isChecked,
                    },
                    cdnURL.value
                )
            );

            this.update(embedBash, this.bash(typeOfEmbed));

            options.visible(modeIntegrate.value !== "integrate" && mode.value === "embed");
            cdn.visible(modeIntegrate.value !== "integrate" && mode.value === "embed" && typeOfEmbed !== "js");
            codePen.visible(typeOfEmbed !== "js");
        };

        if (this.ref.definition instanceof BuilderComponent) {
            const builder = this.ref.definition.ref;

            builder.hook("OnLoad", "synchronous", () => fnUpdate(), this);
            builder.hook("OnChange", "synchronous", () => fnUpdate(), this);

            this.layer.hook("OnClose", "synchronous", () => builder.unhookContext(this));
        }

        let modeInit = true;
        const mode = new Forms.Radiobutton<ShareModes | undefined>(
            [
                {
                    label: pgettext("studio", "🧭 Share a link ([learn more](%1))", HELP_SHARE_LINK),
                    description: pgettext(
                        "studio",
                        "Your form will be available through a dedicated link. Both the form and collected data are stored under your account at Tripetto in Western Europe. This service is free, but a fair-use policy applies."
                    ),
                    markdown: true,
                    value: "url",
                },
                {
                    label: pgettext("studio", "📋 Embed in a website ([learn more](%1))", HELP_SHARE_EMBED),
                    description: pgettext(
                        "studio",
                        "You decide where your form is published. We supply you with the required embed code for publication. Both the form and collected data are stored under your account at Tripetto in Western Europe. This service is free, but a fair-use policy applies."
                    ),
                    markdown: true,
                    value: "embed",
                },
            ],
            "url"
        ).on(() => {
            if (mode.value) {
                modeIntegrate.value = undefined;

                url.visible(mode.value === "url");
                type.visible((this.ref.token && mode.value === "embed") || false);
                options.visible((this.ref.token && mode.value === "embed") || false);
                cdn.visible((this.ref.token && mode.value === "embed" && embedType.value !== "js") || false);
                embed.visible(mode.value === "embed");
                integrate.visible(false);
            }
        });

        const modeIntegrate = new Forms.Radiobutton<ShareModes | undefined>(
            [
                {
                    label: pgettext("studio", "👨‍💻 Integrate in a website or application ([learn more](%1))", HELP_SHARE_INTEGRATE),
                    description: pgettext(
                        "studio",
                        "You decide where your form is published. And you decide where the form and collected data are stored, bypassing the Tripetto servers for that. This method requires the Tripetto FormBuilder SDK, for which specific terms and pricing apply."
                    ),
                    markdown: true,
                    value: "integrate",
                },
            ],
            undefined
        ).on(() => {
            if (!modeInit && modeIntegrate.value === "integrate") {
                mode.value = undefined;

                url.visible(false);
                type.visible(false);
                options.visible(false);
                cdn.visible(false);
                embed.visible(false);
                integrate.visible(true);
            }

            modeInit = false;
        });

        cards.add(
            new Forms.Form({
                title: pgettext("studio", "Method"),
                controls: [
                    new Forms.Static(pgettext("studio", "**Hosted by Tripetto**")).markdown(),
                    mode,
                    new Forms.Static(pgettext("studio", "**Self-hosted**")).markdown(),
                    modeIntegrate,
                ],
            })
        );

        const url = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Shareable link"),
                controls: this.ref.token
                    ? [
                          new Forms.Button(pgettext("studio", "Open in browser"), "accept")
                              .width(200)
                              .url(runnerURL)
                              .on(() => {
                                  this.addSharingMetric("share_open_link");
                              }),
                          new Forms.Static(pgettext("studio", "Or just share the link below with anyone.")),
                          urlText,
                          this.copyToClipboard(urlText, "share_copy_link"),
                      ]
                    : [
                          new Forms.Static(
                              pgettext(
                                  "studio",
                                  "An account is required to host shared forms and collected data at Tripetto. Sign in or create an account with just your email address to do so."
                              )
                          ),
                          new Forms.Spacer("small"),
                          new Forms.Button(pgettext("studio", "Sign in or register")).on(() => this.studio.signIn()),
                      ],
            })
        );

        const optionPausing = new Forms.Checkbox(pgettext("studio", "Allow pausing and resuming"), false)
            .description(pgettext("studio", "Allows users to pause the form and continue with it later on."))
            .on(() => fnUpdate());
        const optionPersistent = new Forms.Checkbox(pgettext("studio", "Save and restore uncompleted forms"), false)
            .description(
                pgettext(
                    "studio",
                    "Saves uncompleted forms in the local storage of the browser. Next time the user visits the form it is restored so the user can continue."
                )
            )
            .on(() => fnUpdate());

        const codePen = new Forms.Button(pgettext("studio", "Try this code"), "accept")
            .width(200)
            .onClick(() => this.tryOnCodePen(embedCode));

        const embed = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Embed code"),
                controls: this.ref.token
                    ? [this.copyToClipboard(embedCode, "share_copy_code"), embedCode, embedBash, codePen]
                    : [
                          new Forms.Static(
                              pgettext(
                                  "studio",
                                  "An account is required to host embedded forms and collected data at Tripetto. Sign in or create an account with just your email address to do so."
                              )
                          ),
                          new Forms.Spacer("small"),
                          new Forms.Button(pgettext("studio", "Sign in or register")).on(() => this.studio.signIn()),
                      ],
            })
        );

        const type = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Embed type"),
                controls: [new Forms.Static(pgettext("studio", "How do you want to embed your form?")), embedType],
            })
        );

        const options = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Options"),
                controls: [optionPausing, optionPersistent],
            })
        );

        const cdnCustom = new Forms.Static(
            pgettext(
                "studio",
                "If you want to use a custom domain or CDN, you need to publish the required JavaScript files for Tripetto to that domain or CDN. To do so, download the files below and publish them to the desired domain or CDN. Update your embed code so the `src`-attributes of the `script` tags point to the right domain and location of the JavaScript files.\n\n📥 [tripetto-runner.js](https://unpkg.com/@tripetto/runner)\n\n📥 [tripetto-runner-%1.js](https://unpkg.com/@tripetto/runner-%1)\n\n📥 [tripetto-studio.js](https://unpkg.com/@tripetto/studio)",
                this.ref.runner
            )
        )
            .markdown()
            .visible(false);
        const cdnURL = new Forms.Radiobutton(
            [
                {
                    label: "jsDelivr ([website](https://www.jsdelivr.com/))",
                    markdown: true,
                    value: "https://cdn.jsdelivr.net/npm/",
                },
                {
                    label: "unpkg ([website](https://unpkg.com/))",
                    markdown: true,
                    value: "https://unpkg.com/",
                },
                {
                    label: pgettext("studio", "Custom domain or CDN"),
                    markdown: true,
                    value: "",
                },
            ],
            "https://cdn.jsdelivr.net/npm/"
        )
            .label(
                pgettext(
                    "studio",
                    "The required packages for embedding Tripetto are loaded from a [content delivery network](%1). Please select the preferred network to use for your embed script:",
                    "https://en.wikipedia.org/wiki/Content_delivery_network"
                )
            )
            .markdown()
            .on(() => {
                cdnCustom.visible(cdnURL.value === "");

                fnUpdate();
            });
        const cdn = cards.add(
            new Forms.Form({
                title: pgettext("studio", "CDN"),
                controls: [cdnURL, cdnCustom],
            })
        );

        const integrateCode = new Forms.Text(
            "multiline",
            JSON.stringify(this.ref.definition instanceof BuilderComponent ? this.ref.definition.ref.definition : this.ref.definition || {})
        )
            .fixedLines(1)
            .sanitize(false)
            .trim(false)
            .readonly()
            .label(
                pgettext(
                    "studio",
                    "Here is the form definition that you can supply to a form runner. This JSON string contains your entire form and is all a runner needs to run your form."
                )
            );
        const integrate = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Integrate"),
                controls: [
                    new Forms.Static(
                        pgettext(
                            "studio",
                            "The Tripetto FormBuilder SDK lets you integrate a Tripetto form (or even the builder itself if you like) into any website or application. It's ideal for bypassing Tripetto servers and infrastructure altogether. Click the Quickstart button to learn how to integrate a so-called form runner into your application or website in minutes. Supply the JSON form definition that is shown below to the runner and your form is ready to go!"
                        )
                    ),
                    new Forms.Button(pgettext("studio", "Quickstart"), "accept").width(200).url(SDK_RUNNER_QUICKSTART),
                    this.copyToClipboard(integrateCode, "share_copy_code"),
                    integrateCode,
                ],
            })
        );
    }
}
