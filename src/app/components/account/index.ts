import { Components, Forms, Layers, pgettext } from "@tripetto/builder";
import { StudioComponent } from "@app/components/studio";
import { mutate } from "@app/helpers/api";
import { IMutationResult } from "@server/entities/mutationresult";
import { DialogComponent } from "../dialog";
import * as ChangePasswordQuery from "@app/queries/users/change-password.graphql";

export class AccountComponent extends Components.Controller<StudioComponent> {
    whenClosed?: () => void;

    static open(studio: StudioComponent): AccountComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new AccountComponent(panel, studio),
            Layers.Layer.configuration.width(400).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, studio: StudioComponent) {
        super(layer, studio, pgettext("studio", "Account"), "compact", studio.style, "right", "always-on", pgettext("studio", "Close"));

        layer.onClose = () => this.whenClosed && this.whenClosed();
    }

    onCards(cards: Components.Cards): void {
        cards.add(
            new Forms.Form({
                title: "📧 " + pgettext("studio", "Email address"),
                controls: [
                    new Forms.Email(this.ref.userAccount && this.ref.userAccount.emailAddress).readonly(),
                    new Forms.Button(pgettext("studio", "Change"), "normal").disable(),
                    new Forms.Static(
                        "💡 " +
                            pgettext(
                                "studio",
                                "Currently it is not possible to change your email address yourself. Please contact Tripetto [support](https://tripetto.com/support/) so we can assist you."
                            )
                    ).markdown(),
                ],
            })
        );

        const passwordChanged = new Forms.Notification(pgettext("studio", "Password is changed!"), "success").hide();
        const passwordError = new Forms.Notification(pgettext("studio", "An error occured!"), "error").hide();
        const passwordButton = new Forms.Button(pgettext("studio", "Update password"), "normal").disable().on(() => {
            passwordButton.disable();
            passwordChanged.hide();
            passwordError.hide();

            mutate({
                query: ChangePasswordQuery,
                variables: { password: password.value },
                onError: () => {
                    passwordButton.enable();
                    passwordError.show();
                },
            }).then((mutationResult: IMutationResult) => {
                if (mutationResult.isSuccess) {
                    passwordChanged.show();
                    password.value = "";
                    passwordConfirm.value = "";
                } else {
                    passwordButton.enable();
                    passwordError.show();
                }
            });
        });
        const password = new Forms.Text("password")
            .placeholder(pgettext("studio", "Enter new password"))
            .label(
                pgettext(
                    "studio",
                    "Here you can set a password for your account (or change it). The password needs to contain at least 8 characters."
                )
            )
            .on(() => {
                passwordConfirm.disabled(password.value.length < 8);
                passwordButton.disabled(password.value.length < 8 || password.value !== passwordConfirm.value);
            });
        const passwordConfirm = new Forms.Text("password")
            .placeholder(pgettext("studio", "Confirm password"))
            .on(() => {
                passwordButton.disabled(password.value.length < 8 || password.value !== passwordConfirm.value);
            })
            .disable();

        cards.add(
            new Forms.Form({
                title: "🔐 " + pgettext("studio", "Change password"),
                controls: [passwordChanged, passwordError, password, passwordConfirm, passwordButton],
            })
        );

        cards.add(
            new Forms.Form({
                title: "⏏️ " + pgettext("studio", "Sign out"),
                controls: [
                    new Forms.Static(
                        pgettext(
                            "studio",
                            "Sign out from your account on this browser. You can always sign in again using your email address."
                        )
                    ),
                    new Forms.Button(pgettext("studio", "Sign out"), "normal")
                        .width("auto")
                        .on(() =>
                            DialogComponent.confirm(
                                pgettext("studio", "Signing you out"),
                                pgettext("studio", "We like having you around and welcome you back anytime. Are you really leaving us?"),
                                pgettext("studio", "I'm outta here"),
                                pgettext("studio", "Cancel"),
                                false,
                                () => this.ref.signOut()
                            )
                        ),
                ],
            })
        );

        cards.add(
            new Forms.Form({
                title: "⚠️ " + pgettext("studio", "Danger zone!"),
                controls: [
                    new Forms.Static(
                        pgettext(
                            "studio",
                            "You can remove your account if you don't need it any longer. This will delete all your account data, including your forms, workspaces, and collected data.\n\n**This action is irreversible!**"
                        )
                    ).markdown(),
                    new Forms.Button(pgettext("studio", "Delete account"), "cancel")
                        .width("auto")
                        .on(() =>
                            DialogComponent.confirm(
                                pgettext("studio", "Deleting your account"),
                                pgettext(
                                    "studio",
                                    "Deletion is irreversible. Are you sure you want to permanently delete your account and everything under it?"
                                ),
                                pgettext("studio", "Delete account"),
                                pgettext("studio", "Cancel"),
                                true,
                                () => this.ref.deleteAccount()
                            )
                        ),
                ],
            })
        );
    }
}
