import { Components } from "@tripetto/builder";
import { IHeaderStyle } from "./style";

export class Divider<T> extends Components.ToolbarItem<T> {
    constructor(style: IHeaderStyle) {
        super(style.divider);
    }
}
