import { Components, DOM, linearicon, pgettext } from "@tripetto/builder";
import { StudioComponent } from "@app/components/studio";
import * as URLS from "@app/urls";

export class Menu<T> extends Components.ToolbarMenu<T> {
    private icon: number;

    constructor(studio: StudioComponent, icon: number, fnMenu: () => Components.MenuOption[]) {
        super(studio.style.header.user, undefined, fnMenu);

        this.icon = icon;
    }

    onDraw(toolbar: Components.Toolbar<T>, element: DOM.Element): void {
        super.onDraw(toolbar, element);

        element.create("i", (icon: DOM.Element) => linearicon(this.icon, icon));
    }
}

export function learnMenu(studio: StudioComponent): Components.MenuOption[] {
    return [
        new Components.MenuLinkWithIcon(0xe7ec, pgettext("studio", "Help center"), URLS.HELP),
        new Components.MenuItemWithIcon(0xe6a5, pgettext("studio", "Cheatsheet"), () => studio.openCheatsheet()),
        new Components.MenuLinkWithIcon(0xe668, pgettext("studio", "Terms of use"), URLS.TERMS),
    ];
}

export function supportMenu(): Components.MenuOption[] {
    return [new Components.MenuLinkWithIcon(0xe7ed, pgettext("studio", "Ask support"), URLS.SUPPORT)];
}

export function developersMenu(): Components.MenuOption[] {
    return [
        new Components.MenuLinkWithIcon(0xe6d6, pgettext("studio", "SDK documentation"), URLS.SDK_DOCUMENTATION),
        new Components.MenuLinkWithIcon(0xe909, pgettext("studio", "Studio API"), URLS.API_DOCUMENTATION),
        new Components.MenuLinkWithIcon(0xe90c, pgettext("studio", "Studio source code"), URLS.SOURCE),
    ];
}
