import { Components, DOM, linearicon, pgettext } from "@tripetto/builder";
import { StudioComponent } from "@app/components/studio";
import { DialogComponent } from "../dialog";
import { DeviceSize } from "@server/providers/metric";

export class User extends Components.ToolbarMenu<DeviceSize> {
    private readonly studio: StudioComponent;
    private icon?: DOM.Element;

    constructor(studio: StudioComponent) {
        super(studio.style.header.user, undefined, () => this.getMenu());

        this.studio = studio;
    }

    get isAuthenticated(): boolean {
        return this.studio.userAccount && this.studio.userAccount.emailAddress ? true : false;
    }

    private getIcon(): number {
        return this.isAuthenticated ? 0xe721 : 0xe71e;
    }

    private getMenu(): Components.MenuOption[] {
        const options: Components.MenuOption[] = [];

        options.push(
            new Components.MenuLabel((this.studio.userAccount && this.studio.userAccount.emailAddress) || ""),
            new Components.MenuItemWithIcon(0xe71b, pgettext("studio", "Account"), () => {
                this.disable();

                this.studio.account().whenClosed = () => this.enable();
            }),
            new Components.MenuItemWithIcon(0xe7b8, pgettext("studio", "Sign out"), () =>
                DialogComponent.confirm(
                    pgettext("studio", "Signing you out"),
                    pgettext("studio", "We like having you around and welcome you back anytime. Are you really leaving us?"),
                    pgettext("studio", "I'm outta here"),
                    pgettext("studio", "Cancel"),
                    false,
                    () => this.studio.signOut()
                )
            )
        );

        return options;
    }

    protected onTap(): void {
        if (!this.isAuthenticated) {
            return this.signIn();
        }

        super.onTap();
    }

    onDraw(toolbar: Components.Toolbar<DeviceSize>, element: DOM.Element): void {
        super.onDraw(toolbar, element);

        element.selector("authenticated", this.isAuthenticated);

        this.icon = element.create("i", (icon: DOM.Element) => linearicon(this.getIcon(), icon, true));
    }

    signIn(): void {
        this.disable();

        this.studio.signIn().whenClosed = () => this.enable();
    }

    refresh(): void {
        if (this.element) {
            this.element.selectorSafe("authenticated", this.isAuthenticated);
        }

        if (this.icon) {
            linearicon(this.getIcon(), this.icon, true);
        }
    }
}
