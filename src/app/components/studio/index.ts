import { DialogComponent } from "../dialog";
import { Components, DOM, Layers, arrayItem, assert, castToBoolean, castToNumber, pgettext, scheduleAction } from "@tripetto/builder";
import { BuilderComponent } from "../builder";
import { HeaderComponent } from "@app/components/header";
import { INTERVAL_KEEP_ALIVE, INTERVAL_KEEP_ALIVE_ANONYMOUS } from "@app/settings";
import { IStudioStyle } from "@app/components/studio/style";
import { IUser } from "@server/entities/users";
import { AccountComponent } from "../account";
import { SignInComponent } from "../sign-in";
import { query } from "@app/helpers/api";
import { WorkspaceComponent } from "@app/components/workspace";
import * as ReadUserQuery from "@app/queries/users/read.graphql";
import * as ReadDefinitionTemplateTokenAliasQuery from "@app/queries/definitions/read-template-token-alias.graphql";
import * as Superagent from "superagent";
import { DELETE_ACCOUNT, KEEP_ALIVE, SIGNED_IN, SIGNED_OUT, SIGN_OUT } from "@server/endpoints";
import { DEFINITION_TEMPLATE, VERSION } from "@app/globals";
import { deviceSize } from "@app/helpers/device";
import { ShareTemplateComponent } from "../template";
import { IServerDefinition } from "@server/entities/definitions";

type TActiveComponent = BuilderComponent | WorkspaceComponent | undefined;

export class StudioComponent extends Layers.LayerComponent {
    private ref: TActiveComponent;
    private readonly history: Layers.Layer[] = [];
    private isAuthenticated = false;
    readonly style: IStudioStyle;
    header!: HeaderComponent;
    rootWorkspace?: WorkspaceComponent;
    userAccount?: IUser;

    static showApiError(authenticated: boolean): void {
        if (!authenticated) {
            window.location.assign(SIGNED_OUT);
        } else {
            DialogComponent.alert(
                "🤯 " + pgettext("studio", "Something's cracking"),
                pgettext(
                    "studio",
                    "Your actions aren't being processed properly. We apologize for the inconvenience. Please try again and drop a line if troubles persist."
                )
            );
        }
    }

    static showConflictDialog(type: "definition" | "workspace"): Promise<boolean> {
        return new Promise<boolean>((resolve: (overwrite: boolean) => void) => {
            if (type === "definition") {
                DialogComponent.confirm(
                    "💥 " + pgettext("studio", "Form is outdated!"),
                    pgettext(
                        "studio",
                        "Your changes cannot be saved because your version of the form is outdated (so the version saved on the server is newer). The form is changed by someone else or by you in another browser window/tab. What do you want to do with the form?"
                    ),
                    pgettext("studio", "Reload from server"),
                    "⚠️ " + pgettext("studio", "Overwrite"),
                    false,
                    () => resolve(false),
                    () => resolve(true)
                );
            } else {
                DialogComponent.confirm(
                    "💥 " + pgettext("studio", "Workspace is outdated!"),
                    pgettext(
                        "studio",
                        "Your changes cannot be saved because your version of the workspace is outdated (so the version saved on the server is newer). The workspace needs to be reloaded from the server since it is changed by someone else or by you in another browser window/tab."
                    ),
                    pgettext("studio", "Reload from server"),
                    undefined,
                    false,
                    () => resolve(false),
                    undefined
                );
            }
        });
    }

    static showRetryDialog(): Promise<boolean> {
        return new Promise<boolean>((resolve: (overwrite: boolean) => void) => {
            DialogComponent.confirm(
                "🤯 " + pgettext("studio", "Something's cracking"),
                pgettext(
                    "studio",
                    "Your actions aren't being processed properly. We apologize for the inconvenience. Please try again and drop a line if troubles persist."
                ),
                pgettext("studio", "Retry"),
                pgettext("studio", "Cancel"),
                false,
                () => resolve(true),
                () => resolve(false)
            );
        });
    }

    constructor(style: IStudioStyle) {
        super(Layers.Layer.configuration.applicationRole().animation(Layers.LayerAnimations.Zoom));

        this.style = style;

        DialogComponent.studio = this;
    }

    get activeComponent(): TActiveComponent {
        return this.ref;
    }

    private async getIsAuthenticated(includeDeviceSize: boolean): Promise<boolean | undefined> {
        const url = includeDeviceSize ? `${KEEP_ALIVE}?device=${deviceSize()}` : KEEP_ALIVE;

        return Superagent.get(url)
            .then((response: Superagent.Response) => {
                if (response.body) {
                    if (response.body.version !== VERSION) {
                        window.location.reload();
                    }

                    return castToBoolean(response.body.isAuthenticated);
                }

                return false;
            })
            .catch(() => Promise.resolve(undefined));
    }

    private keepAlive(): void {
        setInterval(
            () => {
                this.getIsAuthenticated(false).then((isStillAuthenticated?: boolean) => {
                    if (isStillAuthenticated === undefined) {
                        return;
                    }

                    let redirectUrl = "";
                    if (!this.isAuthenticated && isStillAuthenticated) {
                        redirectUrl = SIGNED_IN;
                    } else if (this.isAuthenticated && !isStillAuthenticated) {
                        redirectUrl = SIGNED_OUT;
                    }

                    if (redirectUrl) {
                        window.location.assign(redirectUrl);
                    }
                });
            },
            this.isAuthenticated ? INTERVAL_KEEP_ALIVE : INTERVAL_KEEP_ALIVE_ANONYMOUS
        );
    }

    private makeActiveComponent<T extends TActiveComponent>(ref: T): [T, TActiveComponent] {
        const current = this.ref;

        if (current) {
            current.detach(this);
        }

        this.ref = ref;

        scheduleAction(() => {
            if (this.header) {
                this.header.update();
            }
        });

        return [ref, current];
    }

    onRender(): void {
        DOM.EventListeners.attachListener(
            window,
            "popstate",
            (event: PopStateEvent) => {
                const index = castToNumber(event.state);

                if (index > 0) {
                    const history = arrayItem(this.history, index);

                    if (history) {
                        history.close();
                    }
                }
            },
            undefined,
            this
        );

        this.header = new HeaderComponent(this);

        this.wait();

        this.getIsAuthenticated(true).then((isAuthenticated: boolean) => {
            this.isAuthenticated = isAuthenticated;

            if (this.isAuthenticated) {
                query({ query: ReadUserQuery, onError: StudioComponent.showApiError }).then((userAccount?: IUser) => {
                    this.userAccount = userAccount;
                    this.header.user.refresh();
                });
            }

            if (DEFINITION_TEMPLATE) {
                BuilderComponent.openTemplate(this, DEFINITION_TEMPLATE).then((builder?: BuilderComponent) => {
                    if (!builder) {
                        StudioComponent.showApiError(true);
                    }

                    this.done();
                });
            } else if (this.isAuthenticated) {
                this.openWorkspaceRoot().then(() => this.done());
            } else {
                BuilderComponent.openAsRoot(this).then(() => {
                    this.done();
                });
            }

            this.keepAlive();
        });
    }

    @Layers.component("OnDestroy")
    onDestroy(): void {
        DOM.EventListeners.detachListener(window, "popstate", this);
    }

    openPanel<T>(render: (layer: Layers.Layer) => T, config?: Layers.LayerConfiguration): T {
        let ref!: T;

        (
            (this.activeComponent && this.activeComponent.panel) ||
            assert(
                this.layer.createChain(
                    Layers.Layer.configuration
                        .top(this.style.header.height)
                        .layout("habc")
                        .style({
                            layer: {
                                appearance: {
                                    backgroundColor: this.style.background,
                                },
                            },
                            applyToChildren: false,
                        })
                )
            )
        ).createPanel((panel: Layers.Layer) => {
            ref = render(panel);

            if (ref instanceof BuilderComponent || ref instanceof WorkspaceComponent) {
                const [, previousComponent] = this.makeActiveComponent(ref);

                panel.hook("OnClose", "synchronous", () => this.makeActiveComponent(previousComponent));
            }

            if (history && history.pushState) {
                const index = this.history.length;

                this.history.push(panel);

                history.pushState(this.history.length, "");

                panel.hook("OnClose", "synchronous", () => {
                    this.history.splice(index);

                    history.pushState(this.history.length, "");
                });
            }

            if (!this.rootWorkspace && ref instanceof WorkspaceComponent) {
                this.rootWorkspace = ref;
            }
        }, config);

        return ref;
    }

    signIn(): SignInComponent {
        if (this.isAuthenticated) {
            throw new Error("Sign in not possible, already authenticated!");
        }

        return SignInComponent.open(this);
    }

    signOut(): void {
        if (!this.isAuthenticated) {
            throw new Error("Sign out not possible, not authenticated!");
        }

        Superagent.post(SIGN_OUT).then((value: Superagent.Response) => {
            if (value.status === 200) {
                window.location.assign(SIGN_OUT);
            }
        });
    }

    account(): AccountComponent {
        if (!this.isAuthenticated) {
            throw new Error("Account not available, not authenticated!");
        }

        return AccountComponent.open(this);
    }

    deleteAccount(): void {
        if (!this.isAuthenticated) {
            throw new Error("Cannot delete account, not authenticated!");
        }

        Superagent.delete(DELETE_ACCOUNT).then((value: Superagent.Response) => {
            if (value.status === 200) {
                window.location.assign(DELETE_ACCOUNT);
            }
        });
    }

    showRootWorkspace(): void {
        if (this.rootWorkspace) {
            this.rootWorkspace.panel.closeChildPanels();
        } else {
            this.signIn();
        }
    }

    openWorkspaceRoot(): Promise<WorkspaceComponent | undefined> {
        return WorkspaceComponent.openRoot(this);
    }

    openCheatsheet(): void {
        Components.Tutorial.open(this.layer, this.style.tutorial);
    }

    async shareTemplate(id?: string): Promise<ShareTemplateComponent> {
        let token: string | undefined;
        if (id) {
            const result = await query<IServerDefinition>({
                query: ReadDefinitionTemplateTokenAliasQuery,
                variables: {
                    id,
                },
                onError: StudioComponent.showApiError,
            });
            token = result && result.templateTokenAlias;
        }
        return ShareTemplateComponent.open(this, id, token);
    }
}
