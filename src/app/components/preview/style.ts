import { DOM } from "@tripetto/builder";

export interface IPreviewStyle {
    appearance: DOM.IStyles;
    header: {
        height: number;
        appearance: DOM.IStyles;

        runner: {
            appearance: DOM.IStyles;
            hover: DOM.IStyles;
            tap: DOM.IStyles;
            opened: DOM.IStyles;
        };

        styles: {
            appearance: DOM.IStyles;
            disabled: DOM.IStyles;
            hover: DOM.IStyles;
            tap: DOM.IStyles;
        };

        restart: {
            appearance: DOM.IStyles;
            disabled: DOM.IStyles;
            hover: DOM.IStyles;
            tap: DOM.IStyles;
        };

        help: {
            appearance: DOM.IStyles;
            hover: DOM.IStyles;
            tap: DOM.IStyles;
        };

        close: {
            appearance: DOM.IStyles;
            hover: DOM.IStyles;
            tap: DOM.IStyles;
        };
    };
    buttons: {
        height: number;
        appearance: DOM.IStyles;

        button: {
            appearance: DOM.IStyles;
            hover: DOM.IStyles;
            tap: DOM.IStyles;
            selected: DOM.IStyles;
        };
    };
    runner: {
        appearance: DOM.IStyles;
        active: DOM.IStyles;
    };
}
