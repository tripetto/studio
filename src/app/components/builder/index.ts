import { NotificationsComponent } from "../notifications";
import { ConnectionsComponent } from "../connections";
import { TrackersComponent } from "../trackers";
import { ShareComponent } from "@app/components/share";
import { ResultsComponent } from "@app/components/results";
import { DialogComponent } from "@app/components/dialog";
import { RequestPremiumComponent } from "@app/components/request-premium";
import { Loader } from "../loader";
import { DEFAULT_DEFINITION } from "@app/settings";
import { IServerDefinition } from "@server/entities/definitions";
import { TRunners } from "@server/entities/definitions/runners";
import { mutate, query } from "@app/helpers/api";
import { PreviewComponent, TPreviewDevice } from "@app/components/preview";
import { Builder, Components, Debounce, IDefinition as IBuilderDefinition, Layers, pgettext, set } from "@tripetto/builder";
import { Export, Runner, TL10n, TStyles } from "@tripetto/runner";
import { addToLocalStore, getFromLocalStore } from "@app/helpers/local-store";
import { StudioComponent } from "@app/components/studio";
import { windowSize } from "@app/helpers/device";
import { Runners } from "@app/helpers/runners";
import { IHookSettings, ITrackers } from "@server/entities/definitions/interface";
import { getTranslation } from "@services/translations";
import { LICENSE } from "@app/globals";
import { FONTS, TEMPLATE } from "@server/endpoints";
import { HEADER_TEMPLATE_TOKEN } from "@server/headers";
import {
    HELP_BLOCK_CALCULATOR,
    HELP_BLOCK_ERROR,
    HELP_BLOCK_HIDDEN_FIELD,
    HELP_BLOCK_MAILER,
    HELP_BLOCK_MAILER_SENDER,
    HELP_BRANCHES,
    HELP_CONDITIONS,
    HELP_CULLING,
    HELP_EPILOGUE,
    HELP_L10N,
    HELP_PROLOGUE,
    HELP_STYLES,
    HELP_TERMINATORS,
} from "@app/urls";
import * as Superagent from "superagent";
import * as ReadQuery from "@app/queries/definitions/read.graphql";
import * as UpdateDefinitionQuery from "@app/queries/definitions/update.graphql";
import * as UpdateStylesQuery from "@app/queries/definitions/update-styles.graphql";
import * as UpdateL10nQuery from "@app/queries/definitions/update-l10n.graphql";
import * as UpdateRunnerQuery from "@app/queries/definitions/update-runner.graphql";

export class BuilderComponent {
    private readonly studio: StudioComponent;
    private currentRunner: TRunners;
    private header?: (props: { name: string; hasName: boolean; mode?: "loading" | "workspace" | "build" | "subform" }) => void;
    private editProc: (() => void) | undefined;
    private backProc: (() => void) | undefined;
    private subform = false;

    private readonly mutateDefinition = new Debounce((id: string, definition: IBuilderDefinition) => {
        const queue = `definition:${id}`;

        mutate({
            queue,
            query: UpdateDefinitionQuery,
            variables: {
                id,
                definition,
            },
            onConflict: () => StudioComponent.showConflictDialog("definition"),
            onRetry: StudioComponent.showRetryDialog,
            onReload: this.reloadHandler("definition", queue, id),
            onError: StudioComponent.showApiError,
        });
    }, 300);

    private readonly mutateStyles = new Debounce((id: string, styles: TStyles) => {
        const queue = `definition:${id}`;

        mutate({
            queue,
            query: UpdateStylesQuery,
            variables: {
                id,
                styles,
            },
            onConflict: () => StudioComponent.showConflictDialog("definition"),
            onRetry: StudioComponent.showRetryDialog,
            onReload: this.reloadHandler("styles", queue, id),
            onError: StudioComponent.showApiError,
        });
    }, 300);

    private readonly mutateL10n = new Debounce((id: string, l10n: TL10n) => {
        const queue = `definition:${id}`;

        mutate({
            queue,
            query: UpdateL10nQuery,
            variables: {
                id,
                l10n,
            },
            onConflict: () => StudioComponent.showConflictDialog("definition"),
            onRetry: StudioComponent.showRetryDialog,
            onReload: this.reloadHandler("l10n", queue, id),
            onError: StudioComponent.showApiError,
        });
    }, 300);

    readonly ref: Builder;
    readonly preview: PreviewComponent;
    readonly styles: TStyles;
    readonly l10n: TL10n;
    readonly tier: "standard" | "premium";
    readonly id?: string;
    readonly templateToken?: string;
    readonly layer: Layers.Layer;
    readonly hooks: IHookSettings;
    readonly trackers: ITrackers;
    device: TPreviewDevice;
    onPreviewModeChange?: () => void;
    whenReady?: () => void;
    whenClosed?: () => void;

    private static async getDefaultDefinition(): Promise<IBuilderDefinition | undefined> {
        return await Superagent.get(DEFAULT_DEFINITION)
            .then((value: Superagent.Response) => value.body)
            .catch(() => Promise.resolve);
    }

    static async open(studio: StudioComponent, id: string): Promise<BuilderComponent | undefined> {
        const result = await query<IServerDefinition>({
            queue: `definition:${id}`,
            query: ReadQuery,
            variables: { id },
            onError: StudioComponent.showApiError,
        });

        if (result) {
            return studio.openPanel(
                (layer: Layers.Layer) =>
                    new BuilderComponent(
                        layer,
                        studio,
                        result.runner,
                        result.definition,
                        id,
                        result.styles,
                        result.l10n,
                        result.hooks,
                        result.trackers,
                        result.tier
                    )
            );
        }

        return undefined;
    }

    static async openAsRoot(studio: StudioComponent): Promise<BuilderComponent> {
        const definition = getFromLocalStore<IBuilderDefinition>() || (await this.getDefaultDefinition());
        const styles = getFromLocalStore<TStyles>("styles");
        const l10n = getFromLocalStore<TL10n>("l10n");
        const runner = getFromLocalStore<TRunners>("runner");

        return studio.openPanel((root: Layers.Layer) => new BuilderComponent(root, studio, runner, definition, undefined, styles, l10n));
    }

    static async openTemplate(studio: StudioComponent, templateToken: string): Promise<BuilderComponent | undefined> {
        return Superagent.get(TEMPLATE)
            .set(HEADER_TEMPLATE_TOKEN, templateToken)
            .then((response: Superagent.Response) => {
                return studio.openPanel(
                    (layer: Layers.Layer) =>
                        new BuilderComponent(
                            layer,
                            studio,
                            response.body && response.body.runner,
                            response.body && response.body.definition,
                            undefined,
                            response.body && response.body.styles,
                            response.body && response.body.l10n,
                            undefined,
                            undefined,
                            "standard",
                            templateToken
                        )
                );
            })
            .catch(() => undefined);
    }

    private constructor(
        layer: Layers.Layer,
        studio: StudioComponent,
        runner?: TRunners,
        definition?: IBuilderDefinition,
        id?: string,
        styles?: TStyles,
        l10n?: TL10n,
        hooks?: IHookSettings,
        trackers?: ITrackers,
        tier?: "standard" | "premium",
        templateToken?: string
    ) {
        const size = windowSize();

        this.studio = studio;
        this.id = id;
        this.currentRunner = runner || "autoscroll";
        this.styles = styles || {};
        this.l10n = l10n || {};
        this.hooks = hooks || {};
        this.trackers = trackers || {};
        this.tier = tier || "standard";
        this.templateToken = templateToken;
        this.layer = layer;
        this.device = size === "xs" || size === "s" ? "off" : size === "xl" ? "tablet" : "phone";
        this.ref = Builder.open(definition, {
            license: LICENSE,
            layer,
            layerConfiguration: Layers.Layer.configuration.right(this.device === "off" ? 0 : PreviewComponent.getWidth(this.device)),
            style: studio.style,
            controls: "right",
            fonts: FONTS,
            zoom: "fit-horizontal",
            disableLogo: true,
            disableSaveButton: true,
            disableEditButton: true,
            disableCloseButton: true,
            disableTutorialButton: true,
            disableOpenCloseAnimation: true,
            disableNesting: false,
            disableBookmarks: true,
            supportURL: false,
            helpTopics: {
                prologue: HELP_PROLOGUE,
                epilogue: HELP_EPILOGUE,
                branches: HELP_BRANCHES,
                culling: HELP_CULLING,
                terminators: HELP_TERMINATORS,
                conditions: HELP_CONDITIONS,
                "block:error": HELP_BLOCK_ERROR,
                "block:hidden-field": HELP_BLOCK_HIDDEN_FIELD,
                "block:calculator": HELP_BLOCK_CALCULATOR,
                "block:mailer": HELP_BLOCK_MAILER,
                "block:mailer:sender": HELP_BLOCK_MAILER_SENDER,
            },
            namespace: {
                identifier: this.currentRunner,
                url: Runners.builderBundleURL(this.currentRunner),
            },
            onChange: (changedDefinition: IBuilderDefinition) => {
                if (id) {
                    this.mutateDefinition.invoke(id, changedDefinition);
                } else {
                    addToLocalStore(changedDefinition);
                }
            },
            onPreview: (previewDefinition: IBuilderDefinition) => {
                this.preview.setDefinition(previewDefinition);
            },
            onBreadcrumb: (forms, back) => {
                this.editProc = undefined;
                this.backProc = back;
                this.subform = forms.length > 1;

                if (this.header && forms.length > 0) {
                    const currentForm = forms[forms.length - 1];

                    this.header({
                        name: currentForm.name || pgettext("studio", "Unnamed"),
                        hasName: currentForm.name ? true : false,
                        mode: (forms.length > 1 && "subform") || undefined,
                    });

                    this.editProc = forms.length > 1 ? () => currentForm.edit() : () => this.ref.edit("properties");
                }

                if (this.studio) {
                    this.studio.header.updateBackButton();
                }
            },
            onClose: () => {
                if (this.whenClosed) {
                    this.whenClosed();
                }
            },
        });

        this.preview = this.layer.component(new PreviewComponent(this.studio, this, templateToken ? "test" : "preview"));

        layer.hook("OnShow", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "synchronous", () => {
            this.mutateDefinition.flush();
            this.mutateStyles.flush();
            this.mutateL10n.flush();
        });
    }

    get panel(): Layers.Layer {
        return this.layer;
    }

    get runner(): TRunners {
        return this.currentRunner;
    }

    set runner(runner: TRunners) {
        Loader.show();

        this.ref
            .useNamespace(runner, Runners.builderBundleURL(runner), "url")
            .then(() => {
                this.preview.changeRunner((this.currentRunner = runner));

                if (this.id) {
                    mutate({
                        query: UpdateRunnerQuery,
                        variables: {
                            id: this.id,
                            runner,
                        },
                        onError: StudioComponent.showApiError,
                    });
                } else {
                    addToLocalStore(runner, "runner");
                }

                Loader.hide();
            })
            .catch(() => Loader.hide());
    }

    get hasId(): boolean {
        return this.id ? true : false;
    }

    get isRoot(): boolean {
        return !this.hasId;
    }

    get isTemplate(): boolean {
        return this.templateToken ? true : false;
    }

    get isSubform(): boolean {
        return this.subform;
    }

    get mockup() {
        if (this.ref.definition) {
            const runner = new Runner({
                definition: this.ref.definition,
                mode: "paginated",
                start: true,
            });

            if (runner.instance) {
                return Export.exportables(runner.instance);
            }
        }

        return undefined;
    }

    get name() {
        return this.ref.definition?.name || "";
    }

    private reloadHandler(type: "definition" | "styles" | "l10n", queue: string, id: string) {
        return async () => {
            const result = await query<IServerDefinition>({
                queue,
                query: ReadQuery,
                variables: { id },
                onError: StudioComponent.showApiError,
            });

            if (result?.definition) {
                this.ref.definition = result.definition;

                if (type === "definition") {
                    this.preview.setDefinition(this.ref.definition);
                }
            }

            if (result?.styles) {
                this.preview.setStyles(set(this, "styles", result.styles));

                if (type === "styles") {
                    this.stylesEditor();
                }
            }

            if (result?.l10n) {
                this.preview.setL10n(set(this, "l10n", result.l10n));

                if (type === "l10n") {
                    this.l10nEditor();
                }
            }
        };
    }

    attach(
        context: {},
        callback: (props: { name: string; hasName: boolean; mode?: "loading" | "workspace" | "build" | "subform" }) => void
    ): void {
        this.header = callback;
    }

    detach(): void {
        this.header = undefined;
    }

    back(): boolean {
        if (this.backProc) {
            this.backProc();

            return true;
        }

        return false;
    }

    edit(): void {
        if (this.editProc) {
            this.editProc();
        }
    }

    close(): void {
        this.ref.close();
    }

    async restore(): Promise<void> {
        const definition = await BuilderComponent.getDefaultDefinition();
        if (definition) {
            this.ref.definition = definition;
        }
    }

    wipe(): void {
        this.ref.clear();
    }

    async share(): Promise<ShareComponent> {
        let token: string | undefined;
        let alias: string | undefined;
        let trackers: ITrackers | undefined;

        if (this.id) {
            const definitionResult = await query<IServerDefinition>({
                query: ReadQuery,
                variables: {
                    id: this.id,
                },
                onError: StudioComponent.showApiError,
            });

            token = definitionResult?.readToken;
            alias = definitionResult?.readTokenAlias;
            trackers = definitionResult?.trackers;
        }

        return ShareComponent.open(this.studio, this.runner, this, undefined, undefined, token, alias, trackers);
    }

    stylesEditor(): Components.StylesEditor | undefined {
        const contract = Runners.stylesContract(this.runner);

        return (
            contract &&
            this.preview.closeOnRunnerChange(
                this.studio.openPanel(
                    (panel: Layers.Layer) =>
                        new Components.StylesEditor(
                            panel,
                            contract,
                            this.styles,
                            this.tier === "premium" ? "licensed" : "unlicensed",
                            (styles) => {
                                this.preview.setStyles(set(this, "styles", styles));

                                if (this.id) {
                                    this.mutateStyles.invoke(this.id, styles);
                                } else {
                                    addToLocalStore(styles, "styles");
                                }
                            },
                            false,
                            true,
                            (done: (bReset: boolean) => void) => {
                                DialogComponent.confirm(
                                    pgettext("studio", "Reset styles"),
                                    pgettext("studio", "Are you sure you want to reset the styles for this form?"),
                                    pgettext("studio", "Yes, reset it!"),
                                    pgettext("studio", "No"),
                                    true,
                                    () => done(true),
                                    () => done(false)
                                );
                            },
                            undefined,
                            this.studio.style,
                            "right",
                            [new Components.ToolbarLink(this.studio.style.results.buttons.help, HELP_STYLES)]
                        ),
                    Layers.Layer.configuration.width(400).animation(Layers.LayerAnimations.Zoom)
                )
            )
        );
    }

    l10nEditor(): Components.L10nEditor | undefined {
        const contract = Runners.l10nContract(this.runner);

        return (
            contract &&
            this.preview.closeOnRunnerChange(
                this.studio.openPanel(
                    (panel: Layers.Layer) =>
                        new Components.L10nEditor(
                            panel,
                            contract,
                            this.ref,
                            this.l10n,
                            (l10n, current) => {
                                set(this, "l10n", l10n);

                                this.preview.setL10n(current);

                                if (this.id) {
                                    this.mutateL10n.invoke(this.id, l10n);
                                } else {
                                    addToLocalStore(l10n, "l10n");
                                }
                            },
                            (language: string) => getTranslation("", language, `@tripetto/runner-${this.runner}`),
                            undefined,
                            this.studio.style,
                            "right",
                            [new Components.ToolbarLink(this.studio.style.results.buttons.help, HELP_L10N)]
                        ),
                    Layers.Layer.configuration.width(600).animation(Layers.LayerAnimations.Zoom)
                )
            )
        );
    }

    requestPremium(): RequestPremiumComponent {
        return RequestPremiumComponent.open(this.studio, this.id || "", this.ref.name);
    }

    notifications(): NotificationsComponent {
        return NotificationsComponent.open(this.studio, this.hooks, this.id);
    }

    connections(): ConnectionsComponent {
        return ConnectionsComponent.open(this.studio, this.hooks, this.tier === "premium", this.id, this.ref.name);
    }

    tracking(): TrackersComponent {
        return TrackersComponent.open(this.studio, this.trackers, this.tier === "premium", this.id, this.ref.name);
    }

    results(): ResultsComponent {
        return ResultsComponent.open(this.studio, this.id);
    }

    toggleDevice(device: "phone" | "tablet" | "desktop"): boolean {
        this.device = (this.device !== device && device) || "off";

        if (this.ref.layer) {
            this.ref.layer.configuration.right(this.device === "off" ? 0 : PreviewComponent.getWidth(this.device));
        }

        if (this.preview) {
            this.preview.setDevice(this.device);
        }

        if (this.onPreviewModeChange) {
            this.onPreviewModeChange();
        }

        return this.device === device;
    }

    closePreview(): void {
        if (this.device !== "off") {
            this.toggleDevice(this.device);
        }
    }
}
