import { IServerDefinition } from "@server/entities/definitions";
import { IServerWorkspace, IServerWorkspaceTile } from "@server/entities/workspaces";
import { BuilderComponent } from "@app/components/builder";
import { Loader } from "@app/components/loader";
import { IStudioStyle } from "@app/components/studio/style";
import { mutate, query } from "@app/helpers/api";
import { IMutationResult } from "@server/entities/mutationresult";
import { DocumentNode } from "graphql";
import {
    Components,
    DOM,
    L10n,
    Layers,
    MoveableLayout,
    Rectangles,
    TGridRectangles,
    Touch,
    assert,
    isFilledString,
    linearicon,
    pgettext,
} from "@tripetto/builder";
import { WorkspaceComponent } from "..";
import { DialogComponent } from "@app/components/dialog";
import { StudioComponent } from "@app/components/studio";
import { ShareComponent } from "@app/components/share";
import { ResultsComponent } from "@app/components/results";
import { ShareTemplateComponent } from "@app/components/template";
import { RUN } from "@server/endpoints";
import { NotificationsComponent } from "@app/components/notifications";
import { ConnectionsComponent } from "@app/components/connections";
import { TrackersComponent } from "@app/components/trackers";
import * as DeleteDefinitionQuery from "@app/queries/definitions/delete.graphql";
import * as ReadDefinitionNameQuery from "@app/queries/definitions/read-name.graphql";
import * as ReadDefinitionTokenAliasQuery from "@app/queries/definitions/read-token-alias.graphql";
import * as DeleteWorkspaceQuery from "@app/queries/workspaces/delete.graphql";
import * as ReadWorkspaceNameQuery from "@app/queries/workspaces/read-name.graphql";
import * as ReadQuery from "@app/queries/definitions/read.graphql";

export class TileLayout extends MoveableLayout<
    Components.Workspace<IServerWorkspaceTile>,
    Components.Tile<IServerWorkspaceTile>,
    IStudioStyle
> {
    private icon: DOM.Element | undefined;
    private type: DOM.Element | undefined;
    private label: DOM.Element | undefined;
    private context: DOM.Element | undefined;
    private counter: DOM.Element | undefined;
    private allowDelete = true;
    private resultsCount = 0;

    constructor(renderer: WorkspaceComponent, tile: Components.Tile<IServerWorkspaceTile>, layer: Layers.Layer) {
        super(renderer, tile, layer, true);
    }

    get configuration(): TGridRectangles {
        return new Rectangles([
            {
                name: "self",
                width: this.style.workspace.tiles.width,
                height: this.style.workspace.tiles.height,
                spacingRight: this.style.workspace.tiles.spacing,
                spacingBottom: this.style.workspace.tiles.spacing,
            },
        ]);
    }

    get app(): StudioComponent {
        return (this.renderer as WorkspaceComponent).studio;
    }

    get tile(): Components.Tile<IServerWorkspaceTile> {
        return this.parent;
    }

    private async fetch(redraw: boolean): Promise<{
        type: string;
        icon: number;
        label: string;
        count: number;
    }> {
        switch (this.tile.data?.type) {
            case "workspace":
                /* eslint-disable no-case-declarations */
                const workspace =
                    (!redraw && typeof this.tile.data.name === "string" && this.tile.data) ||
                    (await query<IServerWorkspace>({
                        query: ReadWorkspaceNameQuery,
                        variables: {
                            id: this.tile.data.id,
                        },
                    }));

                if (workspace) {
                    return {
                        type: pgettext("studio", "Workspace"),
                        icon: 0xe977,
                        label: workspace.name || "",
                        count: workspace.count || 0,
                    };
                }

                return {
                    type: pgettext("studio", "Workspace not found!"),
                    icon: 0xe955,
                    label: "",
                    count: 0,
                };
            case "form":
                const form =
                    (!redraw &&
                        this.tile.data.runner && {
                            name: this.tile.data.name,
                            runner: this.tile.data.runner,
                            responseCount: this.tile.data.count,
                        }) ||
                    (await query<IServerDefinition>({
                        query: ReadDefinitionNameQuery,
                        variables: {
                            id: this.tile.data.id,
                        },
                    }));

                if (form) {
                    switch (form.runner) {
                        case "chat":
                            return {
                                type: pgettext("studio", "Chat form"),
                                icon: 0xe7d7,
                                label: form.name || "",
                                count: form.responseCount || 0,
                            };
                        case "classic":
                            return {
                                type: pgettext("studio", "Classic form"),
                                icon: 0xe92f,
                                label: form.name || "",
                                count: form.responseCount || 0,
                            };
                        case "autoscroll":
                            return {
                                type: pgettext("studio", "Autoscroll form"),
                                icon: 0xe711,
                                label: form.name || "",
                                count: form.responseCount || 0,
                            };
                    }

                    return {
                        type: pgettext("studio", "Form not found!"),
                        icon: 0xe955,
                        label: "",
                        count: 0,
                    };
                }
        }

        return {
            type: "",
            icon: 0,
            label: "",
            count: 0,
        };
    }

    private updateCounter(): void {
        if (this.counter) {
            DOM.Element.text(assert(this.counter.child(1)).HTMLElement, L10n.locale.number(this.resultsCount));
        }
    }

    private update(redraw: boolean): void {
        if (this.tile.data) {
            const isWorkspace = this.tile.data.type === "workspace";
            const isForm = this.tile.data.type === "form";

            if (this.element) {
                this.element.selectorSafe("workspace", isWorkspace);
                this.element.selectorSafe("form", isForm);
            }

            this.fetch(redraw).then((tileData: { type: string; icon: number; label: string; count: number }) => {
                this.resultsCount = tileData.count || 0;

                if (this.type) {
                    this.type.text = tileData.type || pgettext("studio", "Unknown type");
                }

                if (this.label) {
                    this.label.text = tileData.label || pgettext("studio", "Unnamed");
                    this.label.selectorSafe("unnamed", !isFilledString(tileData.label));
                }

                linearicon(tileData.icon || 0xe933, this.icon, true);

                if (this.counter) {
                    this.counter.selectorSafe("visible", isForm);

                    if (isForm) {
                        this.updateCounter();
                    }
                }

                this.allowDelete = tileData.count === 0;
            });
        } else {
            if (this.element) {
                this.element.selectorSafe("workspace", false);
                this.element.selectorSafe("form", false);
            }

            if (this.type) {
                this.type.text = pgettext("studio", "Loading...");
            }

            if (this.counter) {
                this.counter.selectorSafe("visible", false);
            }

            linearicon(0xe8cf, this.icon, true);
        }
    }

    private async open(): Promise<void> {
        if (this.tile.data && this.renderer instanceof WorkspaceComponent) {
            Loader.show();

            const component = await (this.tile.data.type === "workspace" ? WorkspaceComponent : BuilderComponent).open(
                this.renderer.studio,
                this.tile.data.id
            );

            if (component) {
                component.whenReady = () => Loader.hide();
                component.whenClosed = () => this.render("update");
            } else {
                Loader.hide();
            }
        }
    }

    private delete(): void {
        if (this.tile.data) {
            const collection = this.tile.collection;

            if (collection) {
                const deleteResource = (id: string, deleteQuery: DocumentNode) => {
                    Loader.show();

                    this.tile.delete();

                    mutate<IMutationResult>({ query: deleteQuery, variables: { id }, onError: StudioComponent.showApiError }).then(
                        (result: IMutationResult) => {
                            if (!result.isSuccess) {
                                const restoreTile = collection.tiles.append();

                                restoreTile.index = this.tile.index;
                                restoreTile.data = this.tile.data;
                            }

                            Loader.hide();
                        }
                    );
                };

                switch (this.tile.data.type) {
                    case "workspace":
                        deleteResource(this.tile.data.id, DeleteWorkspaceQuery);
                        break;
                    case "form":
                        deleteResource(this.tile.data.id, DeleteDefinitionQuery);
                        break;
                }
            }
        } else {
            this.tile.delete();
        }
    }

    private async contextMenu(): Promise<void> {
        if (!this.context) {
            return;
        }

        const menu = this.context;
        let options: Components.MenuOption[] = [];
        let deleteOption: Components.MenuOption | undefined;

        if (this.tile.data) {
            const id = this.tile.data.id;

            switch (this.tile.data.type) {
                case "workspace":
                    options = [
                        new Components.MenuLabel(pgettext("studio", "Workspace")),
                        new Components.MenuItemWithIcon(0xe977, pgettext("studio", "Open"), () => this.open()),
                    ];
                    deleteOption = new Components.MenuItemWithIcon(
                        0xe681,
                        pgettext("studio", "Delete"),
                        () => this.animate(() => this.delete()),
                        !this.allowDelete
                    );
                    break;
                case "form":
                    options = [
                        new Components.MenuLabel(pgettext("studio", "Design")),
                        new Components.MenuItemWithIcon(0xe62d, pgettext("studio", "Edit"), () => this.open()),
                        new Components.MenuItemWithIcon(0xe6db, pgettext("studio", "Share as template"), async () => {
                            Loader.show();

                            this.app
                                .shareTemplate(id)
                                .then((component: ShareTemplateComponent) => (component.whenReady = () => Loader.hide()));
                        }),
                        new Components.MenuSeparator(),
                        new Components.MenuLabel(pgettext("studio", "Run")),
                        new Components.MenuLinkWithIcon(
                            0xe7b1,
                            pgettext("studio", "Open in browser"),
                            await (async () => {
                                const result = await query<IServerDefinition>({
                                    query: ReadDefinitionTokenAliasQuery,
                                    variables: {
                                        id,
                                    },
                                    onError: StudioComponent.showApiError,
                                });
                                const token = result && result.readTokenAlias;

                                return token ? RUN + "/" + token : "";
                            })()
                        ),
                        new Components.MenuItemWithIcon(0xe8c1, pgettext("studio", "Share or embed"), async () => {
                            Loader.show();

                            const result = await query<IServerDefinition>({
                                query: ReadQuery,
                                variables: { id },
                                onError: StudioComponent.showApiError,
                            });

                            if (result) {
                                ShareComponent.open(
                                    this.app,
                                    result.runner,
                                    result.definition,
                                    result.styles,
                                    result.l10n,
                                    result.readToken,
                                    result.readTokenAlias,
                                    result.trackers
                                ).whenReady = () => Loader.hide();
                            } else {
                                Loader.hide();
                            }
                        }),
                        new Components.MenuSeparator(),
                        new Components.MenuLabel(pgettext("studio", "Automate")),
                        new Components.MenuItemWithIcon(0xe8aa, pgettext("studio", "Notifications"), async () => {
                            Loader.show();

                            const result = await query<IServerDefinition>({
                                queue: `definition:${id}`,
                                query: ReadQuery,
                                variables: { id },
                                onError: StudioComponent.showApiError,
                            });

                            if (result) {
                                NotificationsComponent.open(this.app, result.hooks || {}, id).whenReady = () => Loader.hide();
                            } else {
                                Loader.hide();
                            }
                        }),
                        new Components.MenuItemWithIcon(0xe920, pgettext("studio", "Connections"), async () => {
                            Loader.show();

                            const result = await query<IServerDefinition>({
                                queue: `definition:${id}`,
                                query: ReadQuery,
                                variables: { id },
                                onError: StudioComponent.showApiError,
                            });

                            if (result) {
                                ConnectionsComponent.open(
                                    this.app,
                                    result.hooks || {},
                                    result.tier === "premium",
                                    id,
                                    result.name
                                ).whenReady = () => Loader.hide();
                            } else {
                                Loader.hide();
                            }
                        }),
                        new Components.MenuItemWithIcon(0xe77f, pgettext("studio", "Tracking"), async () => {
                            Loader.show();

                            const result = await query<IServerDefinition>({
                                queue: `definition:${id}`,
                                query: ReadQuery,
                                variables: { id },
                                onError: StudioComponent.showApiError,
                            });

                            if (result) {
                                TrackersComponent.open(
                                    this.app,
                                    result.trackers || {},
                                    result.tier === "premium",
                                    id,
                                    result.name
                                ).whenReady = () => Loader.hide();
                            } else {
                                Loader.hide();
                            }
                        }),
                        new Components.MenuSeparator(),
                        new Components.MenuLabel(pgettext("studio", "Results")),
                        new Components.MenuItemWithIcon(
                            0xe6ac,
                            pgettext("studio", "View results (%1)", L10n.locale.number(this.resultsCount)),
                            () => this.viewResults(),
                            this.resultsCount === 0
                        ),
                    ];
                    deleteOption = new Components.MenuItemWithIcon(
                        0xe681,
                        pgettext("studio", "Delete"),
                        () =>
                            DialogComponent.confirm(
                                pgettext("studio", "Deleting the form"),
                                pgettext(
                                    "studio",
                                    "Deletion is irreversable. Are you sure you want to permanently delete this form and the collected results within it?"
                                ),
                                pgettext("studio", "Delete form"),
                                pgettext("studio", "Cancel"),
                                true,
                                () => this.animate(() => this.delete())
                            ),
                        !this.allowDelete
                    );
                    break;
            }
        }

        if (options.length > 0) {
            options.push(new Components.MenuSeparator());
        }

        options.push(
            new Components.MenuLabel(pgettext("studio", "Actions")),
            new Components.MenuSubmenuWithIcon(
                0xe93d,
                pgettext("studio", "Move"),
                [
                    new Components.MenuLabel(pgettext("studio", "Move within collection")),
                    new Components.MenuItemWithIcon(
                        0xe943,
                        pgettext("studio", "Left"),
                        () => this.animate(() => (this.tile.index = this.tile.predecessorInRow!.index)),
                        !this.tile.predecessorInRow
                    ),
                    new Components.MenuItemWithIcon(
                        0xe944,
                        pgettext("studio", "Right"),
                        () => this.animate(() => (this.tile.index = this.tile.successorInRow!.index)),
                        !this.tile.successorInRow
                    ),
                    new Components.MenuItemWithIcon(
                        0xe941,
                        pgettext("studio", "Up"),
                        () => this.animate(() => (this.tile.index = this.tile.predecessorInColumn!.index)),
                        !this.tile.predecessorInColumn
                    ),
                    new Components.MenuItemWithIcon(
                        0xe942,
                        pgettext("studio", "Down"),
                        () => this.animate(() => (this.tile.index = this.tile.successorInColumn!.index)),
                        !this.tile.successorInColumn
                    ),
                ],
                !this.tile.predecessorInRow && !this.tile.successorInRow && !this.tile.predecessorInColumn && !this.tile.successorInColumn
            )
        );

        if (deleteOption) {
            options.push(deleteOption);
        } else if (!this.tile.data) {
            options.push(
                new Components.MenuItemWithIcon(
                    0xe681,
                    pgettext("studio", "Delete"),
                    () => this.animate(() => this.delete()),
                    !this.allowDelete
                )
            );
        }

        Components.Menu.openAtElement(options, this.context, {
            style: this.style.menu,
            position: "right",
            onOpen: () => menu.addSelectorSafe("opened"),
            onClose: () => menu.removeSelectorSafe("opened"),
        });
    }

    private viewResults(): void {
        if (this.tile.data) {
            const results = ResultsComponent.open(this.app, this.tile.data.id);

            Loader.show();

            results.whenReady = () => Loader.hide();
            results.whenUpdated = (resultsCount: number) => {
                this.resultsCount = resultsCount;

                this.updateCounter();
            };
        }
    }

    draw(x: number, y: number, w: number, h: number, zoom: number, zIndx: number, redraw: boolean): void {
        if (!this.element) {
            return;
        }

        if (!redraw) {
            if (this.style.workspace.rulers) {
                this.enableRulers("rgba(0,0,255,0.5)");
            }

            this.element.style(
                [
                    this.style.workspace.tiles.appearance,
                    {
                        [DOM.Stylesheet.selector("moving")]: this.style.workspace.tiles.moving,
                        [DOM.Stylesheet.selector("following")]: this.style.workspace.tiles.following,
                        [DOM.Stylesheet.selector("ensuing")]: this.style.workspace.tiles.ensuing,
                    },
                ],
                this.stylesheet
            );

            this.element.create("div");
            this.element.create("div");

            this.icon = this.element.create("div", undefined, this.style.workspace.tiles.icon, this.stylesheet);
            this.type = this.element.create("div", undefined, this.style.workspace.tiles.type, this.stylesheet);

            this.label = this.element.create(
                "div",
                undefined,
                [
                    this.style.workspace.tiles.label.appearance,
                    {
                        [DOM.Stylesheet.selector("unnamed")]: this.style.workspace.tiles.label.unnamed,
                    },
                ],
                this.stylesheet
            );

            this.counter = this.element.create(
                "div",
                (counter: DOM.Element) => {
                    counter.create("i", (icon: DOM.Element) => linearicon(0xe7e9, icon));
                    counter.create("span");

                    Touch.Tap.single(
                        counter,
                        () => this.viewResults(),
                        () => counter.addSelectorSafe("tap"),
                        () => counter.removeSelectorSafe("tap")
                    );

                    Touch.Hover.pointer(counter, (pHover: Touch.IHoverEvent) => counter.selectorSafe("hover", pHover.isHovered));
                },
                [
                    this.style.workspace.tiles.counter.appearance,
                    {
                        [DOM.Stylesheet.selector("visible")]: this.style.workspace.tiles.counter.visible,
                        [DOM.Stylesheet.selector("hover")]: this.style.workspace.tiles.counter.hover,
                        [DOM.Stylesheet.selector("tap")]: this.style.workspace.tiles.counter.tap,
                    },
                ],
                this.stylesheet
            );

            this.context = this.element.create(
                "div",
                (context: DOM.Element) => {
                    linearicon(0xe672, context);

                    Touch.Tap.single(
                        context,
                        () => this.contextMenu(),
                        () => context.addSelectorSafe("tap"),
                        () => context.removeSelectorSafe("tap")
                    );

                    Touch.Hover.pointer(context, (pHover: Touch.IHoverEvent) => context.selectorSafe("hover", pHover.isHovered));
                },
                [
                    this.style.workspace.tiles.buttons.context.appearance,
                    {
                        [DOM.Stylesheet.selector("hover")]: this.style.workspace.tiles.buttons.context.hover,
                        [DOM.Stylesheet.selector("tap")]: this.style.workspace.tiles.buttons.context.tap,
                        [DOM.Stylesheet.selector("opened")]: this.style.workspace.tiles.buttons.context.opened,
                    },
                ],
                this.stylesheet
            );

            Touch.Tap.on(this.element, () => this.open());
            Touch.Tap.context(this.element, () => this.contextMenu());
        }

        this.update(redraw);
    }

    destroy(): void {
        this.icon = undefined;
        this.type = undefined;
        this.label = undefined;
        this.context = undefined;
        this.counter = undefined;

        super.destroy();
    }
}
