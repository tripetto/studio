import { StudioComponent } from "@app/components/studio";
import { Components, Debounce, LayerRenderer, Layers, Num, assert, pgettext } from "@tripetto/builder";
import { WorkspaceEditor } from "./workspace/editor";
import { CollectionLayout } from "./collection/layout";
import { TileLayout } from "./tile/layout";
import { IStudioStyle } from "@app/components/studio/style";
import { WorkspaceLayout } from "./workspace/layout";
import { IServerWorkspace, IServerWorkspaceTile } from "@server/entities/workspaces";
import { mutate, query } from "@app/helpers/api";
import { appendLocalStoreToEmptyWorkspace } from "@app/helpers/local-store";
import * as ReadRootQuery from "@app/queries/workspaces/read-root.graphql";
import * as ReadQuery from "@app/queries/workspaces/read.graphql";
import * as UpdateQuery from "@app/queries/workspaces/update.graphql";

export class WorkspaceComponent extends LayerRenderer<
    Components.Workspace<IServerWorkspaceTile>,
    Components.Workspace<IServerWorkspaceTile> | Components.Collection<IServerWorkspaceTile> | Components.Tile<IServerWorkspaceTile>,
    IStudioStyle
> {
    readonly studio: StudioComponent;
    readonly isRoot: boolean;
    whenReady?: () => void;
    whenClosed?: () => void;

    static async open(
        studio: StudioComponent,
        id: string | "root",
        onError?: (authenticated: boolean) => void
    ): Promise<WorkspaceComponent | undefined> {
        const queue = `workspace:${id}`;
        const result = await query<IServerWorkspace>({
            queue,
            query: id === "root" ? ReadRootQuery : ReadQuery,
            variables: { id },
            onError: onError || StudioComponent.showApiError,
        });

        if (result) {
            return studio.openPanel((panel: Layers.Layer) => {
                let ref!: WorkspaceComponent;
                const workspacePanels = assert(panel.createChain(Layers.Layer.configuration.layout("hbca")));

                workspacePanels.createPanel(
                    (layer: Layers.Layer) => {
                        const workspaceComponent = Components.Workspace.create(
                            (ref = new WorkspaceComponent(layer, studio, !id || id === "root"))
                        );
                        const workspaceMutate = new Debounce((name: string, workspace: Components.IWorkspace<IServerWorkspaceTile>) => {
                            if (
                                result.recovered ||
                                result.name !== name ||
                                JSON.stringify(result.workspace) !== JSON.stringify(workspace)
                            ) {
                                result.name = name;
                                result.workspace = JSON.parse(JSON.stringify(workspace));

                                mutate({
                                    queue,
                                    query: UpdateQuery,
                                    variables: {
                                        id,
                                        name,
                                        workspace,
                                    },
                                    onConflict: () => StudioComponent.showConflictDialog("workspace"),
                                    onRetry: StudioComponent.showRetryDialog,
                                    onReload: async () => {
                                        const res = await query<IServerWorkspace>({
                                            queue,
                                            query: id === "root" ? ReadRootQuery : ReadQuery,
                                            variables: { id },
                                            onError: onError || StudioComponent.showApiError,
                                        });

                                        if (res?.workspace) {
                                            workspaceComponent.load(res.workspace);
                                        }
                                    },
                                    onError: StudioComponent.showApiError,
                                });
                            }
                        }, 1000);

                        workspaceComponent.onChanged = () => workspaceMutate.invoke(workspaceComponent.name, workspaceComponent.save());

                        if (result.workspace) {
                            workspaceComponent.load(result.workspace);
                        }

                        if (id === "root") {
                            appendLocalStoreToEmptyWorkspace(workspaceComponent);
                        }
                    },
                    Layers.Layer.configuration
                        .scrollbars({
                            style: studio.style.workspace.scrollbars,
                            direction: "horizontal",
                            bounceHorizontal: "always",
                        })
                        .autoCloseChildPanels("tap")
                );

                return ref;
            });
        }

        return undefined;
    }

    static async openRoot(studio: StudioComponent): Promise<WorkspaceComponent | undefined> {
        return await this.open(studio, "root");
    }

    constructor(layer: Layers.Layer, studio: StudioComponent, isRoot: boolean) {
        super(
            [
                {
                    layout: WorkspaceLayout,
                    type: Components.Workspace,
                },
                {
                    layout: CollectionLayout,
                    type: Components.Collection,
                },
                {
                    layout: TileLayout,
                    type: Components.Tile,
                },
            ],
            layer,
            studio.style
        );

        this.studio = studio;
        this.isRoot = isRoot;

        layer.onReady = () => this.whenReady && this.whenReady();
        layer.onClose = () => this.whenClosed && this.whenClosed();
    }

    get panel(): Layers.Layer {
        return assert(assert(this.layer.chain).parent.layer);
    }

    get workspace(): Components.Workspace<IServerWorkspaceTile> | undefined {
        return this.origin as Components.Workspace<IServerWorkspaceTile> | undefined;
    }

    private recalculate(): void {
        const workspace = this.workspace;

        if (workspace) {
            let height = this.layer.height / workspace.zoom;

            workspace.rectangles.minHeight("self", height);
            workspace.rectangles.maxHeight("self", height);

            height -=
                workspace.rectangles.spacingVertical("children") +
                this.style.workspace.collections.header.height +
                this.style.workspace.collections.padding * 2 -
                this.style.workspace.tiles.spacing;

            workspace.threshold = Num.floor(height / (this.style.workspace.tiles.height + this.style.workspace.tiles.spacing));

            workspace.applyResize();
        }
    }

    onInit(): void {
        this.layer.hook("OnResize", "synchronous", () => this.recalculate());
    }

    onZoomEnd(zoom: number): void {
        this.recalculate();

        super.onZoomEnd(zoom);
    }

    render(): void {
        this.recalculate();

        if (this.workspace) {
            this.workspace.render(true);
        }
    }

    attach(context: {}, callback: (props: { name: string; hasName: boolean }) => void): void {
        const workspace = assert(this.workspace);
        const fnName = () => {
            callback({
                name: workspace.name || pgettext("studio", "Unnamed workspace"),
                hasName: workspace.name ? true : false,
            });
        };

        fnName();

        workspace.hook("OnRename", "synchronous", () => fnName(), context);
    }

    detach(context: {}): void {
        assert(this.workspace).unhookContext(context);
    }

    edit(): void {
        WorkspaceEditor.openPanel(this.layer, assert(this.workspace));
    }

    close(): void {
        this.panel.close();
    }
}
