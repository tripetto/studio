import {
    Components,
    DOM,
    Layers,
    MoveableLayout,
    Rectangles,
    TGridRectangles,
    Touch,
    assert,
    isFilledString,
    linearicon,
    pgettext,
} from "@tripetto/builder";
import { WorkspaceComponent } from "..";
import { IStudioStyle } from "@app/components/studio/style";
import { CollectionEditor } from "./editor";
import { mutate } from "@app/helpers/api";
import { StudioComponent } from "@app/components/studio";
import { TRunners } from "@server/entities/definitions/runners";
import { IServerDefinition } from "@server/entities/definitions";
import { IServerWorkspace, IServerWorkspaceTile } from "@server/entities/workspaces";
import * as CreateWorkspaceQuery from "@app/queries/workspaces/create.graphql";
import * as CreateDefinitionQuery from "@app/queries/definitions/create.graphql";

export class CollectionLayout extends MoveableLayout<
    Components.Workspace<IServerWorkspaceTile>,
    Components.Collection<IServerWorkspaceTile>,
    IStudioStyle
> {
    private label: DOM.Element | undefined;
    private buttons: [DOM.Element, DOM.Element] | undefined;
    private message: DOM.Element | undefined;

    constructor(renderer: WorkspaceComponent, collection: Components.Collection<IServerWorkspaceTile>, layer: Layers.Layer) {
        super(renderer, collection, layer, true);

        collection.tiles.hook("OnCountChange", "synchronous", () => collection.resize());
    }

    get configuration(): TGridRectangles {
        return new Rectangles([
            {
                name: "self",
                height: this.style.workspace.collections.header.height,
                minWidth: this.style.workspace.tiles.width + this.style.workspace.collections.padding * 2,
                minHeight:
                    this.style.workspace.collections.header.height +
                    this.style.workspace.collections.padding * 2 +
                    this.style.workspace.tiles.height,
                spacingRight: this.style.workspace.collections.spacing,
            },
            {
                name: "children",
                spacingLeft: this.style.workspace.collections.padding,
                spacingTop: this.style.workspace.collections.header.height + this.style.workspace.collections.padding,
                spacingRight:
                    this.style.workspace.collections.spacing +
                    this.style.workspace.collections.padding -
                    this.style.workspace.tiles.spacing,
                spacingBottom: this.style.workspace.collections.padding - this.style.workspace.tiles.spacing,
                align: ["self"],
            },
        ]);
    }

    get collection(): Components.Collection<IServerWorkspaceTile> {
        return this.parent;
    }

    get name(): string {
        return this.collection.name || pgettext("studio", "Unnamed");
    }

    get app(): StudioComponent {
        return (this.renderer as WorkspaceComponent).studio;
    }

    private addMenu(): void {
        if (!this.buttons) {
            return;
        }

        const button = this.buttons[1];

        Components.Menu.openAtElement(
            [
                new Components.MenuLabel(pgettext("studio", "New")),
                new Components.MenuItemWithIcon(0xe711, pgettext("studio", "Autoscroll form"), () =>
                    this.animate(() => this.createForm("autoscroll"))
                ),
                new Components.MenuItemWithIcon(0xe7d7, pgettext("studio", "Chat form"), () => this.animate(() => this.createForm("chat"))),
                new Components.MenuItemWithIcon(0xe92f, pgettext("studio", "Classic form"), () =>
                    this.animate(() => this.createForm("classic"))
                ),
                new Components.MenuSeparator(),
                new Components.MenuItemWithIcon(0xe977, pgettext("studio", "Workspace"), () => this.animate(() => this.createWorkspace())),
            ],
            button,
            {
                style: this.style.menu,
                position: "bottom",
                onOpen: () => button.addSelectorSafe("opened"),
                onClose: () => button.removeSelectorSafe("opened"),
            }
        );
    }

    private contextMenu(): void {
        if (!this.buttons) {
            return;
        }

        const button = this.buttons[0];

        Components.Menu.openAtElement(
            [
                new Components.MenuLabel(pgettext("studio", "Collection")),
                new Components.MenuItemWithIcon(0xe60d, pgettext("studio", "Edit"), () => this.edit()),
                new Components.MenuSubmenuWithIcon(
                    0xe93d,
                    pgettext("studio", "Move"),
                    [
                        new Components.MenuItemWithIcon(
                            0xe943,
                            pgettext("studio", "Left"),
                            () => this.animate(() => this.collection.index--),
                            this.collection.isFirst
                        ),
                        new Components.MenuItemWithIcon(
                            0xe944,
                            pgettext("studio", "Right"),
                            () => this.animate(() => this.collection.index++),
                            this.collection.isLast
                        ),
                    ],
                    this.collection.isFirst && this.collection.isLast
                ),
                new Components.MenuItemWithIcon(
                    0xe681,
                    pgettext("studio", "Delete"),
                    () => this.animate(() => this.collection.delete()),
                    this.collection.tiles.count > 0
                ),
            ],
            button,
            {
                style: this.style.menu,
                position: "bottom",
                onOpen: () => button.addSelectorSafe("opened"),
                onClose: () => button.removeSelectorSafe("opened"),
            }
        );
    }

    draw(x: number, y: number, w: number, h: number, zoom: number, zIndex: number, redraw: boolean): void {
        if (!this.element) {
            return;
        }
        if (!redraw) {
            if (this.style.workspace.rulers) {
                this.enableRulers("rgba(0,255,0,0.5)");
            }

            this.element.style(
                [
                    this.style.workspace.collections.appearance,
                    {
                        [DOM.Stylesheet.selector("moving")]: this.style.workspace.collections.moving,
                        [DOM.Stylesheet.selector("ensuing")]: this.style.workspace.collections.ensuing,
                    },
                ],
                this.stylesheet
            );

            this.element.create(
                "div",
                (header: DOM.Element) => {
                    this.label = header.create(
                        "div",
                        (label: DOM.Element) => {
                            Touch.Tap.on(label, () => this.edit());
                            Touch.Tap.context(label, () => this.contextMenu());
                        },
                        [
                            this.style.workspace.collections.header.label.appearance,
                            {
                                [DOM.Stylesheet.selector("unnamed")]: this.style.workspace.collections.header.label.unnamed,
                            },
                        ],
                        this.stylesheet
                    );

                    this.buttons = [
                        header.create(
                            "div",
                            (context: DOM.Element) => {
                                linearicon(0xe672, context);

                                Touch.Tap.on(
                                    context,
                                    () => this.contextMenu(),
                                    () => context.addSelectorSafe("tap"),
                                    () => context.removeSelectorSafe("tap")
                                );

                                Touch.Hover.pointer(context, (pHover: Touch.IHoverEvent) =>
                                    context.selectorSafe("hover", pHover.isHovered)
                                );
                            },
                            [
                                this.style.workspace.collections.header.buttons.context.appearance,
                                {
                                    [DOM.Stylesheet.selector("hover")]: this.style.workspace.collections.header.buttons.context.hover,
                                    [DOM.Stylesheet.selector("tap")]: this.style.workspace.collections.header.buttons.context.tap,
                                    [DOM.Stylesheet.selector("opened")]: this.style.workspace.collections.header.buttons.context.opened,
                                },
                            ],
                            this.stylesheet
                        ),
                        header.create(
                            "div",
                            (add: DOM.Element) => {
                                linearicon(0xe936, add);

                                Touch.Tap.on(
                                    add,
                                    () => this.addMenu(),
                                    () => add.addSelectorSafe("tap"),
                                    () => add.removeSelectorSafe("tap")
                                );

                                Touch.Hover.pointer(add, (pHover: Touch.IHoverEvent) => add.selectorSafe("hover", pHover.isHovered));
                            },
                            [
                                this.style.workspace.collections.header.buttons.add.appearance,
                                {
                                    [DOM.Stylesheet.selector("hover")]: this.style.workspace.collections.header.buttons.add.hover,
                                    [DOM.Stylesheet.selector("tap")]: this.style.workspace.collections.header.buttons.add.tap,
                                    [DOM.Stylesheet.selector("opened")]: this.style.workspace.collections.header.buttons.add.opened,
                                },
                            ],
                            this.stylesheet
                        ),
                    ];
                },
                this.style.workspace.collections.header.appearance,
                this.stylesheet
            );

            this.message = this.element.create(
                "div",
                (message: DOM.Element) => {
                    message.text = pgettext("studio", "Press + above to add something to this collection");
                },
                [
                    this.style.workspace.collections.message.appearance,
                    {
                        [DOM.Stylesheet.selector("visible")]: this.style.workspace.collections.message.visible,
                    },
                ],
                this.stylesheet
            );
        }

        if (this.label) {
            this.label.text = this.name;
            this.label.selectorSafe("unnamed", !isFilledString(this.collection.name));
        }

        if (this.message) {
            this.message.selectorSafe("visible", this.collection.tiles.count === 0);
        }
    }

    destroy(): void {
        this.label = undefined;
        this.buttons = undefined;
        this.message = undefined;

        super.destroy();
    }

    edit(): void {
        CollectionEditor.openPanel(this.layer, this.collection);
    }

    async createForm(runner: TRunners): Promise<void> {
        const tile = this.collection.tiles.append();
        const result = assert(
            await mutate<IServerDefinition>({
                query: CreateDefinitionQuery,
                variables: {
                    runner,
                },
                onError: StudioComponent.showApiError,
            })
        );

        tile.data = {
            type: "form",
            id: result.id,
        };
    }

    async createWorkspace(): Promise<void> {
        const tile = this.collection.tiles.append();
        const result = assert(
            await mutate<IServerWorkspace>({
                query: CreateWorkspaceQuery,
                onError: StudioComponent.showApiError,
            })
        );

        tile.data = {
            type: "workspace",
            id: result.id,
        };
    }
}
