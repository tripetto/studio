import { Components, Forms, Layers, MoveableLayout, assert, pgettext } from "@tripetto/builder";
import { IStudioStyle } from "@app/components/studio/style";
import { IServerWorkspaceTile } from "@server/entities/workspaces";

export class CollectionEditor extends Components.Controller<Components.Collection<IServerWorkspaceTile>> {
    static open(layer: Layers.Layer, collection: Components.Collection<IServerWorkspaceTile>): CollectionEditor {
        return new CollectionEditor(layer, collection);
    }

    static openPanel(parent: Layers.Layer, collection: Components.Collection<IServerWorkspaceTile>): Layers.Layer {
        const pStyle =
            collection.layout &&
            (
                collection.layout as MoveableLayout<
                    Components.Workspace<IServerWorkspaceTile>,
                    Components.Collection<IServerWorkspaceTile>,
                    IStudioStyle
                >
            ).style;

        return assert(
            parent.createPanel(
                (layer: Layers.Layer) => {
                    this.open(layer, collection);
                },
                Layers.Layer.configuration
                    .width(pStyle ? pStyle.forms.width.small : 0)
                    .animation(Layers.LayerAnimations.Zoom)
                    .autoCloseChildPanels("stroke")
            )
        );
    }

    private constructor(layer: Layers.Layer, collection: Components.Collection<IServerWorkspaceTile>) {
        super(
            layer,
            collection,
            pgettext("studio", "Collection properties"),
            "compact",
            collection.layout &&
                (
                    collection.layout as MoveableLayout<
                        Components.Workspace<IServerWorkspaceTile>,
                        Components.Collection<IServerWorkspaceTile>,
                        IStudioStyle
                    >
                ).style
        );
    }

    private get collection(): Components.Collection<IServerWorkspaceTile> {
        return this.ref;
    }

    onCards(cards: Components.Cards): void {
        cards.add(
            new Forms.Form({
                title: pgettext("studio", "Name"),
                controls: [
                    new Forms.Text("singleline", Forms.Text.bind(this.collection, "name", ""))
                        .escape(() => {
                            this.close();

                            return true;
                        })
                        .enter(() => {
                            this.close();

                            return true;
                        })
                        .placeholder(pgettext("studio", "Unnamed collection"))
                        .autoFocus()
                        .autoSelect(),
                ],
            })
        );
    }
}
