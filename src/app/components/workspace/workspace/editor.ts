import { Components, Forms, Layers, MoveableLayout, assert, pgettext } from "@tripetto/builder";
import { IStudioStyle } from "@app/components/studio/style";
import { IServerWorkspaceTile } from "@server/entities/workspaces";

export class WorkspaceEditor extends Components.Controller<Components.Workspace<IServerWorkspaceTile>> {
    static open(layer: Layers.Layer, workspace: Components.Workspace<IServerWorkspaceTile>): WorkspaceEditor {
        return new WorkspaceEditor(layer, workspace);
    }

    static openPanel(parent: Layers.Layer, workspace: Components.Workspace<IServerWorkspaceTile>): Layers.Layer {
        const pStyle =
            workspace.layout &&
            (
                workspace.layout as MoveableLayout<
                    Components.Workspace<IServerWorkspaceTile>,
                    Components.Workspace<IServerWorkspaceTile>,
                    IStudioStyle
                >
            ).style;

        return assert(
            parent.createPanel(
                (layer: Layers.Layer) => {
                    this.open(layer, workspace);
                },
                Layers.Layer.configuration
                    .width(pStyle ? pStyle.forms.width.small : 0)
                    .animation(Layers.LayerAnimations.Zoom)
                    .autoCloseChildPanels("stroke")
            )
        );
    }

    private constructor(layer: Layers.Layer, workspace: Components.Workspace<IServerWorkspaceTile>) {
        super(
            layer,
            workspace,
            pgettext("studio", "Workspace properties"),
            "compact",
            workspace.layout &&
                (
                    workspace.layout as MoveableLayout<
                        Components.Workspace<IServerWorkspaceTile>,
                        Components.Workspace<IServerWorkspaceTile>,
                        IStudioStyle
                    >
                ).style
        );
    }

    private get workspace(): Components.Workspace<IServerWorkspaceTile> {
        return this.ref;
    }

    onCards(cards: Components.Cards): void {
        cards.add(
            new Forms.Form({
                title: pgettext("studio", "Name"),
                controls: [
                    new Forms.Text("singleline", Forms.Text.bind(this.workspace, "name", ""))
                        .escape(() => {
                            this.close();

                            return true;
                        })
                        .enter(() => {
                            this.close();

                            return true;
                        })
                        .placeholder(pgettext("studio", "Unnamed workspace"))
                        .autoFocus()
                        .autoSelect(),
                ],
            })
        );
    }
}
