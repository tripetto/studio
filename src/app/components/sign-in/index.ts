import * as Superagent from "superagent";
import {
    Components,
    DateTime,
    Forms,
    L10n,
    Layers,
    Num,
    Str,
    cancelInterval,
    castToString,
    pgettext,
    scheduleInterval,
} from "@tripetto/builder";
import { StudioComponent } from "@app/components/studio";
import { BLOCKED, SIGN_IN, SIGN_IN_VERIFY } from "@server/endpoints";
import { LOCAL_STORAGE_KEY_LOGIN_ADDRESS, LOCAL_STORAGE_KEY_LOGIN_TYPE } from "@app/settings";
import { ENV } from "@app/globals";
import { TERMS } from "@app/urls";
import { IStudioStyle } from "@app/components/studio/style";

const LOGIN_EXPIRES_IN = 15; /* minutes */

export class SignInComponent extends Components.Controller<{
    emailAddress: string;
}> {
    private loginStep1!: Components.Card;
    private loginStep1Error!: Forms.Notification;
    private loginStep2!: Components.Card;
    private loginStep2Error!: Forms.Notification;
    private loginStep2Label!: Forms.Static;
    private loginExpired!: Forms.Notification;
    private loginAddress!: Forms.Email;
    private loginRemember!: Forms.Checkbox;
    private loginType!: Forms.Radiobutton<"link" | "password">;
    private loginPassword!: Forms.Text;
    private loginButtonNext!: Forms.Button;
    private loginButtonLogin!: Forms.Button;
    private loginToken = "";
    private loginTime = 0;
    private loginTimer = 0;
    private loginTimerLabel!: Forms.Static;
    whenClosed?: () => void;

    static open(studio: StudioComponent): SignInComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new SignInComponent(panel, studio),
            Layers.Layer.configuration.width(400).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, studio: StudioComponent) {
        super(
            layer,
            {
                emailAddress: "",
            },
            pgettext("studio", "Sign in or create your account"),
            "compact",
            studio.style,
            "right",
            "always-on",
            pgettext("studio", "Close")
        );

        layer.onClose = () => {
            if (this.loginTimer) {
                cancelInterval(this.loginTimer);
            }

            if (this.whenClosed) {
                this.whenClosed();
            }
        };
    }

    private signIn(): void {
        const emailAddress = this.ref.emailAddress;
        const type = this.loginType.value;

        this.loginStep1Error.hide();
        this.loginExpired.hide();
        this.loginAddress.disable();
        this.loginRemember.disable();
        this.loginType.disable();
        this.loginButtonNext.disable();

        Superagent.post(SIGN_IN)
            .send({ emailAddress, type, language: L10n.current, locale: L10n.locale.identifier })
            .retry(2)
            .then((response) => {
                if (ENV === "development") {
                    console.log(response.body);
                }

                if (response.ok && response.body.loginToken) {
                    this.loginToken = response.body.loginToken;

                    this.loginStep1.hide();
                    this.loginStep2.show();
                    this.loginPassword.enable();
                    this.loginPassword.focus();
                    this.loginPassword.value = "";

                    if (this.loginType.value === "password") {
                        this.loginStep2Label.label(
                            "🔑 " +
                                pgettext(
                                    "studio",
                                    "Enter your password and click **Login**.\n\n*If there was no account found for the supplied email address or no password was set, we've send you a magic link anyway.*"
                                )
                        );
                    } else {
                        this.loginStep2Label.label(
                            "📧 " +
                                pgettext(
                                    "studio",
                                    "**You've got mail!*\n\nPlease click the magic link in the email to login. Or, copy-paste the temporary password from the email in the password field below and click **Login**."
                                )
                        );
                    }

                    this.loginPassword.placeholder(
                        this.loginType.value === "password"
                            ? pgettext("studio", "Your password")
                            : pgettext("studio", "Paste temporary password here")
                    );

                    if (this.loginTimer) {
                        this.loginTimer = cancelInterval(this.loginTimer);
                    }

                    this.loginTime = DateTime.now;
                    this.loginTimer = scheduleInterval(() => {
                        const elapsed = DateTime.now - this.loginTime;

                        if (elapsed >= 1000 * 60 * LOGIN_EXPIRES_IN) {
                            this.sessionExpired();
                        } else {
                            const remaining = 1000 * 60 * LOGIN_EXPIRES_IN - elapsed;
                            const minutes = Num.floor(remaining / (1000 * 60));
                            const seconds = Num.round((remaining - 1000 * 60 * minutes) / 1000);

                            this.loginTimerLabel.label(
                                "⌛ " +
                                    pgettext(
                                        "studio",
                                        "Please complete the login within **%1:%2** minutes.",
                                        Str.padLeft(castToString(minutes), "0", 2),
                                        Str.padLeft(castToString(seconds), "0", 2)
                                    )
                            );
                        }
                    }, 1000);
                } else {
                    this.loginStep1Error.visible();
                    this.loginAddress.enable();
                    this.loginButtonNext.enable();
                }
            })
            .catch(() => {
                window.location.assign(BLOCKED);
            });
    }

    private submitPassword(): void {
        if (this.loginToken && this.loginPassword.value.length >= 8) {
            const fnError = (error: number) => {
                if (error === 401) {
                    this.loginStep2Error.visible();
                    this.loginPassword.enable();
                    this.loginButtonLogin.enable();
                } else {
                    this.sessionExpired();
                }
            };

            this.loginStep2Error.hide();
            this.loginPassword.disable();
            this.loginButtonLogin.disable();

            Superagent.post(SIGN_IN_VERIFY)
                .send({ password: this.loginPassword.value, token: this.loginToken })
                .retry(2)
                .then((response) => {
                    if (response.ok) {
                        window.location.assign("/?no-cache=" + this.loginToken);
                    } else {
                        fnError(response.statusCode);
                    }
                })
                .catch((error) => fnError(error.status || 400));
        }
    }

    private sessionExpired(): void {
        if (this.loginTimer) {
            this.loginTimer = cancelInterval(this.loginTimer);
        }

        this.loginStep2.hide();
        this.loginStep1.show();
        this.loginExpired.show();
        this.loginAddress.enable();
        this.loginRemember.enable();
        this.loginType.enable();
    }

    onCards(cards: Components.Cards): void {
        let remember = false;

        if (!this.ref.emailAddress && localStorage) {
            this.ref.emailAddress = localStorage.getItem(LOCAL_STORAGE_KEY_LOGIN_ADDRESS) || "";

            if (this.ref.emailAddress) {
                remember = true;
            }
        }

        this.loginStep1 = cards.add(
            new Forms.Form({
                title: pgettext("studio", "Step 1 of 2"),
                controls: [
                    (this.loginStep1Error = new Forms.Notification(
                        pgettext("studio", "Something went wrong, please try again!"),
                        "error"
                    ).hide()),
                    (this.loginExpired = new Forms.Notification(pgettext("studio", "Login session expired!"), "warning").hide()),
                    (this.loginAddress = new Forms.Email(Forms.Email.bind(this.ref, "emailAddress", ""))
                        .label(pgettext("studio", "Enter your email address, select how you want to log in and click the **Next** button."))
                        .markdown()
                        .hook("OnValidate", "synchronous", (ev: Forms.IControlValidateEvent<Forms.Email>) => {
                            this.loginButtonNext.disabled(!ev.control.isPassed);
                        })
                        .on(() => {
                            if (this.loginRemember.isChecked && localStorage) {
                                localStorage.setItem(LOCAL_STORAGE_KEY_LOGIN_ADDRESS, this.loginAddress.value);
                            }
                        })
                        .enter((email: Forms.Email) => {
                            if (email.isPassed) {
                                this.signIn();

                                return true;
                            }

                            return false;
                        })
                        .escape(() => {
                            this.close();

                            return true;
                        })
                        .placeholder(pgettext("studio", "Enter your email address here"))
                        .require()
                        .autoValidate()
                        .autoFocus()
                        .autoSelect()),
                    (this.loginRemember = new Forms.Checkbox(pgettext("studio", "Remember email address"), remember).on(() => {
                        if (localStorage) {
                            if (this.loginRemember.isChecked) {
                                localStorage.setItem(LOCAL_STORAGE_KEY_LOGIN_ADDRESS, this.loginAddress.value);
                            } else {
                                localStorage.removeItem(LOCAL_STORAGE_KEY_LOGIN_ADDRESS);
                            }
                        }
                    })),
                    (this.loginType = new Forms.Radiobutton<"link" | "password">(
                        [
                            {
                                value: "link",
                                label: "✉️ " + pgettext("studio", "Login with email"),
                                description: pgettext(
                                    "studio",
                                    "We'll send you a magic link to your inbox. Click the link in the email to log in, or use the temporary password that's in the email."
                                ),
                            },
                            {
                                value: "password",
                                label: "🔐 " + pgettext("studio", "Login with password"),
                                description: pgettext(
                                    "studio",
                                    "If you already have a Tripetto account and set a password, you can log in with your password. In that case, we won't send you the magic link."
                                ),
                            },
                        ],
                        localStorage && localStorage.getItem(LOCAL_STORAGE_KEY_LOGIN_TYPE) === "password" ? "password" : "link"
                    ).on((type) => {
                        if (localStorage) {
                            if (type.value === "password") {
                                localStorage.setItem(LOCAL_STORAGE_KEY_LOGIN_TYPE, "password");
                            } else {
                                localStorage.removeItem(LOCAL_STORAGE_KEY_LOGIN_TYPE);
                            }
                        }
                    })),
                    (this.loginButtonNext = new Forms.Button(pgettext("studio", "Next")).disable().on(() => this.signIn())),
                ],
            })
        );

        this.loginStep2 = cards
            .add(
                new Forms.Form({
                    title: pgettext("studio", "Step 2 of 2"),
                    controls: [
                        (this.loginStep2Error = new Forms.Notification(pgettext("studio", "Incorrect password!"), "warning").hide()),
                        (this.loginStep2Label = new Forms.Static(
                            "📧 " +
                                pgettext(
                                    "studio",
                                    "**You've got mail!*\n\nPlease click the magic link in the email to login. Or, copy-paste the temporary password from the email in the password field below and click **Login**."
                                )
                        ).markdown()),
                        (this.loginPassword = new Forms.Text("password")
                            .placeholder(pgettext("studio", "Paste temporary password here"))
                            .markdown()
                            .on(() => {
                                this.loginButtonLogin.disabled(this.loginPassword.value.length >= 8 ? false : true);
                            })).enter((password) => {
                            if (password.value.length >= 8) {
                                this.submitPassword();

                                return true;
                            }

                            return false;
                        }),
                        new Forms.Spacer("small"),
                        (this.loginButtonLogin = new Forms.Button(pgettext("studio", "Login")).disable().on(() => this.submitPassword())),
                        new Forms.Spacer("small"),
                        (this.loginTimerLabel = new Forms.Static(
                            "⌛ " +
                                pgettext(
                                    "studio",
                                    "Please complete the login within **%1:%2** minutes.",
                                    Str.padLeft(castToString(LOGIN_EXPIRES_IN), "0", 2),
                                    "00"
                                )
                        ).markdown()),
                    ],
                })
            )
            .hide();

        cards.add(
            new Forms.Form({
                controls: [
                    new Forms.HTML(
                        `<b>${pgettext("studio", "Terms of use")}<\u002fb><br \u002f>` +
                            pgettext(
                                "studio",
                                "By using the Tripetto Studio, you acknowledge that you have read and understand the applicable terms and conditions found at %1 and you agree to be bound by these.",
                                `<a href="${TERMS}" target="_blank">tripetto.app\u002fterms<\u002fa>`
                            ),
                        false,
                        {
                            appearance: (this.style as IStudioStyle).signIn.terms,
                        }
                    ),
                ],
                style: {
                    appearance: {
                        border: "none",
                    },
                },
            })
        );
    }
}
