import { Components } from "@tripetto/builder";
import { IServerWorkspaceTile } from "@server/entities/workspaces";

export function getOrCreateFirstCollection(
    workspace: Components.Workspace<IServerWorkspaceTile>
): Components.Collection<IServerWorkspaceTile> | undefined {
    return workspace.collections.count === 0 ? workspace.collections.append() : workspace.collections.firstItem;
}

export function addTile(collection: Components.Collection<IServerWorkspaceTile>, id: string): void {
    collection.tiles.append().data = {
        type: "form",
        id,
    };
}
