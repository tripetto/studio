import fetch from "cross-fetch";
import { DocumentNode } from "graphql";
import { ApolloClient, HttpLink, InMemoryCache, from } from "@apollo/client";
import { RetryLink } from "@apollo/client/link/retry";
import { ErrorResponse } from "@apollo/client/link/error";
import { API } from "@server/endpoints";

interface IQueryProperties {
    query: DocumentNode;
    variables?: {};
    queue?: string;
    onError?: (authenticated: boolean) => void;
}

const client = new ApolloClient({
    link: from([
        new RetryLink({
            delay: {
                initial: 300,
                max: Infinity,
                jitter: true,
            },
            attempts: {
                max: 5,
            },
        }),
        new HttpLink({ uri: API, credentials: "same-origin", fetch }),
    ]),
    cache: new InMemoryCache(),
});

const queue: {
    [name: string]: {
        token: string;
        jobs: (IQueryProperties & {
            waiting: boolean;
            overwrite: boolean;
            readonly resolve: (value: {} | undefined) => void;
            readonly reject: () => void;
            readonly onConflict: () => Promise<boolean>;
            readonly onRetry: () => Promise<boolean>;
            readonly onReload: () => void;
        })[];
    };
} = {};

function onApiError(onError?: (authenticated: boolean) => void): (err: ErrorResponse) => undefined {
    return (error: ErrorResponse) => {
        const networkError = error.networkError;
        let authenticated = true;

        if (networkError && "statusCode" in networkError && networkError.statusCode === 401) {
            authenticated = false;
        }

        if (onError) {
            onError(authenticated);
        }
        return undefined;
    };
}

export async function query<T>(props: IQueryProperties): Promise<T | undefined> {
    return client
        .query({
            query: props.query,
            variables: props.variables,
            fetchPolicy: "no-cache",
        })
        .then((value) => {
            const data = (value.data && value.data[Object.keys(value.data)[0]]) || undefined;

            if (props.queue) {
                (
                    queue[props.queue] ||
                    (queue[props.queue] = {
                        token: "",
                        jobs: [],
                    })
                ).token = typeof data["updateToken"] === "string" ? data["updateToken"] : "";
            }

            return data as T;
        })
        .catch(onApiError(props.onError));
}

function worker(id: string) {
    const q =
        queue[id] ||
        (queue[id] = {
            token: "",
            jobs: [],
        });

    if (q.jobs.length > 0 && q.jobs[0].waiting) {
        const job = q.jobs[0];

        job.waiting = false;

        client
            .mutate({
                mutation: job.query,
                variables: {
                    ...job.variables,
                    updateToken: (!job.overwrite && q.token) || "",
                },
            })
            .then((value) => {
                const data = (value.data && value.data[Object.keys(value.data)[0]]) || undefined;

                if (typeof data["updateToken"] !== "string" || data["updateToken"] === "") {
                    job.onConflict()
                        .then((overwrite: boolean) => {
                            if (overwrite) {
                                job.overwrite = true;
                                job.waiting = true;

                                worker(id);
                            } else {
                                q.jobs = [];

                                job.onReload();
                            }
                        })
                        .catch(() => job.onReload());
                } else {
                    q.token = data["updateToken"];
                    q.jobs.shift();

                    job.resolve(data);

                    worker(id);
                }
            })
            .catch((err: ErrorResponse) => {
                const networkError = err.networkError;
                const fnCancel = () => {
                    q.jobs = [];

                    job.reject();

                    onApiError(job.onError)(err);
                };

                if (networkError && "statusCode" in networkError && networkError.statusCode === 500) {
                    job.onRetry()
                        .then((retry: boolean) => {
                            if (retry) {
                                job.waiting = false;

                                worker(id);
                            } else {
                                fnCancel();
                            }
                        })
                        .catch(() => fnCancel());
                } else {
                    fnCancel();
                }
            });
    }
}

export async function mutate<T>(
    props: IQueryProperties & {
        readonly queue?: string;
        readonly onConflict?: () => Promise<boolean>;
        readonly onRetry?: () => Promise<boolean>;
        readonly onReload?: () => void;
    }
): Promise<T | undefined> {
    if (props.queue) {
        const id = props.queue;
        const onConflict = props.onConflict || (() => new Promise<boolean>((resolve: (overwrite: boolean) => void) => resolve(true)));
        const onRetry = props.onRetry || (() => new Promise<boolean>((resolve: (retry: boolean) => void) => resolve(false)));
        const onReload = props.onReload || (() => {});

        return new Promise<{} | undefined>((resolve: (value: {} | undefined) => void, reject: () => void) => {
            (
                queue[id] ||
                (queue[id] = {
                    token: "",
                    jobs: [],
                })
            ).jobs.push({ ...props, waiting: true, overwrite: false, resolve, reject, onConflict, onRetry, onReload });

            worker(id);
        }) as Promise<T | undefined>;
    } else {
        return client
            .mutate({
                mutation: props.query,
                variables: props.variables,
            })
            .then((value) => value.data && value.data[Object.keys(value.data)[0]])
            .catch(onApiError(props.onError)) as Promise<T | undefined>;
    }
}
