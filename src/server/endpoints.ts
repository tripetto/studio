// Authentication
export const SIGN_IN = "/sign-in";
export const SIGN_IN_VERIFY = "/sign-in-verify";
export const SIGN_OUT = "/sign-out";
export const SIGNED_IN = "/signed-in";
export const SIGNED_OUT = "/signed-out";
export const KEEP_ALIVE = "/keep-alive";
export const DELETE_ACCOUNT = "/goodbye";
export const BLOCKED = "/blocked";

// Api
export const API = "/api";

// Runner
export const RUN = "/run";
export const DEFINITION = RUN + "/definition";
export const SNAPSHOT = RUN + "/snapshot";
export const RESPONSE_ANNOUNCE = RUN + "/response/announce";
export const RESPONSE_SUBMIT = RUN + "/response/submit";
export const STYLES = RUN + "/styles";
export const L10N = RUN + "/l10n";
export const LICENSE = RUN + "/license";

// UMD
export const UMD = "/js";

// Template
export const TEMPLATE = "/template";

// Export
export const EXPORT = "/export";

// Fonts
export const FONTS = "/fonts/";

// Globalization
export const LOCALE = "/locale";
export const TRANSLATION = "/translation";

// Metric
export const METRIC = "/metric";

// File
export const TERMS = "/terms";
export const ATTACHMENT = "/attachment";

// Test
export const TEST_SLACK = "/test/slack";
export const TEST_WEBHOOK = "/test/webhook";
