import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { inject, injectable } from "inversify";
import { logProviderSymbol, userProviderSymbol } from "../../symbols";
import { ILogProvider, IUserProvider } from "../../providers";
import { IUserContext } from "../tokens/interface";
import { IUser, User } from ".";
import { logError } from "../../helpers/error";
import { createHash } from "../../helpers/crypto";
import { MutationResult } from "../mutationresult";
import { TRIPETTO_APP_PK } from "../../settings";

@injectable()
@Resolver()
export class UserResolver {
    private readonly userProvider: IUserProvider;
    private readonly logProvider: ILogProvider;

    constructor(@inject(userProviderSymbol) userProvider: IUserProvider, @inject(logProviderSymbol) logProvider: ILogProvider) {
        this.logProvider = logProvider;
        this.userProvider = userProvider;
    }

    @Authorized()
    @Query(() => User, { nullable: true, description: "Get user info for authenticated user." })
    user(@Ctx() context: IUserContext): Promise<IUser | undefined> {
        return this.userProvider
            .readById(context.token.user)
            .then((value?: IUser) => value)
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update password for authenticated user." })
    changePassword(
        @Arg("password", { description: "New password for the user." }) password: string,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.userProvider
            .changePassword(context.token.user, (userId) => createHash(`${TRIPETTO_APP_PK}${userId}${password}`))
            .then(() => {
                const result = new MutationResult();

                result.isSuccess = true;

                return result;
            })
            .catch(logError(this.logProvider));
    }
}
