import { Field, ObjectType } from "type-graphql";
import { IUser } from ".";

@ObjectType({ description: "Describes a user." })
export class User implements IUser {
    @Field({ description: "Emailaddress of user." })
    emailAddress!: string;

    @Field({ description: "Public key of user. " })
    publicKey!: string;

    @Field({ description: "Language of user. " })
    language!: string;

    @Field({ description: "Locale of user. " })
    locale!: string;
}
