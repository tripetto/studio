import { IPublic } from "../public";

/** Describes a user. */
export interface IUser extends IPublic {
    /** Language of the user. */
    language: string;

    /** Locale of the user. */
    locale: string;

    /** Emailaddress of user. */
    emailAddress: string;

    /** Specifies if a password is set. */
    hasPassword?: boolean;
}
