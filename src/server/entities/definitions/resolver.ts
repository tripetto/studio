import { IUser } from "../users";
import { IHookSettings, IServerDefinition, ITrackers } from "./interface";
import { TRunners } from "./runners";
import { TL10n, TStyles } from "@tripetto/runner";
import { IDefinition } from "@tripetto/builder";
import { Definition } from ".";
import { MutationResult } from "../mutationresult";
import { inject, injectable } from "inversify";
import {
    definitionProviderSymbol,
    logProviderSymbol,
    metricProviderSymbol,
    responseProviderSymbol,
    shortIdProviderSymbol,
    tokenAliasProviderSymbol,
    tokenProviderSymbol,
    userProviderSymbol,
} from "../../symbols";
import { Arg, Authorized, Ctx, Info, Mutation, Query, Resolver } from "type-graphql";
import { IUserContext } from "../tokens/interface";
import { GraphQLResolveInfo } from "graphql";
import { getFieldsFromQuery } from "../helpers/query";
import {
    IDefinitionProvider,
    ILogProvider,
    IMetricProvider,
    IResponseProvider,
    IShortIdProvider,
    ITokenAliasProvider,
    ITokenProvider,
    IUserProvider,
} from "../../providers";
import { logError } from "../../helpers/error";
import { ITokenAlias } from "../../providers/token-alias";
import {
    definition as definitionScalar,
    hooks as hooksScalar,
    l10n as l10nScalar,
    runners as runnersScalar,
    styles as stylesScalar,
    trackers as trackersScalar,
} from "./scalar";

@injectable()
@Resolver()
export class DefinitionResolver {
    private readonly definitionProvider: IDefinitionProvider;
    private readonly userProvider: IUserProvider;
    private readonly responseProvider: IResponseProvider;
    private readonly tokenProvider: ITokenProvider;
    private readonly tokenAliasProvider: ITokenAliasProvider;
    private readonly shortIdProvider: IShortIdProvider;
    private readonly logProvider: ILogProvider;
    private readonly metricProvider: IMetricProvider;

    constructor(
        @inject(definitionProviderSymbol) definitionProvider: IDefinitionProvider,
        @inject(userProviderSymbol) userProvider: IUserProvider,
        @inject(responseProviderSymbol) responseProvider: IResponseProvider,
        @inject(tokenProviderSymbol) tokenProvider: ITokenProvider,
        @inject(tokenAliasProviderSymbol) tokenAliasProvider: ITokenAliasProvider,
        @inject(shortIdProviderSymbol) shortIdProvider: IShortIdProvider,
        @inject(logProviderSymbol) logProvider: ILogProvider,
        @inject(metricProviderSymbol) metricProvider: IMetricProvider
    ) {
        this.logProvider = logProvider;
        this.definitionProvider = definitionProvider;
        this.userProvider = userProvider;
        this.responseProvider = responseProvider;
        this.tokenProvider = tokenProvider;
        this.tokenAliasProvider = tokenAliasProvider;
        this.shortIdProvider = shortIdProvider;
        this.metricProvider = metricProvider;
    }

    private updateTokenAlias(userId: string, user?: IUser, definition?: IServerDefinition): Promise<ITokenAlias | undefined> {
        if (!user || !definition) {
            return Promise.resolve(undefined);
        }

        const alias = this.shortIdProvider.generate();
        const readToken = this.tokenProvider.generateRunnerToken(user.publicKey, definition.publicKey);
        const tokenAlias: ITokenAlias = { token: readToken, alias: alias };

        return Promise.all([
            this.definitionProvider.updateReadTokenAlias(userId, definition.id, alias),
            this.tokenAliasProvider.create(tokenAlias),
        ]).then(() => tokenAlias);
    }

    private readTokenByAlias(alias: string): Promise<ITokenAlias | undefined> {
        return this.tokenAliasProvider.read(alias).then((token?: string) => {
            return token ? { token, alias } : undefined;
        });
    }

    private updateTemplateTokenAlias(userId: string, user?: IUser, definition?: IServerDefinition): Promise<ITokenAlias | undefined> {
        if (!user || !definition) {
            return Promise.resolve(undefined);
        }

        const alias = this.shortIdProvider.generate();
        const templateToken = this.tokenProvider.generateTemplateToken(user.publicKey, definition.publicKey);
        const tokenAlias: ITokenAlias = { token: templateToken, alias: alias };

        return Promise.all([
            this.definitionProvider.updateTemplateTokenAlias(userId, definition.id, alias),
            this.tokenAliasProvider.create(tokenAlias),
        ]).then(() => tokenAlias);
    }

    @Authorized()
    @Query(() => Definition, { description: "Read definition for authenticated user." })
    definition(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Ctx() context: IUserContext,
        @Info() info: GraphQLResolveInfo
    ): Promise<Definition | undefined> {
        return Promise.all([
            this.definitionProvider.readById(context.token.user, id, getFieldsFromQuery(info).includes("definition")),
            this.userProvider.readById(context.token.user),
            this.responseProvider.readCountByDefinition(context.token.user, id),
        ])
            .then(([definition, user, responseCount]: [IServerDefinition | undefined, IUser | undefined, number | undefined]) => {
                if (!definition || !user) {
                    return Promise.resolve(undefined);
                }

                const result = new Definition().load(definition);

                result.responseCount = responseCount || 0;

                return Promise.all([
                    !definition.readTokenAlias
                        ? this.updateTokenAlias(context.token.user, user, definition)
                        : this.readTokenByAlias(definition.readTokenAlias),
                    !definition.templateTokenAlias
                        ? this.updateTemplateTokenAlias(context.token.user, user, definition)
                        : Promise.resolve(undefined),
                ]).then(([readTokenAliasPair, templateTokenAliasPair]: [ITokenAlias | undefined, ITokenAlias | undefined]) => {
                    if (readTokenAliasPair) {
                        result.readToken = readTokenAliasPair.token;
                        result.readTokenAlias = readTokenAliasPair.alias;
                    }
                    if (templateTokenAliasPair) {
                        result.templateTokenAlias = templateTokenAliasPair.alias;
                    }

                    return result;
                });
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => Definition, { description: "Create definition for authenticated user." })
    createDefinition(
        @Arg("runner", () => runnersScalar, { description: "New runner type." }) runner: TRunners,
        @Ctx() context: IUserContext
    ): Promise<Definition | undefined> {
        return Promise.all([this.definitionProvider.create(context.token.user, runner), this.userProvider.readById(context.token.user)])
            .then(([definition, user]: [IServerDefinition | undefined, IUser | undefined]) => {
                return Promise.all([
                    this.updateTokenAlias(context.token.user, user, definition),
                    this.updateTemplateTokenAlias(context.token.user, user, definition),
                ]).then(([readTokenAliasPair, templateTokenAliasPair]: [ITokenAlias | undefined, ITokenAlias | undefined]) => {
                    if (definition && readTokenAliasPair) {
                        definition.readToken = readTokenAliasPair.token;
                        definition.readTokenAlias = readTokenAliasPair.alias;
                    }
                    if (definition && templateTokenAliasPair) {
                        definition.templateTokenAlias = templateTokenAliasPair.alias;
                    }
                    return definition;
                });
            })
            .then((definition?: IServerDefinition) => {
                if (!definition) {
                    return undefined;
                }
                this.metricProvider.add({
                    event: "create",
                    subject: "definition",
                    subjectId: definition.id,
                    userId: context.token.user,
                });

                return new Definition().load(definition);
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update definition for authenticated user." })
    updateDefinition(
        @Arg("id", { description: "Id of the form." }) id: string,
        @Arg("definition", () => definitionScalar, { description: "Definition to update." }) definition: IDefinition,
        @Arg("updateToken", () => String, { nullable: true, description: "Specifies the update token." }) updateToken: string | undefined,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateDefinition(context.token.user, id, definition, updateToken)
            .then((nextToken) => ({ id, isSuccess: true, updateToken: nextToken }))
            .catch(logError(this.logProvider))
            .catch(() => ({ id, isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update runner for authenticated user." })
    updateRunner(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Arg("runner", () => runnersScalar, { description: "New runner type." }) input: TRunners,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateRunner(context.token.user, id, input)
            .then(() => ({ id, isSuccess: true }))
            .catch(logError(this.logProvider))
            .catch(() => ({ id, isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update styles for authenticated user." })
    updateStyles(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Arg("styles", () => stylesScalar, { description: "Style to update." }) styles: TStyles,
        @Arg("updateToken", () => String, { nullable: true, description: "Specifies the update token." }) updateToken: string | undefined,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateStyles(context.token.user, id, styles, updateToken)
            .then((nextToken) => ({ id, isSuccess: true, updateToken: nextToken }))
            .catch(logError(this.logProvider))
            .catch(() => ({ id, isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update l10n data for authenticated user." })
    updateL10n(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Arg("l10n", () => l10nScalar, { description: "L10n data to update." }) l10n: TL10n,
        @Arg("updateToken", () => String, { nullable: true, description: "Specifies the update token." }) updateToken: string | undefined,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateL10n(context.token.user, id, l10n, updateToken)
            .then((nextToken) => ({ id, isSuccess: true, updateToken: nextToken }))
            .catch(logError(this.logProvider))
            .catch(() => ({ id, isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update hooks for authenticated user." })
    updateHooks(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Arg("hooks", () => hooksScalar, { description: "Hooks to update." }) hooks: IHookSettings,
        @Arg("updateToken", () => String, { nullable: true, description: "Specifies the update token." }) updateToken: string | undefined,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateHooks(context.token.user, id, hooks, updateToken)
            .then((nextToken) => ({ id, isSuccess: true, updateToken: nextToken }))
            .catch(logError(this.logProvider))
            .catch(() => ({ id, isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update trackers for authenticated user." })
    updateTrackers(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Arg("trackers", () => trackersScalar, { description: "Trackers to update." }) trackers: ITrackers,
        @Arg("updateToken", () => String, { nullable: true, description: "Specifies the update token." }) updateToken: string | undefined,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateTrackers(context.token.user, id, trackers, updateToken)
            .then((nextToken) => ({ id, isSuccess: true, updateToken: nextToken }))
            .catch(logError(this.logProvider))
            .catch(() => ({ id, isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Delete definition for authenticated user." })
    deleteDefinition(@Arg("id", { description: "Id of definition." }) id: string, @Ctx() context: IUserContext): Promise<MutationResult> {
        this.metricProvider.add({
            event: "delete",
            subject: "definition",
            subjectId: id,
            userId: context.token.user,
        });

        return this.definitionProvider
            .delete(context.token.user, id)
            .then(() => ({ id, isSuccess: true }))
            .catch(logError(this.logProvider));
    }
}
