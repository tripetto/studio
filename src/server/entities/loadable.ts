/* eslint-disable @typescript-eslint/no-explicit-any */
export abstract class Loadable<T extends {}> {
    load(data: T): this {
        const src = data as {
            [key: string]: any;
        };

        const dest = this as {
            [key: string]: any;
        };

        Object.keys(data).forEach((key: string) => {
            dest[key] = src[key];
        });

        return this;
    }
}
