import { Field, ObjectType } from "type-graphql";
import { IMutationResult } from "./interface";

@ObjectType({ description: "Describes the result of a mutation." })
export class MutationResult implements IMutationResult {
    @Field()
    id!: string;

    @Field()
    isSuccess!: boolean;

    @Field({ nullable: true, description: "Numeric token required for updating the definition." })
    updateToken?: string;

    @Field({ nullable: true })
    message?: string;
}
