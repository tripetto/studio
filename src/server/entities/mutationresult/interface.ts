export interface IMutationResult {
    id: string;
    isSuccess: boolean;
    updateToken?: string;
    message?: string;
}
