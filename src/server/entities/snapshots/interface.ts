import { ISnapshot as ISnapshotData } from "@tripetto/runner";

export interface ISnapshot {
    id: string;
    key: string;
    data?: ISnapshotData;
    created: number;
    modified: number;
}
