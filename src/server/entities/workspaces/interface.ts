import { Components } from "@tripetto/builder";

export interface IServerWorkspaceTile {
    readonly type: "workspace" | "form";
    readonly id: string;
    name?: string;
    runner?: "autoscroll" | "chat" | "classic";
    count?: number;
}

export interface IServerWorkspace {
    id: string;
    name: string;
    count: number;
    workspace?: Components.IWorkspace<IServerWorkspaceTile>;
    created: number;
    modified: number;
    updateToken: string;
    recovered?: boolean;
}
