import { GraphQLError, GraphQLScalarType, Kind, ValueNode } from "graphql";
import { Components } from "@tripetto/builder";
import { IServerWorkspaceTile } from "./interface";

export const workspace = new GraphQLScalarType({
    name: "workspace",
    description: "Content of a Tripetto workspace",
    parseValue: (value: Components.IWorkspace<IServerWorkspaceTile>): Components.IWorkspace<IServerWorkspaceTile> => {
        return value;
    },
    serialize: (value?: Components.IWorkspace<IServerWorkspaceTile>): Components.IWorkspace<IServerWorkspaceTile> | undefined => {
        return value;
    },
    parseLiteral: (ast: ValueNode): Components.IWorkspace<IServerWorkspaceTile> => {
        // Here we can validate the input
        if (ast.kind === Kind.STRING) {
            return JSON.parse(ast.value);
        }
        throw new GraphQLError(`Can only validate as string, but got a: ${ast.kind}`);
    },
});
