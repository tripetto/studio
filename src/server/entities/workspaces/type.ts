import { IServerWorkspace, IServerWorkspaceTile } from "./interface";
import { Field, ObjectType } from "type-graphql";
import { Loadable } from "../loadable";
import { workspace } from "./scalar";
import { Components } from "@tripetto/builder";

@ObjectType({ description: "Describes a workspace." })
export class Workspace extends Loadable<IServerWorkspace> implements IServerWorkspace {
    @Field({ description: "Id of workspace." })
    id!: string;

    @Field({ nullable: true, description: "Name of workspace." })
    name!: string;

    @Field({ description: "Specifies the number of tiles in the workspace." })
    count!: number;

    @Field(() => workspace, { nullable: true, description: "Data of workspace." })
    workspace?: Components.IWorkspace<IServerWorkspaceTile>;

    @Field({ nullable: true, description: "Timestamp when workspace was created." })
    created!: number;

    @Field({ nullable: true, description: "Timestamp when workspace was last modified." })
    modified!: number;

    @Field({ description: "Token for updating a workspace." })
    updateToken!: string;

    @Field({ nullable: true, description: "Specifies if the workspace was recovered." })
    recovered!: boolean;
}
