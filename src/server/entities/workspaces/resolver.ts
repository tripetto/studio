import { GraphQLResolveInfo } from "graphql";
import { Arg, Authorized, Ctx, Info, Mutation, Query, Resolver } from "type-graphql";
import { inject, injectable } from "inversify";
import { Components } from "@tripetto/builder";
import { logProviderSymbol, metricProviderSymbol, workspaceProviderSymbol } from "../../symbols";
import { ILogProvider, IMetricProvider, IWorkspaceProvider } from "../../providers";
import { IServerWorkspace, IServerWorkspaceTile, Workspace } from ".";
import { IUserContext } from "../tokens/interface";
import { IMutationResult, MutationResult } from "../mutationresult";
import { getFieldsFromQuery } from "../helpers/query";
import { logError } from "../../helpers/error";
import { workspace as workspaceScalar } from "./scalar";

@injectable()
@Resolver()
export class WorkspaceResolver {
    private readonly workspaceProvider: IWorkspaceProvider;
    private readonly logProvider: ILogProvider;
    private readonly metricProvider: IMetricProvider;

    constructor(
        @inject(workspaceProviderSymbol) workspaceProvider: IWorkspaceProvider,
        @inject(logProviderSymbol) logProvider: ILogProvider,
        @inject(metricProviderSymbol) metricProvider: IMetricProvider
    ) {
        this.logProvider = logProvider;
        this.workspaceProvider = workspaceProvider;
        this.logProvider = logProvider;
        this.metricProvider = metricProvider;
    }

    @Authorized()
    @Query(() => Workspace, { nullable: true, description: "Get specific workspace for authenticated user." })
    workspace(
        @Arg("id", { description: "Id of workspace." }) id: string,
        @Ctx() context: IUserContext,
        @Info() info: GraphQLResolveInfo
    ): Promise<Workspace | undefined> {
        return this.workspaceProvider
            .read(context.token.user, id, getFieldsFromQuery(info).includes("workspace"))
            .then((workspace: IServerWorkspace | undefined) => (workspace && new Workspace().load(workspace)) || undefined)
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Query(() => Workspace, { nullable: true, description: "Get root workspace for authenticated user." })
    rootWorkspace(@Ctx() context: IUserContext, @Info() info: GraphQLResolveInfo): Promise<Workspace | undefined> {
        return this.workspaceProvider
            .readRoot(context.token.user, getFieldsFromQuery(info).includes("workspace"))
            .then((workspace?: IServerWorkspace) => (workspace && new Workspace().load(workspace)) || undefined)
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => Workspace, { nullable: true, description: "Create workspace for authenticated user." })
    createWorkspace(@Ctx() context: IUserContext): Promise<Workspace | undefined> {
        return this.workspaceProvider
            .create(context.token.user)
            .then((workspace?: IServerWorkspace) => {
                if (!workspace) {
                    return undefined;
                }

                this.metricProvider.add({ event: "create", subject: "workspace", subjectId: workspace?.id, userId: context.token.user });

                return new Workspace().load(workspace);
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update workspace for authenticated user." })
    updateWorkspace(
        @Arg("id", { description: "Id of the workspace." }) id: string,
        @Arg("name", { description: "name of the workspace." }) name: string,
        @Arg("workspace", () => workspaceScalar, { description: "Workspace to update." })
        workspace: Components.IWorkspace<IServerWorkspaceTile>,
        @Arg("updateToken", () => String, { nullable: true, description: "Specifies the update token." }) updateToken: string | undefined,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.workspaceProvider
            .update(context.token.user, id, name, workspace, updateToken)
            .then((nextToken) => ({ id, isSuccess: true, updateToken: nextToken }))
            .catch(logError(this.logProvider))
            .catch(() => ({ id, isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Delete workspace for authenticated user." })
    deleteWorkspace(@Arg("id", { description: "Id of workspace." }) id: string, @Ctx() context: IUserContext): Promise<IMutationResult> {
        return this.workspaceProvider
            .delete(context.token.user, id)
            .then(() => {
                this.metricProvider.add({
                    event: "delete",
                    subject: "workspace",
                    subjectId: id,
                    userId: context.token.user,
                });
                return { id, isSuccess: true };
            })
            .catch(logError(this.logProvider));
    }
}
