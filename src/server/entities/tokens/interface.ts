import { TokenTypes } from "./type";

export interface IUserContext {
    token: IAuthenticationToken;
}

export interface IAccessToken extends IToken {
    publicKey: string;
}

export interface IAuthenticationToken extends IToken {
    user: string;
}

export interface IRunnerToken extends IAuthenticationToken {
    definition: string;
    snapshot?: string;
}

export interface ITemplateToken extends IAuthenticationToken {
    definition: string;
}

export interface IAttachmentToken extends IToken {
    name: string;
    user: string;
}

export interface IToken {
    /** Timestamp the token was issued at (seconds since epoch time).  */
    iat?: number;
    type: TokenTypes;
}
