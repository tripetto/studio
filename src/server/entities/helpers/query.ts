import { FieldNode, GraphQLResolveInfo, SelectionNode } from "graphql";

export function getFieldsFromQuery(info: GraphQLResolveInfo): string[] {
    const fields: string[] = [];

    info.fieldNodes.forEach((fieldNode: FieldNode) => {
        if (fieldNode && fieldNode.selectionSet) {
            fieldNode.selectionSet.selections.forEach((value: SelectionNode) => {
                if (value.kind === "Field") {
                    fields.push(value.name.value);
                }
            });
        }
    });
    return fields;
}
