import { Container } from "inversify";
import { UserResolver } from "./entities/users";
import { JwtTokenProvider } from "./providers/jwt";
import { NanoIdProvider } from "./providers/nano-id";
import { FilestoreProvider } from "./providers/filestore";
import { SendGridEmailServiceProvider } from "./providers/sendgrid";
import { BucketProvider } from "./providers/google-cloud-platform/bucket";
import {
    BucketLogProvider,
    DefinitionProvider,
    LogProvider,
    LoginTokenProvider,
    MetricProvider,
    ResponseProvider,
    SnapshotProvider,
    TokenAliasProvider,
    UserProvider,
    WorkspaceProvider,
} from "./providers/google-cloud-platform";
import * as Symbols from "./symbols";
import { WorkspaceResolver } from "./entities/workspaces";
import { DefinitionResolver } from "./entities/definitions";
import { ResponseResolver } from "./entities/responses";

export const dependencyContainer = new Container();

dependencyContainer.bind(Symbols.fileProviderSymbol).to(BucketProvider).inSingletonScope();
dependencyContainer.bind(Symbols.userProviderSymbol).to(UserProvider).inSingletonScope();
dependencyContainer.bind(Symbols.workspaceProviderSymbol).to(WorkspaceProvider).inSingletonScope();
dependencyContainer.bind(Symbols.definitionProviderSymbol).to(DefinitionProvider).inSingletonScope();
dependencyContainer.bind(Symbols.responseProviderSymbol).to(ResponseProvider).inSingletonScope();
dependencyContainer.bind(Symbols.snapshotProviderSymbol).to(SnapshotProvider).inSingletonScope();
dependencyContainer.bind(Symbols.emailServiceProviderSymbol).to(SendGridEmailServiceProvider).inSingletonScope();
dependencyContainer.bind(Symbols.tokenProviderSymbol).to(JwtTokenProvider).inSingletonScope();
dependencyContainer.bind(Symbols.logProviderSymbol).to(LogProvider).inSingletonScope();
dependencyContainer.bind(Symbols.largeLogProviderSymbol).to(BucketLogProvider).inSingletonScope();
dependencyContainer.bind(Symbols.metricProviderSymbol).to(MetricProvider).inSingletonScope();
dependencyContainer.bind(Symbols.loginTokenProviderSymbol).to(LoginTokenProvider).inSingletonScope();
dependencyContainer.bind(Symbols.tokenAliasProviderSymbol).to(TokenAliasProvider).inSingletonScope();
dependencyContainer.bind(Symbols.shortIdProviderSymbol).to(NanoIdProvider).inSingletonScope();
dependencyContainer.bind(Symbols.attachmentProviderSymbol).to(FilestoreProvider).inSingletonScope();

dependencyContainer.bind(UserResolver).toSelf().inSingletonScope();
dependencyContainer.bind(WorkspaceResolver).toSelf().inSingletonScope();
dependencyContainer.bind(DefinitionResolver).toSelf().inSingletonScope();
dependencyContainer.bind(ResponseResolver).toSelf().inSingletonScope();
