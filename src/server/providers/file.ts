export interface IFileProvider {
    /** Delete a user. */
    deleteUser: (userId: string) => Promise<void>;

    /** Writes a workspace file.  */
    writeWorkspace: (userId: string, workspaceId: string, content: string) => void;

    /** Reads a workspace file.  */
    readWorkspace: (userId: string, workspaceId: string) => Promise<Buffer | string | undefined>;

    /** Deletes a workspace file.  */
    deleteWorkspace: (userId: string, workspaceId: string) => Promise<void>;

    /** Writes a definition file.  */
    writeDefinition: (userId: string, definitionId: string, content: string) => void;

    /** Reads a definition file.  */
    readDefinition: (userId: string, definitionId: string) => Promise<Buffer | string | undefined>;

    /** Deletes a definition file.  */
    deleteDefinition: (userId: string, definitionId: string) => Promise<void>;

    /** Writes a response file.  */
    writeResponse: (userId: string, definitionId: string, responseId: string, content: string) => void;

    /** Reads a response file.  */
    readResponse: (userId: string, definitionId: string, responseId: string) => Promise<Buffer | string | undefined>;

    /** Deletes a response file.  */
    deleteResponse: (userId: string, definitionId: string, responseId: string) => Promise<void>;

    /** Deletes all responses of a definition. */
    deleteResponses: (userId: string, definitionId: string) => Promise<void>;

    /** Writes a snapshot file.  */
    writeSnapshot: (userId: string, definitionId: string, snapshotId: string, content: string) => void;

    /** Reads a snapshot file.  */
    readSnapshot: (userId: string, definitionId: string, snapshotId: string) => Promise<Buffer | string | undefined>;

    /** Deletes a snapshot file.  */
    deleteSnapshot: (userId: string, definitionId: string, snapshotId: string) => Promise<void>;

    /** Deletes all snapshots of a definition. */
    deleteSnapshots: (userId: string, definitionId: string) => Promise<void>;
}
