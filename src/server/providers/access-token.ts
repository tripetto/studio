/** Describes the access token provider. */
export interface IAccessTokenProvider {
    /** Create a access token. */
    readonly create: (accessToken: string, loginToken: string, password: string) => Promise<string | undefined>;

    /** Read an access token. */
    readonly readAccessToken: (token: string) => Promise<string | undefined>;

    /** Read a login token. */
    readonly readLoginToken: (token: string) => Promise<
        | {
              id?: string;
              password?: string;
              accessToken?: string;
          }
        | undefined
    >;

    /** Delete an access token. */
    readonly delete: (id: string) => Promise<void>;

    /** Performs access token housekeeping (removes expired tokens). */
    readonly housekeeping: (fnVerify: (token: string) => Promise<boolean>) => Promise<void>;
}
