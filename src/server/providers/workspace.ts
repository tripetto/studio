import { Components } from "@tripetto/builder";
import { IServerWorkspace, IServerWorkspaceTile } from "../entities/workspaces/interface";

/** Describes the workspace provider. */
export interface IWorkspaceProvider {
    /** Create a workspace. */
    readonly create: (userId: string) => Promise<IServerWorkspace | undefined>;

    /** Read a workspace. */
    readonly read: (userId: string, workspaceId: string, includeData: boolean) => Promise<IServerWorkspace | undefined>;

    /** Read the root workspace. */
    readonly readRoot: (userId: string, includeData: boolean) => Promise<IServerWorkspace | undefined>;

    /** Update a workspace. */
    readonly update: (
        userId: string,
        workspaceId: string,
        name: string,
        workspace: Components.IWorkspace<IServerWorkspaceTile>,
        updateToken: string | undefined
    ) => Promise<string | undefined>;

    /** Delete a workspace. */
    readonly delete: (userId: string, workspaceId: string) => Promise<void>;
}
