/** Describes an attachment provider. */
export interface IAttachmentProvider {
    /** Deletes an attachment.  */
    delete: (name: string, user: string, definition: string) => Promise<void>;

    /** Confirms an attachment.  */
    confirm: (name: string, user: string, definition: string) => Promise<void>;
}
