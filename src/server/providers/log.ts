/** Provider for logging messages. */
export interface ILogProvider {
    /* Log a debug message. */
    debug(data: {}): void;
    /* Log an info message. */
    info(data: {}, logName?: string): void;
    /* Log a warning message. */
    warning(data: {}): void;
    /* Log an error message. */
    error(data: {}): void;
    /* Log a critical message. */
    critical(data: {}): void;
}
