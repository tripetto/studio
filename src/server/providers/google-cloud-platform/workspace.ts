import { DocumentReference, DocumentSnapshot, FieldValue, Timestamp } from "@google-cloud/firestore";
import { Components, DateTime, SHA2, Str, castToNumber, filter, findFirst, map, reduce } from "@tripetto/builder";
import { FirestoreProvider } from "./firestore";
import { definitionProviderSymbol, fileProviderSymbol, responseProviderSymbol } from "../../symbols";
import { inject, injectable } from "inversify";
import { IWorkspaceProvider } from "../workspace";
import { IServerWorkspace, IServerWorkspaceTile } from "../../entities/workspaces/interface";
import { IFileProvider } from "../file";
import { IDefinitionProvider } from "../definition";
import { IResponseProvider } from "../response";
import { createHash } from "../../helpers/crypto";

/* eslint-disable no-case-declarations */

const ROOT_ID = "root";

@injectable()
export class WorkspaceProvider extends FirestoreProvider implements IWorkspaceProvider {
    private readonly fileProvider: IFileProvider;
    private readonly definitionProvider: IDefinitionProvider;
    private readonly responseProvider: IResponseProvider;

    constructor(
        @inject(fileProviderSymbol) fileProvider: IFileProvider,
        @inject(definitionProviderSymbol) definitionProvider: IDefinitionProvider,
        @inject(responseProviderSymbol) responseProvider: IResponseProvider
    ) {
        super();
        this.fileProvider = fileProvider;
        this.definitionProvider = definitionProvider;
        this.responseProvider = responseProvider;
    }

    private createRoot(userId: string): Promise<IServerWorkspace | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .collection(this.getWorkspacesPath(userId))
                .doc(ROOT_ID)
                .create(this.getServerTimestampValues())
                .then(() => {
                    const now = this.getNowInMilliseconds();

                    return {
                        id: ROOT_ID,
                        name: "",
                        count: 0,
                        created: now,
                        modified: now,
                        updateToken: createHash(`update:0`),
                    };
                });
        });
    }

    create(userId: string): Promise<IServerWorkspace | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .collection(this.getWorkspacesPath(userId))
                .add(this.getServerTimestampValues())
                .then((doc: DocumentReference) => doc.id)
                .then((id: string) => this.read(userId, id, false));
        });
    }

    async fill(workspace: Components.IWorkspace<IServerWorkspaceTile>, userId: string): Promise<void> {
        await Promise.all(
            (workspace.collections || []).map(async (collection) => {
                const invalids: number[] = [];

                await Promise.all(
                    (collection.tiles || []).map(async (tile, index) => {
                        if (tile.data) {
                            switch (tile.data.type) {
                                case "workspace":
                                    const data = (tile.data.id && (await this.read(userId, tile.data.id, false))) || undefined;

                                    if (data) {
                                        tile.data.name = data.name || "";
                                        tile.data.count = data.count || 0;

                                        return;
                                    }

                                    break;
                                case "form":
                                    const count =
                                        (tile.data.id && (await this.responseProvider.readCountByDefinition(userId, tile.data.id))) ||
                                        undefined;
                                    const definition =
                                        (tile.data.id && (await this.definitionProvider.readById(userId, tile.data.id, false))) ||
                                        undefined;

                                    if (definition) {
                                        tile.data.name = definition.name || "";
                                        tile.data.runner = definition.runner || "";
                                        tile.data.count = count || 0;

                                        return;
                                    }

                                    break;
                            }
                        }

                        // No data found for the tile, remove it from the workspace collection
                        invalids.push(index);
                    })
                );

                if (invalids.length > 0) {
                    invalids
                        .sort((a, b) => b - a)
                        .forEach((index) => {
                            collection.tiles.splice(index, 1);
                        });
                }
            })
        );
    }

    read(userId: string, workspaceId: string, includeData: boolean): Promise<IServerWorkspace | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .doc(this.getWorkspacePath(userId, workspaceId))
                .get()
                .then((workspaceSnapshot: DocumentSnapshot) => {
                    if (!workspaceSnapshot.exists) {
                        return Promise.resolve(undefined);
                    }
                    const data = workspaceSnapshot.data();

                    if (!data) {
                        return undefined;
                    }

                    const workspace: IServerWorkspace = {
                        id: workspaceId,
                        name: data.name,
                        count: castToNumber(data.count, data.tileCount || 0),
                        updateToken: createHash(`update:${data.updateCount || 0}`),
                        created: (data.created as Timestamp).toMillis(),
                        modified: (data.modified as Timestamp).toMillis(),
                    };

                    if (includeData) {
                        return this.fileProvider.readWorkspace(userId, workspaceId).then(async (file?: string | Buffer) => {
                            let workspaceData = (file && file.toString()) || "";

                            if (workspaceData.indexOf(`"ref":`) !== -1) {
                                workspaceData = Str.replace(
                                    Str.replace(JSON.stringify(JSON.parse(workspaceData)), `"ref":{"type":"`, `"data":{"type":"`),
                                    `"data":"`,
                                    `"id":"`
                                );
                            }

                            workspace.workspace =
                                (workspaceData && (JSON.parse(workspaceData) as Components.IWorkspace<IServerWorkspaceTile>)) || undefined;

                            if (workspace.workspace) {
                                try {
                                    await this.fill(workspace.workspace, userId);
                                    // eslint-disable-next-line no-empty
                                } catch {}
                            }

                            return workspace;
                        });
                    }

                    return workspace;
                });
        });
    }

    readRoot(userId: string, includeData: boolean): Promise<IServerWorkspace | undefined> {
        return this.requireUserBeforeExecute(userId, (userDocument) => {
            return userDocument.get().then((value) => {
                const recoveryMode = includeData && value.get("recoveryMode") === true;

                if (recoveryMode) {
                    const doc = this.firestore.doc(this.getUserPath(userId));

                    doc.get().then((user) => {
                        if (user.exists) {
                            doc.update({ recoveryMode: FieldValue.delete() });
                        }
                    });
                }

                return this.read(userId, ROOT_ID, includeData).then((existingRoot?: IServerWorkspace) => {
                    if (existingRoot) {
                        if (recoveryMode && existingRoot.workspace) {
                            return this.list(userId).then((workspaceIds) =>
                                this.definitionProvider.list(userId).then((definitionIds) =>
                                    Promise.all((workspaceIds || []).map((workspaceId) => this.read(userId, workspaceId, true))).then(
                                        (workspaces) => {
                                            const workspaceData = workspaces.map((workspace) => JSON.stringify(workspace?.workspace || {}));
                                            const missingWorkspaces = filter(
                                                workspaceIds,
                                                (id) => id !== "root" && !findFirst(workspaceData, (data) => data.indexOf(`"${id}"`) !== -1)
                                            );
                                            const missingDefinitions = filter(
                                                definitionIds,
                                                (id) => !findFirst(workspaceData, (data) => data.indexOf(`"${id}"`) !== -1)
                                            );

                                            if (missingWorkspaces.length > 0 || missingDefinitions.length > 0) {
                                                const name = `Recovered items (${DateTime.format("dd-MM-y HH:mm")})`;

                                                existingRoot.workspace?.collections.push({
                                                    id: SHA2.SHA2_256(name),
                                                    name,
                                                    tiles: [
                                                        ...missingWorkspaces.map((missingWorkspace) => ({
                                                            id: SHA2.SHA2_256(missingWorkspace),
                                                            data: {
                                                                type: "workspace" as const,
                                                                id: missingWorkspace,
                                                            },
                                                        })),
                                                        ...missingDefinitions.map((missingDefinition) => ({
                                                            id: SHA2.SHA2_256(missingDefinition),
                                                            data: {
                                                                type: "form" as const,
                                                                id: missingDefinition,
                                                            },
                                                        })),
                                                    ],
                                                });
                                            }

                                            existingRoot.recovered = true;

                                            return existingRoot;
                                        }
                                    )
                                )
                            );
                        }

                        return existingRoot;
                    }

                    return this.createRoot(userId);
                });
            });
        });
    }

    update(
        userId: string,
        workspaceId: string,
        name: string,
        workspace: Components.IWorkspace<IServerWorkspaceTile>,
        updateToken: string | undefined
    ): Promise<string | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return new Promise<string>((fnResolve: (updateToken: string) => void, fnReject: () => void) => {
                const workspaceData = JSON.stringify(workspace);

                if (
                    !workspace ||
                    !workspaceData ||
                    workspaceData === "{}" ||
                    workspaceData === "null" ||
                    // Reject calls from outdated client bundles
                    workspaceData.indexOf(`"ref":{`) !== -1
                ) {
                    fnReject();

                    return;
                }

                this.firestore
                    .doc(this.getWorkspacePath(userId, workspaceId))
                    .get()
                    .then((workspaceSnapshot: DocumentSnapshot) => {
                        let updateCount: number = (workspaceSnapshot.exists && workspaceSnapshot.data()?.updateCount) || 0;

                        if (!updateToken || createHash(`update:${updateCount}`) === updateToken) {
                            updateCount++;

                            Promise.all([
                                this.firestore.doc(this.getWorkspacePath(userId, workspaceId)).update({
                                    name,
                                    count: reduce(
                                        map(workspace.collections, (collection) => (collection.tiles && collection.tiles.length) || 0),
                                        (count: number, tiles: number) => count + tiles,
                                        0
                                    ),
                                    tileCount: FieldValue.delete(),
                                    modified: FieldValue.serverTimestamp(),
                                    updateCount,
                                }),
                                this.fileProvider.writeWorkspace(userId, workspaceId, workspaceData),
                            ])
                                .then(() => fnResolve(createHash(`update:${updateCount}`)))
                                .catch(() => fnReject());
                        } else {
                            fnResolve("");
                        }
                    });
            });
        });
    }

    list(userId: string): Promise<string[] | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .collection(this.getWorkspacesPath(userId))
                .listDocuments()
                .then((documents: DocumentReference[]) => documents.map((value: DocumentReference) => value.id));
        });
    }

    delete(userId: string, workspaceId: string): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return Promise.all([
                this.fileProvider.deleteWorkspace(userId, workspaceId),
                // Only delete firestore document when file is actually deleted.
                this.firestore.doc(this.getWorkspacePath(userId, workspaceId)).delete(),
            ]).then(() => Promise.resolve());
        });
    }
}
