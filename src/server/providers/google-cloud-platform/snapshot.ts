import { FirestoreProvider } from "./firestore";
import { fileProviderSymbol } from "../../symbols";
import { inject, injectable } from "inversify";
import { DocumentReference, DocumentSnapshot, FieldValue, QuerySnapshot } from "@google-cloud/firestore";
import { ISnapshotProvider } from "../snapshot";
import { ISnapshot } from "../../entities/snapshots/interface";
import { IFileProvider } from "../file";

interface ISnapshotContext {
    user?: DocumentReference;
    definition?: DocumentReference;
}

@injectable()
export class SnapshotProvider extends FirestoreProvider implements ISnapshotProvider {
    private readonly fileProvider: IFileProvider;

    constructor(@inject(fileProviderSymbol) fileProvider: IFileProvider) {
        super();

        this.fileProvider = fileProvider;
    }

    private getSnapshotContext(userPublicKey: string, definitionPublicKey: string): Promise<ISnapshotContext> {
        const context: ISnapshotContext = {};
        return this.getDocumentByPublicKey(this.firestore.collection(this.userCollectionName), userPublicKey)
            .then((user?: DocumentReference) => {
                if (user) {
                    context.user = user;
                    return this.getDocumentByPublicKey(user.collection(this.definitionCollectionName), definitionPublicKey);
                }
                return undefined;
            })
            .then((definition?: DocumentReference) => {
                if (definition) {
                    context.definition = definition;
                }
                return context;
            });
    }

    private create(
        userId: string,
        definition: DocumentReference,
        key: string,
        data: string,
        runner: string,
        language: string,
        locale: string
    ): Promise<string | undefined> {
        let docId: string | undefined = undefined;

        return definition
            .collection(this.snapshotCollectionName)
            .add({
                created: FieldValue.serverTimestamp(),
                key,
                runner,
                language,
                locale,
            })
            .then((doc: DocumentReference) => {
                docId = doc.id;

                this.fileProvider.writeSnapshot(userId, definition.id, doc.id, data);

                return doc.id;
            })
            .catch((err: Error) => {
                if (docId) {
                    // Something wrong while writing the file, so delete the document as well.
                    definition.collection(this.snapshotCollectionName).doc(docId).delete();
                }

                return Promise.reject(err);
            });
    }

    upsert(
        userPublicKey: string,
        definitionPublicKey: string,
        key: string,
        data: string,
        runner: string,
        language: string,
        locale: string
    ): Promise<string | undefined> {
        return this.getSnapshotContext(userPublicKey, definitionPublicKey).then((context: ISnapshotContext) => {
            if (!context.user || !context.definition) {
                return undefined;
            }

            return context.definition
                .collection(this.snapshotCollectionName)
                .where("key", "==", key)
                .get()
                .then((querySnapshot: QuerySnapshot) => {
                    if (querySnapshot.empty) {
                        return this.create(context.user!.id, context.definition!, key, data, runner, language, locale);
                    } else {
                        const snapshotId = querySnapshot.docs[0].id;

                        return context
                            .definition!.collection(this.snapshotCollectionName)
                            .doc(snapshotId)
                            .update({ runner, language, locale })
                            .then(() => this.fileProvider.writeSnapshot(context.user!.id, context.definition!.id, snapshotId, data))
                            .then(() => snapshotId);
                    }
                });
        });
    }

    read(userPublicKey: string, definitionPublicKey: string, snapshotId: string, includeData: boolean): Promise<ISnapshot | undefined> {
        return this.getSnapshotContext(userPublicKey, definitionPublicKey).then((context: ISnapshotContext) => {
            if (!context.user || !context.definition) {
                return undefined;
            }

            return this.firestore
                .doc(this.getSnapshotPath(context.user.id, context.definition.id, snapshotId))
                .get()
                .then((snapshotSnapshot: DocumentSnapshot) => {
                    if (!snapshotSnapshot.exists) {
                        return Promise.resolve(undefined);
                    }
                    const data = snapshotSnapshot.data();
                    if (!data) {
                        return undefined;
                    }

                    const snapshot: ISnapshot = {
                        id: snapshotId,
                        key: data.key,
                        created: snapshotSnapshot.createTime?.toMillis() || 0,
                        modified: snapshotSnapshot.updateTime?.toMillis() || 0,
                    };

                    if (includeData) {
                        return this.fileProvider
                            .readSnapshot(context.user!.id, context.definition!.id, snapshotId)
                            .then((file?: string | Buffer) => {
                                snapshot.data = file && JSON.parse(file.toString());
                                return snapshot;
                            });
                    }
                    return snapshot;
                });
        });
    }

    delete(userPublicKey: string, definitionPublicKey: string, snapshotId: string): Promise<void | undefined> {
        return this.getSnapshotContext(userPublicKey, definitionPublicKey).then((context: ISnapshotContext) => {
            if (!context.user || !context.definition) {
                return undefined;
            }

            return Promise.all([
                this.fileProvider.deleteSnapshot(context.user.id, context.definition.id, snapshotId),
                this.firestore.doc(this.getSnapshotPath(context.user.id, context.definition.id, snapshotId)).delete(),
            ]).then(() => Promise.resolve());
        });
    }
}
