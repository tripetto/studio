import { inject, injectable } from "inversify";
import { DocumentData, DocumentReference, DocumentSnapshot, FieldValue, Timestamp } from "@google-cloud/firestore";
import { fileProviderSymbol, responseProviderSymbol, tokenAliasProviderSymbol } from "../../symbols";
import { IDefinitionProvider, IFileProvider, IResponseProvider, ITokenAliasProvider } from "..";
import { FirestoreProvider } from "./firestore";
import { IHookSettings, IServerDefinition, ITrackers } from "../../entities/definitions/interface";
import { TRunners } from "../../entities/definitions/runners";
import { migrateL10n, migrateStyles } from "../../helpers/migration";
import { TL10n, TStyles } from "@tripetto/runner";
import { IDefinition, SHA2, castToString } from "@tripetto/builder";
import { calculateFingerprintAndStencil } from "@tripetto/runner";
import { createHash } from "../../helpers/crypto";

@injectable()
export class DefinitionProvider extends FirestoreProvider implements IDefinitionProvider {
    private readonly fileProvider: IFileProvider;
    private readonly tokenAliasProvider: ITokenAliasProvider;
    private readonly responseProvider: IResponseProvider;

    constructor(
        @inject(fileProviderSymbol) fileProvider: IFileProvider,
        @inject(tokenAliasProviderSymbol) tokenAliasProvider: ITokenAliasProvider,
        @inject(responseProviderSymbol) responseProvider: IResponseProvider
    ) {
        super();
        this.fileProvider = fileProvider;
        this.tokenAliasProvider = tokenAliasProvider;
        this.responseProvider = responseProvider;
    }

    private saveFile(userId: string, definitionId: string, definitionData: string): void {
        this.fileProvider.writeDefinition(userId, definitionId, definitionData);
    }

    private incrementReadCount(definition: DocumentReference, readCount?: number): number {
        readCount = (readCount || 0) + 1;

        try {
            definition.update({ readCount });
        } catch {
            definition.update({ readCount });
        }

        return readCount;
    }

    private load(id: string, data: DocumentData): IServerDefinition {
        const styles = (data.styles && JSON.parse(data.styles)) || data.style || undefined;
        const l10n = (data.l10n && JSON.parse(data.l10n)) || undefined;

        return {
            id,
            name: data.name,
            publicKey: data.publicKey,
            runner: data.runner || "autoscroll",
            readTokenAlias: data.readTokenAlias || undefined,
            readCount: data.readCount || 0,
            updateToken: createHash(`update:${data.updateCount || 0}`),
            styles: styles && migrateStyles(styles),
            l10n: migrateL10n(l10n, styles),
            templateTokenAlias: data.templateTokenAlias || undefined,
            fingerprint: data.fingerprint || undefined,
            stencil: data.stencil || undefined,
            actionables: data.actionables || undefined,
            hooks: data.hooks || undefined,
            trackers: data.trackers || undefined,
            tier: data.tier || undefined,
            created: (data.created as Timestamp).toMillis(),
            modified: (data.modified as Timestamp).toMillis(),
        };
    }

    private appendDataToDefinition(userId: string, definitionId: string, definition: IServerDefinition): Promise<IServerDefinition> {
        return this.fileProvider.readDefinition(userId, definitionId).then((file?: string | Buffer) => {
            definition.definition = (file && JSON.parse(file.toString())) || ({} as IDefinition);

            return definition;
        });
    }

    create(userId: string, runner: TRunners): Promise<IServerDefinition | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .collection(this.getDefinitionsPath(userId))
                .add({
                    runner,
                    ...this.getServerTimestampValues(),
                })
                .then((documentReference: DocumentReference) => this.setPublicKey(documentReference).then(() => documentReference.id))
                .then((id: string) => this.readById(userId, id, false));
        });
    }

    readById(userId: string, definitionId: string, includeData: boolean): Promise<IServerDefinition | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .get()
                .then((definitionSnapshot: DocumentSnapshot) => {
                    if (!definitionSnapshot.exists) {
                        return undefined;
                    }

                    const data = definitionSnapshot.data();

                    if (!data) {
                        return undefined;
                    }

                    const definition = this.load(definitionId, data);

                    if (includeData) {
                        return this.appendDataToDefinition(userId, definitionId, definition);
                    }

                    return definition;
                });
        });
    }

    readByPublicKey(
        userPublicKey: string,
        definitionPublicKey: string,
        includeData: boolean,
        verifySignature?: {
            token: string;
            signature: string;
        }
    ): Promise<IServerDefinition | undefined> {
        return this.getDocumentByPublicKey(this.firestore.collection(this.userCollectionName), userPublicKey).then(
            (user?: DocumentReference) => {
                if (!user) {
                    return undefined;
                }

                return user.get().then((userSnapshot: DocumentSnapshot) => {
                    const userData = userSnapshot.data();

                    if (userSnapshot.exists && userData && userData.emailAddress) {
                        const emailAddress = castToString(userData.emailAddress);

                        if (
                            verifySignature &&
                            SHA2.SHA2_256(`${emailAddress}:${verifySignature.token}`) !== verifySignature.signature &&
                            SHA2.SHA2_256(`*${emailAddress.substring(emailAddress.lastIndexOf("@"))}:${verifySignature.token}`) !==
                                verifySignature.signature
                        ) {
                            return undefined;
                        }

                        return this.firestore
                            .collection(this.denyCollectionName)
                            .where("emailAddress", "==", userData.emailAddress)
                            .get()
                            .then((denyListSnapshot) => {
                                if (denyListSnapshot.empty) {
                                    return this.getDocumentByPublicKey(
                                        user.collection(this.definitionCollectionName),
                                        definitionPublicKey
                                    ).then((definitionReference?: DocumentReference) => {
                                        if (!definitionReference) {
                                            return undefined;
                                        }

                                        return definitionReference.get().then((snapshot: DocumentSnapshot) => {
                                            const data = snapshot.data();

                                            if (!data) {
                                                return undefined;
                                            }

                                            const definition = this.load(definitionReference.id, data);

                                            if (includeData) {
                                                definition.readCount = this.incrementReadCount(definitionReference, definition.readCount);
                                                return this.appendDataToDefinition(user.id, definition.id, definition);
                                            }
                                            return definition;
                                        });
                                    });
                                }

                                return undefined;
                            });
                    }

                    return undefined;
                });
            }
        );
    }

    list(userId: string): Promise<string[] | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .collection(this.getDefinitionsPath(userId))
                .listDocuments()
                .then((documents: DocumentReference[]) => documents.map((value: DocumentReference) => value.id));
        });
    }

    updateDefinition(
        userId: string,
        definitionId: string,
        definition: IDefinition,
        updateToken: string | undefined
    ): Promise<string | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return new Promise<string>((fnResolve: (updateToken: string) => void, fnReject: () => void) => {
                const definitionData = JSON.stringify(definition);

                if (!definition || !definitionData || definitionData === "{}" || definitionData === "null") {
                    fnReject();

                    return;
                }

                this.firestore
                    .doc(this.getDefinitionPath(userId, definitionId))
                    .get()
                    .then((definitionSnapshot: DocumentSnapshot) => {
                        const signatures = calculateFingerprintAndStencil(definition);
                        let updateCount: number = (definitionSnapshot.exists && definitionSnapshot.data()?.updateCount) || 0;

                        if (!updateToken || createHash(`update:${updateCount}`) === updateToken) {
                            updateCount++;

                            Promise.all([
                                this.firestore.doc(this.getDefinitionPath(userId, definitionId)).update({
                                    name: definition.name || "",
                                    fingerprint: signatures.fingerprint,
                                    stencil: signatures.stencil("exportables"),
                                    actionables: signatures.stencil("actionables"),
                                    modified: FieldValue.serverTimestamp(),
                                    updateCount,
                                }),
                                this.saveFile(userId, definitionId, definitionData),
                            ])
                                .then(() => fnResolve(createHash(`update:${updateCount}`)))
                                .catch(() => fnReject());
                        } else {
                            fnResolve("");
                        }
                    });
            });
        });
    }

    updateRunner(userId: string, definitionId: string, runner: TRunners): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .update({ runner })
                .then(() => Promise.resolve());
        });
    }

    updateStyles(userId: string, definitionId: string, styles: TStyles, updateToken: string | undefined): Promise<string | undefined> {
        return this.requireUserBeforeExecute(userId, () =>
            this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .get()
                .then((definitionSnapshot: DocumentSnapshot) => {
                    let updateCount: number = (definitionSnapshot.exists && definitionSnapshot.data()?.updateCount) || 0;

                    if (!updateToken || createHash(`update:${updateCount}`) === updateToken) {
                        updateCount++;

                        return this.firestore
                            .doc(this.getDefinitionPath(userId, definitionId))
                            .update({
                                styles: JSON.stringify(styles),
                                modified: FieldValue.serverTimestamp(),
                                updateCount,
                            })
                            .then(() => createHash(`update:${updateCount}`));
                    }

                    return "";
                })
        );
    }

    updateL10n(userId: string, definitionId: string, l10n: TL10n, updateToken: string | undefined): Promise<string | undefined> {
        return this.requireUserBeforeExecute(userId, () =>
            this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .get()
                .then((definitionSnapshot: DocumentSnapshot) => {
                    let updateCount: number = (definitionSnapshot.exists && definitionSnapshot.data()?.updateCount) || 0;

                    if (!updateToken || createHash(`update:${updateCount}`) === updateToken) {
                        updateCount++;

                        return this.firestore
                            .doc(this.getDefinitionPath(userId, definitionId))
                            .update({
                                l10n: JSON.stringify(l10n),
                                modified: FieldValue.serverTimestamp(),
                                updateCount,
                            })
                            .then(() => createHash(`update:${updateCount}`));
                    }

                    return "";
                })
        );
    }

    updateHooks(userId: string, definitionId: string, hooks: IHookSettings, updateToken: string | undefined): Promise<string | undefined> {
        return this.requireUserBeforeExecute(userId, () =>
            this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .get()
                .then((definitionSnapshot: DocumentSnapshot) => {
                    let updateCount: number = (definitionSnapshot.exists && definitionSnapshot.data()?.updateCount) || 0;

                    if (!updateToken || createHash(`update:${updateCount}`) === updateToken) {
                        updateCount++;

                        return this.firestore
                            .doc(this.getDefinitionPath(userId, definitionId))
                            .update({
                                hooks,
                                modified: FieldValue.serverTimestamp(),
                                updateCount,
                            })
                            .then(() => createHash(`update:${updateCount}`));
                    }

                    return "";
                })
        );
    }

    updateTrackers(
        userId: string,
        definitionId: string,
        trackers: ITrackers,
        updateToken: string | undefined
    ): Promise<string | undefined> {
        return this.requireUserBeforeExecute(userId, () =>
            this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .get()
                .then((definitionSnapshot: DocumentSnapshot) => {
                    let updateCount: number = (definitionSnapshot.exists && definitionSnapshot.data()?.updateCount) || 0;

                    if (!updateToken || createHash(`update:${updateCount}`) === updateToken) {
                        updateCount++;

                        return this.firestore
                            .doc(this.getDefinitionPath(userId, definitionId))
                            .update({
                                trackers,
                                modified: FieldValue.serverTimestamp(),
                                updateCount,
                            })
                            .then(() => createHash(`update:${updateCount}`));
                    }

                    return "";
                })
        );
    }

    updateReadTokenAlias(userId: string, definitionId: string, readTokenAlias: string): Promise<void> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .update({ readTokenAlias })
                .then(() => Promise.resolve());
        });
    }

    updateTemplateTokenAlias(userId: string, definitionId: string, templateTokenAlias: string): Promise<void> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.firestore
                .doc(this.getDefinitionPath(userId, definitionId))
                .update({ templateTokenAlias })
                .then(() => Promise.resolve());
        });
    }

    delete(userId: string, definitionId: string): Promise<void> {
        return this.requireUserBeforeExecute(userId, () => {
            return this.readById(userId, definitionId, false).then((definition?: IServerDefinition) => {
                if (!definition) {
                    return;
                }

                return this.responseProvider.list(userId, definitionId).then((responses?: string[]) => {
                    return Promise.all([
                        // Delete all responses.
                        responses
                            ? responses.map((responseId: string) =>
                                  this.responseProvider.delete(userId, definitionId, definition.publicKey, responseId)
                              )
                            : [],

                        // Delete all snapshots.
                        this.fileProvider.deleteSnapshots(userId, definitionId),
                        this.deleteCollection(this.getSnapshotsPath(userId, definitionId), 100),

                        // Delete token alias.
                        definition.readTokenAlias ? this.tokenAliasProvider.delete(definition.readTokenAlias) : Promise.resolve(),

                        // Delete definition itself.
                        this.fileProvider.deleteDefinition(userId, definitionId),
                        this.firestore.doc(this.getDefinitionPath(userId, definitionId)).delete(),
                    ]).then(() => Promise.resolve());
                });
            });
        });
    }
}
