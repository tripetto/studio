import { injectable } from "inversify";
import { Entry, Log, Logging } from "@google-cloud/logging";
import { ILogProvider } from "../log";
import { ENV, GOOGLE_CLOUD_PROJECT } from "../../settings";

@injectable()
export class LogProvider implements ILogProvider {
    private log: Log;
    private logging: Logging;
    private readonly defaultLogName = `log_${ENV}`;

    constructor() {
        this.logging = new Logging({ projectId: GOOGLE_CLOUD_PROJECT });
        this.log = this.logging.log(this.defaultLogName);
    }

    private getEntry(data: {}): Entry {
        if (data instanceof Object) {
            data = JSON.parse(JSON.stringify(data));
        }

        return new Entry(undefined, data);
    }

    private logToConsole(data: {}): void {
        if (ENV === "development") {
            console.log(data);
        }
    }

    info(data: {}, logName?: string): void {
        const entry = this.getEntry(data);
        let log = this.log;
        if (logName) {
            log = this.logging.log(logName);
        }
        log.info(entry);
        this.logToConsole(data);
    }

    debug(data: {}): void {
        this.log.debug(this.getEntry(data));
        this.logToConsole(data);
    }

    warning(data: {}): void {
        this.log.warning(this.getEntry(data));
        this.logToConsole(data);
    }

    error(data: {}): void {
        this.log.error(this.getEntry(data));
        this.logToConsole(data);
    }

    critical(data: {}): void {
        this.log.critical(this.getEntry(data));
        this.logToConsole(data);
    }
}
