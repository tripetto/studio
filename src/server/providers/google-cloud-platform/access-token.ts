import { injectable } from "inversify";
import { IAccessTokenProvider } from "..";
import { FirestoreProvider } from "./firestore";

@injectable()
export class AccessTokenProvider extends FirestoreProvider implements IAccessTokenProvider {
    create(token: string, loginToken: string, password: string): Promise<string | undefined> {
        return this.firestore
            .collection(this.accessTokenCollectionName)
            .add({
                token,
                loginToken,
                password,
                used: 0,
            })
            .then((result) => result.id || undefined);
    }

    readAccessToken(token: string): Promise<string | undefined> {
        return this.firestore
            .collection(this.accessTokenCollectionName)
            .where("token", "==", token)
            .get()
            .then((result) => {
                if (!result.empty) {
                    const doc = result.docs[0];

                    if (doc.exists) {
                        const used = doc.data().used || 0;

                        if (used < 2) {
                            return this.firestore
                                .doc(this.getLoginTokenPath(doc.id))
                                .update({
                                    used: used + 1,
                                })
                                .then(() => doc.id);
                        }
                    }
                }

                return undefined;
            });
    }

    readLoginToken(token: string): Promise<
        | {
              id?: string;
              password?: string;
              accessToken?: string;
          }
        | undefined
    > {
        return this.firestore
            .collection(this.accessTokenCollectionName)
            .where("loginToken", "==", token)
            .get()
            .then((result) => {
                if (!result.empty) {
                    const doc = result.docs[0];
                    const data = doc.data();

                    return {
                        id: doc.id,
                        password: data.password || undefined,
                        accessToken: data.token || undefined,
                    };
                }

                return undefined;
            });
    }

    delete(id: string): Promise<void> {
        return this.firestore.doc(this.getLoginTokenPath(id)).delete().then();
    }

    housekeeping(fnVerify: (token: string) => Promise<boolean>): Promise<void> {
        return this.firestore
            .collection(this.accessTokenCollectionName)
            .get()
            .then((tokens) => {
                const promises: Promise<void>[] = [];

                tokens.forEach((token) => {
                    const accessToken = (token.exists && (token.data().token as string)) || undefined;

                    if (!accessToken) {
                        promises.push(this.delete(token.id));
                    } else {
                        promises.push(
                            fnVerify(accessToken).then((result) => {
                                if (!result) {
                                    return this.delete(token.id);
                                }

                                return Promise.resolve();
                            })
                        );
                    }
                });

                return Promise.all(promises).then();
            });
    }
}
