import { injectable } from "inversify";
import { ILogProvider } from "../log";
import { ENV, GOOGLE_CLOUD_PROJECT } from "../../settings";
import { Bucket, Storage } from "@google-cloud/storage";

@injectable()
export class BucketLogProvider implements ILogProvider {
    protected readonly bucket: Bucket;

    constructor() {
        this.bucket = new Storage({
            projectId: GOOGLE_CLOUD_PROJECT,
        }).bucket(`${GOOGLE_CLOUD_PROJECT}-logfiles`);
    }

    private getFilename(level: string): string {
        return `${level}/${ENV}/${Date.now()}.log`;
    }

    private writeFile(filename: string, data: {}): Promise<void> {
        return this.bucket.file(filename).save(JSON.stringify(data));
    }

    info(data: {}): Promise<void> {
        return this.writeFile(this.getFilename("info"), data);
    }

    debug(data: {}): Promise<void> {
        return this.writeFile(this.getFilename("debug"), data);
    }

    warning(data: {}): Promise<void> {
        return this.writeFile(this.getFilename("warning"), data);
    }

    error(data: {}): Promise<void> {
        return this.writeFile(this.getFilename("error"), data);
    }

    critical(data: {}): Promise<void> {
        return this.writeFile(this.getFilename("critical"), data);
    }
}
