import { DocumentReference, DocumentSnapshot, FieldValue, QuerySnapshot, WriteResult } from "@google-cloud/firestore";
import { IUser } from "../../entities/users/interface";
import { IUserProvider } from "../user";
import { FirestoreProvider } from "./firestore";
import { inject, injectable } from "inversify";
import { definitionProviderSymbol, fileProviderSymbol } from "../../symbols";
import { IDefinitionProvider } from "../definition";
import { IFileProvider } from "../file";

@injectable()
export class UserProvider extends FirestoreProvider implements IUserProvider {
    private readonly definitionProvider: IDefinitionProvider;
    private readonly fileProvider: IFileProvider;

    constructor(
        @inject(definitionProviderSymbol) definitionProvider: IDefinitionProvider,
        @inject(fileProviderSymbol) fileProvider: IFileProvider
    ) {
        super();

        this.definitionProvider = definitionProvider;
        this.fileProvider = fileProvider;
    }

    private updateLoginData(user: DocumentReference, snapshot: DocumentSnapshot): Promise<WriteResult> {
        const data = snapshot.data();
        const serverTimestamp = FieldValue.serverTimestamp();

        const updateData: {
            firstLogin?: FieldValue;
            lastLogin: FieldValue;
            loginCount: number;
            publishSettings: {};
            shareSettings: {};
        } = {
            lastLogin: serverTimestamp,
            loginCount: 0,
            publishSettings: FieldValue.delete(),
            shareSettings: FieldValue.delete(),
        };

        if (data) {
            updateData.loginCount = data.loginCount || updateData.loginCount;

            if (!data.firstLogin) {
                updateData.firstLogin = serverTimestamp;
            }
        }

        updateData.loginCount = updateData.loginCount + 1;

        return user.update(updateData);
    }

    create(emailAddress: string, language: string, locale: string): Promise<IUser> {
        return this.firestore
            .collection(this.userCollectionName)
            .add({
                emailAddress: emailAddress.toLowerCase(),
                language,
                locale,
                created: FieldValue.serverTimestamp(),
                loginCount: 0,
            })
            .then((result) =>
                this.setPublicKey(result).then((publicKey) => ({
                    language,
                    locale,
                    emailAddress,
                    publicKey,
                }))
            );
    }

    readByEmail(emailAddress: string, language: string, locale: string): Promise<IUser | undefined> {
        return this.firestore
            .collection(this.userCollectionName)
            .where("emailAddress", "==", emailAddress.toLowerCase())
            .get()
            .then((existingUsers: QuerySnapshot) => {
                if (existingUsers.empty) {
                    return undefined;
                }

                const userDoc = existingUsers.docs[0];
                const userData = userDoc.data();

                return this.firestore
                    .collection(this.denyCollectionName)
                    .where("emailAddress", "==", userData.emailAddress)
                    .get()
                    .then((denyListSnapshot) => {
                        if (denyListSnapshot.empty) {
                            /**
                             * This adds the language and locale for existing users that
                             * don't have these fields in the database.
                             */
                            if (!userData.language || !userData.locale) {
                                userDoc.ref.update({
                                    language: userData.language || language,
                                    locale: userData.locale || locale,
                                });
                            }

                            return {
                                emailAddress: userData.emailAddress,
                                publicKey: userData.publicKey,
                                language: userData.language || language,
                                locale: userData.locale || locale,
                                hasPassword: userData.password ? true : false,
                            };
                        } else {
                            return new Promise((resolve: (user: IUser | undefined) => void, reject: () => void) => reject());
                        }
                    });
            });
    }

    readById(id: string): Promise<IUser | undefined> {
        return this.firestore
            .doc(this.getUserPath(id))
            .get()
            .then((user) => {
                const userData = user.data();

                if (!user.exists || !userData) {
                    return undefined;
                }

                return {
                    emailAddress: userData.emailAddress,
                    publicKey: userData.publicKey,
                    language: userData.language || "",
                    locale: userData.locale || "",
                };
            });
    }

    login(publicKey: string): Promise<string | undefined> {
        return this.getDocumentByPublicKey(this.firestore.collection(this.userCollectionName), publicKey).then(
            (user) => user?.get().then((userData) => this.updateLoginData(user, userData).then(() => user.id)) || undefined
        );
    }

    verifyPassword(publicKey: string, getSaltedPassword: (id: string) => string): Promise<boolean> {
        return this.getDocumentByPublicKey(this.firestore.collection(this.userCollectionName), publicKey).then(
            (doc) =>
                doc?.get().then((user) => {
                    if (user.exists) {
                        const password = user.data()?.password || undefined;

                        if (password && getSaltedPassword(user.id) === password) {
                            return true;
                        }
                    }

                    return false;
                }) || false
        );
    }

    changePassword(id: string, getSaltedPassword: (id: string) => string): Promise<boolean> {
        const doc = this.firestore.doc(this.getUserPath(id));

        return doc.get().then((user) => {
            if (user.exists) {
                return doc.update({ password: getSaltedPassword(id), passwordModified: FieldValue.serverTimestamp() }).then(() => true);
            }

            return false;
        });
    }

    delete(id: string): Promise<void> {
        return this.requireUserBeforeExecute(id, (userDocument) => {
            // Find all definitions.
            return this.definitionProvider.list(id).then((definitions?: string[]) => {
                const deletes: Promise<void>[] = [];

                if (definitions) {
                    deletes.push(...definitions.map((definitionId) => this.definitionProvider.delete(id, definitionId)));
                }

                // Delete all workspaces.
                deletes.push(this.deleteCollection(this.getWorkspacesPath(id), 100));

                return Promise.all(deletes).then(() => Promise.all([userDocument.delete(), this.fileProvider.deleteUser(id)]).then());
            });
        });
    }
}
