import { injectable } from "inversify";
import { IEmailOptions, IEmailServiceProvider } from "../email";
import { SENDGRID_API_KEY } from "../../settings";
import sendgrid from "@sendgrid/mail";

@injectable()
export class SendGridEmailServiceProvider implements IEmailServiceProvider {
    constructor() {
        sendgrid.setApiKey(SENDGRID_API_KEY);
    }

    send(options: IEmailOptions): Promise<void> {
        return sendgrid.send(options).then(() => Promise.resolve());
    }
}
