import { injectable } from "inversify";
import { IShortIdProvider } from "..";
import { customAlphabet } from "nanoid";
import { SHORT_ID_SIZE } from "../../settings";

const nanoId = customAlphabet("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", SHORT_ID_SIZE);

@injectable()
export class NanoIdProvider implements IShortIdProvider {
    generate(): string {
        /*
            ~10 years needed, in order to have a 1% probability of at least one collision.

                - https://zelark.github.io/nano-id-cc/
        */

        return nanoId();
    }
}
