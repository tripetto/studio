/** Provider for metrics. */
export interface IMetricProvider {
    add(metric: IMetric): void;
}

export interface IMetric {
    event: MetricEventType;
    subject: MetricSubjectType;
    subjectId?: string;
    userId?: string;
    ip?: string;
    referer?: string;
    deviceSize?: DeviceSize;
}

export type MetricEventType =
    | "create"
    | "read"
    | "update"
    | "delete"
    | "login_token_invalid"
    | "login_token_used"
    | "share_open_link"
    | "share_copy_link"
    | "share_copy_code"
    | "share_codepen"
    | "register"
    | "login"
    | "logout";

export type MetricSubjectType = "anonymous_user" | "user" | "workspace" | "definition" | "definition_template" | "response" | "snapshot";

export type DeviceSize = "xs" | "s" | "m" | "l" | "xl";
