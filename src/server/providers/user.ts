import { IUser } from "../entities/users/interface";

/** Describes the user provider. */
export interface IUserProvider {
    /** Create a user. */
    readonly create: (emailAddress: string, language: string, locale: string) => Promise<IUser>;

    /** Read a user by its email. */
    readonly readByEmail: (emailAddress: string, language: string, locale: string) => Promise<IUser | undefined>;

    /** Read a user by its id. */
    readonly readById: (id: string) => Promise<IUser | undefined>;

    /** Login the user by the public key. */
    readonly login: (publicKey: string) => Promise<string | undefined>;

    /** Verifies a password. */
    readonly verifyPassword: (publicKey: string, getSaltedPassword: (id: string) => string) => Promise<boolean>;

    /** Change a password. */
    readonly changePassword: (id: string, getSaltedPassword: (id: string) => string) => Promise<boolean>;

    /** Delete the user and all it's data. */
    readonly delete: (id: string) => Promise<void>;
}
