/** Describes a token alias provider. */
export interface ITokenAliasProvider {
    /** Create a token alias. */
    readonly create: (tokenAlias: ITokenAlias) => Promise<void>;

    /** Read a token. */
    readonly read: (alias: string) => Promise<string | undefined>;

    /** Delete a token alias. */
    readonly delete: (alias: string) => Promise<void>;
}

export interface ITokenAlias {
    token: string;
    alias: string;
}
