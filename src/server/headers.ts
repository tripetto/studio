export const HEADER_RUNNER_TOKEN = "Tripetto-Runner-Token";
export const HEADER_RUNNER_SIGNATURE = "Tripetto-Runner-Signature";
export const HEADER_TEMPLATE_TOKEN = "Tripetto-Template-Token";
export const HEADER_ATTACHMENT_TOKEN = "Tripetto-Attachment-Token";
