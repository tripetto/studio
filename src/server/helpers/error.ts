import { ILogProvider } from "../providers";

type TError = (err: {}) => Promise<never>;

export function logError(logProvider: ILogProvider): TError {
    return (err: {}) => {
        logProvider.error(err);
        return Promise.reject("Server error");
    };
}

export function logCriticalError(logProvider: ILogProvider): TError {
    return (err: {}) => {
        logProvider.critical(err);
        return Promise.reject("Critical Server error");
    };
}
