import { createHash as _createHash, randomFillSync } from "crypto";

export function createHash(data: string): string {
    const hash = _createHash("sha256");
    hash.update(data);
    return hash.digest("base64");
}

export const generatePassword = (length = 32, wishlist = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$") =>
    Array.from(randomFillSync(new Uint32Array(length)))
        .map((x) => wishlist[x % wishlist.length])
        .join("");
