import { Export } from "@tripetto/runner";
import { ATTACHMENT } from "../endpoints";
import { URL } from "../settings";

export function isAttachment(field: Export.IExportableField): boolean {
    return (
        field.type === "@tripetto/block-file-upload" ||
        field.type === "tripetto-block-file-upload" ||
        field.type === "file-upload" ||
        field.type === "@tripetto/block-signature"
    );
}

export function attachmentUrl(field: Export.IExportableField): string {
    return (
        (field.reference &&
            `${URL}${ATTACHMENT}/${field.reference || ""}${(field.string && `?filename=${encodeURIComponent(field.string)}`) || ""}`) ||
        ""
    );
}
