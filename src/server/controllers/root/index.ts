import {
    definitionProviderSymbol,
    metricProviderSymbol,
    tokenAliasProviderSymbol,
    tokenProviderSymbol,
    userProviderSymbol,
} from "../../symbols";
import express from "express";
import jwt from "jsonwebtoken";
import { inject, injectable } from "inversify";
import { IController, IUserTokenRequest } from "../controller";
import { BaseController } from "../base";
import { IDefinitionProvider, IMetricProvider, ITokenAliasProvider, ITokenProvider, IUserProvider } from "../../providers";
import cookieSession from "cookie-session";
import {
    ENV,
    SENTRY_DSN_WEBAPP,
    SESSION_COOKIE_NAME,
    TOKEN_COOKIE_NAME,
    TRIPETTO_APP_PK,
    TRIPETTO_FILESTORE_URL,
    TRIPETTO_LICENSE,
    URL,
} from "../../settings";
import { getReferer } from "../../helpers/request";
import { createHash } from "../../helpers/crypto";
import { IUser } from "../../entities/users";
import { ITemplateToken } from "../../entities/tokens/interface";
import { TEMPLATE } from "../../endpoints";

@injectable()
export class RootController extends BaseController implements IController {
    private readonly metricProvider: IMetricProvider;
    private readonly userProvider: IUserProvider;
    private readonly definitionProvider: IDefinitionProvider;
    private readonly tokenAliasProvider: ITokenAliasProvider;
    public router = express.Router();

    constructor(
        @inject(tokenProviderSymbol) tokenProvider: ITokenProvider,
        @inject(tokenAliasProviderSymbol) tokenAliasProvider: ITokenAliasProvider,
        @inject(userProviderSymbol) userProvider: IUserProvider,
        @inject(definitionProviderSymbol) definitionProvider: IDefinitionProvider,
        @inject(metricProviderSymbol) metricProvider: IMetricProvider
    ) {
        super(tokenProvider);

        this.tokenProvider = tokenProvider;
        this.tokenAliasProvider = tokenAliasProvider;
        this.userProvider = userProvider;
        this.definitionProvider = definitionProvider;
        this.metricProvider = metricProvider;

        this.router.get(
            ["/", `${TEMPLATE}/:alias`],
            this.authenticateByCookie(false),
            this.verifyTokenType("auth", false),
            cookieSession({ name: SESSION_COOKIE_NAME, secret: TRIPETTO_APP_PK, signed: false }),
            this.main
        );
    }

    /** Create an unique session id. */
    private createSessionId(request: express.Request): string {
        return createHash(`${request.ip}-${Date.now()}`);
    }

    private get main(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            const userRequest = req as IUserTokenRequest;

            if (req.session) {
                req.session.start = req.session.start || Date.now();

                if (!userRequest.token && req.session.isNew) {
                    const sessionId = this.createSessionId(req);
                    req.session.id = sessionId;

                    this.metricProvider.add({
                        event: "create",
                        subject: "anonymous_user",
                        subjectId: sessionId,
                        ip: req.ip,
                        referer: getReferer(req),
                    });
                }
            }

            this.hasValidTokenButIsNoValidUser(userRequest).then((value: boolean) => {
                if (value) {
                    res.clearCookie(TOKEN_COOKIE_NAME);
                }

                const tokenAlias = req.params.alias;
                this.isValidTemplate(tokenAlias).then((token: boolean | string) => {
                    if (tokenAlias) {
                        if (token === false) {
                            res.status(404).render("pages/template/not-found");
                            return;
                        } else {
                            this.metricProvider.add({
                                event: "read",
                                subject: "definition_template",
                                subjectId: tokenAlias,
                                userId: userRequest && userRequest.token && userRequest.token.user,
                                ip: req.ip,
                                referer: getReferer(req),
                            });
                        }
                    }

                    res.render("pages/app", {
                        cspNonce: res.locals.cspNonce,
                        url: URL,
                        filestoreUrl: TRIPETTO_FILESTORE_URL,
                        license: TRIPETTO_LICENSE,
                        sentryDSN: SENTRY_DSN_WEBAPP,
                        env: ENV,
                        isNewVisitor: req.session && req.session.isNew,
                        definitionTemplate: tokenAlias && token,
                    });
                });
            });
        };
    }

    private hasValidTokenButIsNoValidUser(userRequest: IUserTokenRequest): Promise<boolean> {
        if (userRequest.token) {
            return this.userProvider.readById(userRequest.token.user).then((user?: IUser) => {
                return !user;
            });
        } else {
            return Promise.resolve(false);
        }
    }

    private isValidTemplate(tokenAlias?: string): Promise<boolean | string> {
        if (!tokenAlias) {
            return Promise.resolve(false);
        }

        return this.tokenAliasProvider.read(tokenAlias).then(async (token?: string) => {
            if (!token) {
                return false;
            }

            try {
                const decoded = jwt.verify(token, TRIPETTO_APP_PK) as ITemplateToken;
                if (!decoded || decoded.type !== "template") {
                    return false;
                }

                const definition = await this.definitionProvider.readByPublicKey(decoded.user, decoded.definition, false);
                return definition !== undefined ? token : false;
            } catch (err) {
                return false;
            }
        });
    }
}
