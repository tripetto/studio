import * as TrustedTypes from "trusted-types";

declare const trustedTypes: typeof TrustedTypes.trustedTypes;

let cache: TrustedTypes.TrustedTypePolicy | undefined;

export function trackingTrustedType(url: string): string {
    if (typeof trustedTypes !== "undefined" && trustedTypes.createPolicy) {
        const policy =
            cache ||
            (cache = trustedTypes.createPolicy("default", {
                createScriptURL: (s) => {
                    if (s.indexOf("https://www.googletagmanager.com") === 0 || s.indexOf("https://connect.facebook.net") === 0) {
                        return s;
                    }

                    throw new Error(`Tripetto: Script '${s}' is not allowed!`);
                },
            }) as TrustedTypes.TrustedTypePolicy);

        return policy.createScriptURL(url) as {} as string;
    }

    return url;
}
